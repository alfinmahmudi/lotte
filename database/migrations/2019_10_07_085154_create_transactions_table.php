<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transactions', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->timestamps();
            $table->integer('cart_id');
            $table->integer('product_id');
            $table->integer('store_id');
            $table->integer('user_id');
            $table->integer('address_id');
            $table->bigInteger('total_quantity');
            $table->decimal('sub_total');
            $table->decimal('ppn_total');
            $table->decimal('diskon_total');
            $table->decimal('delivery_total');
            $table->decimal('grand_total');
            $table->decimal('payment_status');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transactions');
    }
}
