<?php

namespace App\Helpers;

use App\Service;

class Helper
{
    
    public static function check_login()
    {
        $token = session('token');
        return redirect()->action('LoginController@index'); 
    }

    public static function logout()
    {
        session()->flush();
        // print_r(session('token'));
        // die();
    }
    
    public static function storedata()
    {
        $service = new Service();
        $arrstore = $service->get_store();
        if (is_array($arrstore)) {
            $Allstore = $arrstore['data'];
        } else {
            $Allstore = [];
        }

        $first = 0;
        $sec = 0;
        $htmlres = '';

        foreach ($Allstore as $store) {
            if ($first == 0) {
                $htmlres .= '<div class="single__product">';
                $htmlres .= '        <div class="col-lg-3 col-md-4 col-sm-6 col-12">';
                $htmlres .= '            <div class="product product__style--3">';
                $htmlres .= '                <div class="product__thumb">';
                $htmlres .= '                    <a class="first__img" href="' . url('home') . '/' . $store['id'] . '"><img class="store" src="' . $store['image_path'] . '" alt="product image"></a>';
                $htmlres .= '                    <a class="second__img animation1" href="' . url('home') . '/' . $store['id'] . '"><img src="' . $store['image_path'] . '" alt="product image"></a>';
                $htmlres .= '                    <div class="hot__box">';
                $htmlres .= '                        <span class="hot-label">' . $store['name'] . '</span>';
                $htmlres .= '                    </div>';
                $htmlres .= '                </div>';
                $htmlres .= '                <div class="product__content content--center content--center">';
                $htmlres .= '                    <h4><a href="' . url('home') . '/' . $store['id'] . '">' . $store['name'] . '</a></h4>';
                $htmlres .= '                    <div class="product__hover--content">';
                $htmlres .= '                        <h3><a href="' . url('home') . '/' . $store['id'] . '">Pilih</a></h3>';
                $htmlres .= '                    </div>';
                $htmlres .= '                </div>';
                $htmlres .= '            </div>';
                $htmlres .= '        </div>';

                $first = 1;
                $sec = 0;
            } elseif ($sec == 0) {
                $htmlres .= '<div class="col-lg-3 col-md-4 col-sm-6 col-12">';
                $htmlres .= '            <div class="product product__style--3">';
                $htmlres .= '                <div class="product__thumb">';
                $htmlres .= '                    <a class="first__img" href="' . url('home') . '/' . $store['id'] . '"><img class="store" src="' . $store['image_path'] . '" alt="product image"></a>';
                $htmlres .= '                    <a class="second__img animation1" href="' . url('home') . '/' . $store['id'] . '"><img src="' . $store['image_path'] . '" style="width: 270px; height: 202px;" alt="product image"></a>';
                $htmlres .= '                    <div class="hot__box">';
                $htmlres .= '                        <span class="hot-label">' . $store['name'] . '</span>';
                $htmlres .= '                    </div>';
                $htmlres .= '                </div>';
                $htmlres .= '                <div class="product__content content--center content--center">';
                $htmlres .= '                    <h4><a href="' . url('home') . '/' . $store['id'] . '">' . $store['name'] . '</a></h4>';
                $htmlres .= '                    <div class="product__hover--content">';
                $htmlres .= '                        <h3><a a href="' . url('home') . '/' . $store['id'] . '">Pilih</a></h3>';
                $htmlres .= '                    </div>';
                $htmlres .= '                </div>';
                $htmlres .= '            </div>';
                $htmlres .= '        </div>';
                $htmlres .= '    </div>';

                $first = 0;
                $sec = 1;
            }
        }

        return $htmlres;
    }

    public static function generate_product($store_id, $arrprod)
    {
        $service = new Service();
        // print_r($arrprod);
        // die();
        $htmlprod = '';
        if (is_array($arrprod) && $arrprod['code'] == 200) {
            $Allprod = $arrprod['data'];
            foreach ($Allprod as $prod) {
                $url_photo = '';
                $htmlbblm = '';
                // $Allphotoprod = Product_photo::all()->toArray();
                $arrprodimg = $service->get_productimg($prod['product_id']);
                // print_r($arrprodimg);
                // die();
                if ($arrprodimg['code'] == 200) {
                    $url_photo = $arrprodimg['data'][0]['image'];
                }

                if ($prod['is_bblm'] == 1) {
                    $is_bblm = 1;

                    $arrbblm = $service->get_productdetail($store_id, $prod['product_id'], '', $is_bblm);
                    // print_r($arrbblm);
                    // die();

                    if ($arrbblm['code'] == 200) {
                        foreach ($arrbblm['data'] as $bblm) {
                            $htmlbblm .= '<div class="col-lg-6 col-6">';
                            $htmlbblm .= '    <table>';
                            $htmlbblm .= '        <tr>';
                            $htmlbblm .= '            <td colspan="3"><b>Level ' . $bblm['level'] . '</b></td>';
                            $htmlbblm .= '        </tr>';
                            $htmlbblm .= '        <tr>';
                            $htmlbblm .= '            <td>Info Pembelian</td>';
                            $htmlbblm .= '            <td>:</td>';
                            $htmlbblm .= '            <td>' . $bblm['info'] . '</td>';
                            $htmlbblm .= '        </tr>';
                            $htmlbblm .= '        <tr>';
                            $htmlbblm .= '            <td>Harga Persatuan</td>';
                            $htmlbblm .= '            <td>:</td>';
                            $htmlbblm .= '            <td>Rp. ' . number_format($bblm['price']) . '</td>';
                            $htmlbblm .= '        </tr>';
                            $htmlbblm .= '    </table>';
                            $htmlbblm .= '</div>';
                        }
                    }
                    
                }else{
                    $is_bblm = 0;
                }

                $htmlprod .= '<div class="product product__style--3 col-lg-3 col-md-3 col-sm-6 col-12">';
                $htmlprod .= '    <div class="product__thumb">';
                $htmlprod .= '        <a class="first__img" href="detail"><img class="product" src="' . $url_photo . '" alt="product image"></a>';
                $htmlprod .= '        <a class="second__img animation1" href="' . action('ProductController@detail', ['store_id' => $store_id, 'product_id' => $prod['product_id'], 'is_bblm' => $is_bblm]) . '"><img class="product" src="' . $url_photo . '" alt="product image"></a>';

                if ($prod['is_bblm']) {
                    $htmlprod .= '        <div class="hot__box_cust">';
                    $htmlprod .= '            <img style="width: 20%;" src="' . asset('img/bblm.png') . '" alt="">';
                    $htmlprod .= '        </div>';
                }

                $htmlprod .= '    </div>';
                $htmlprod .= '    <div class="product__content content--center">';
                $htmlprod .= '        <h4><a href="' . action('ProductController@detail', ['store_id' => $store_id, 'product_id' => $prod['product_id'], 'is_bblm' => $is_bblm]) . '">' . $prod['product_name'] . '</a></h4>';

                if ($prod['is_promo']) {

                    $htmlprod .= '        <ul class="prize d-flex">';
                    $htmlprod .= '            <li>Rp. ' . number_format($prod['price']) . '</li>';
                    $htmlprod .= '        </ul>';

                    $htmlprod .= '        <ul class="prize d-flex">';
                    $htmlprod .= '            <li class="old_prize">Rp. ' . number_format($prod['base_price']) . '</li> ';
                    $htmlprod .= '        </ul>';

                } else {
                    $htmlprod .= '        <ul class="prize d-flex">';
                    $htmlprod .= '            <li>Rp. ' . number_format($prod['base_price']) . '</li> ';
                    $htmlprod .= '        </ul>';
                }

                
                $htmlprod .= '        <div class="action">';
                $htmlprod .= '            <div class="product__hover--content">';
                $htmlprod .= '              <h3>
                                                <a href="javascript:void(0)" prod_price="' . number_format($prod['base_price']) . '" prod_quant="' . $prod['min_order'] . '" 
                                                prod_name="' . $prod['product_name'] . '" prod_img="' . $url_photo . '" 
                                                onclick="quantity_modal($(this), \'' . $store_id . '\', \'' . $prod['product_id'] . '\')">
                                                Add To Cart</a>
                                            </h3>';
                $htmlprod .= '            </div> ';
                $htmlprod .= '        </div>';
                $htmlprod .= '    </div>';
                $htmlprod .= '</div>';
                $htmlprod .= '<div style="display: none;" id="bblm_' . $prod['product_id'] . '">';
                $htmlprod .= $htmlbblm;
                $htmlprod .= '</div>';
            }
        } else {
            $htmlprod = 'Product Not Found';
        }

        return $htmlprod;
    }
}



?>