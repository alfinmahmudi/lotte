<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Session;

class Service extends Model
{
    protected $store_id;
    var $appid = 'b2b8d15a-4ce8-461a-86ac-f5c44886474d';
    var $url_api = 'https://devlotteapp.ngobrol.co.id/web/v1';
    var $keymaps = 'AIzaSyDPmDqJoysQK_h3pjevhJLs6Mt8WYYhoNI';

    public function api($path, $data = '', $token = '')
    {
        if ($token) {
            $headers = array(
                'Authorization: Bearer ' . $token,
                'X-APP-ID: ' . $this->appid,
                'Content-Type: application/json'
            );   
        }else{
            $headers = array(
                'X-APP-ID: ' . $this->appid,
                'Content-Type: application/json'
            );
        }

        if (!$data) {
            $data = json_encode(array($data));
        }

        // print_r($data);
        // die();

        #Send Reponse To FireBase Server
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $this->url_api . $path);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, ($data));
        $result = curl_exec($ch);
        curl_close($ch);

        #Echo Result Of FireBase Server
        $json = json_decode($result, True);

        return $json;
    }

    public function api_get($path)
    {
        $token = session('token');
        $headers = array(
            'Authorization: Bearer ' . $token,
            'X-APP-ID: ' . $this->appid,
            'Content-Type: application/json'
        );

        // print_r($this->url_api . $path);
        // die();

        #Send Reponse To FireBase Server
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $this->url_api . $path);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $result = curl_exec($ch);
        curl_close($ch);

        #Demo mode
        // $result = array('message_id' => 'testing');

        #Echo Result Of FireBase Server
        $json = json_decode($result, True);

        return $json;
    }

    public function api_delete($path, $data='')
    {
        $token = session('token');
        $headers = array(
            'Authorization: Bearer ' . $token,
            'X-APP-ID: ' . $this->appid,
            'Content-Type: application/json'
        );

        // print_r($data);
        // die();

        #Send Reponse To FireBase Server
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $this->url_api . $path);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "DELETE");
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        if ($data) {
            curl_setopt($ch, CURLOPT_POSTFIELDS, ($data));
        }

        $result = curl_exec($ch);
        curl_close($ch);

        #Demo mode
        // $result = array('message_id' => 'testing');

        #Echo Result Of FireBase Server
        $json = json_decode($result, True);

        return $json;
    }

    public function api_delete_all($path)
    {
        $token = session('token');
        $headers = array(
            'Authorization: Bearer ' . $token,
            'X-APP-ID: ' . $this->appid,
            'Content-Type: application/json'
        );

        #Send Reponse To FireBase Server
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $this->url_api . $path);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "DELETE");
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $result = curl_exec($ch);
        curl_close($ch);

        #Demo mode
        // $result = array('message_id' => 'testing');

        #Echo Result Of FireBase Server
        $json = json_decode($result, True);

        return $json;
    }

    public function api_update($path, $data)
    {
        $token = session('token');
        $headers = array(
            'Authorization: Bearer ' . $token,
            'X-APP-ID: ' . $this->appid,
            'Content-Type: application/json'
        );

        if (!$data) {
            $data = json_encode(array($data));
        }

        // print_r($data);
        // die();

        #Send Reponse To FireBase Server
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $this->url_api . $path);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, ($data));
        $result = curl_exec($ch);
        curl_close($ch);

        #Demo mode
        // $result = array('message_id' => 'testing');

        #Echo Result Of FireBase Server
        $json = json_decode($result, True);

        return $json;
    }

    public function check_address($addrs_id = '')
    {
        if ($addrs_id == '') {
            $res = $this->api_get('/user/address');
        }else{
            $res = $this->api_get('/user/address/' . $addrs_id);
        }

        $arrret = ['code' => 404];
        if (!is_array($res)) {
            $res = $arrret;
        }

        // print_r($res);
        // die();

        return $res;
    }

    public function check_user()
    {
        $res = $this->api_get('/user');

        // print_r($res);
        // die();

        return $res;
    }

    public function get_province()
    {
        $res = $this->api_get('/geo/province');
        // print_r($res);
        // die();

        return $res;
    }

    public function get_regency($province_id)
    {
        $res = $this->api_get('/geo/regency/' . $province_id);
        // print_r($res);
        // die();

        return $res;
    }

    public function get_store()
    {
        $res = $this->api_get('/store');
        // print_r($res);
        // die();

        return $res;
    }

    public function get_store_data($store_id_params)
    {
        $res = false;
        $result = $this->api_get('/store/' . $store_id_params);
        // print_r($res);
        // die();
        if (is_array($result) && $result['data']) {
            if (!Session::get('storedata')) {
                Session::put('store_id', $store_id_params);
                Session::put('storedata', $result['data']);
            }elseif (Session('store_id') != $store_id_params) {
                Session::put('store_id', $store_id_params);
                Session::put('storedata', $result['data']);
    
                $result_delete = $this->api_delete_all('/cart');
                if ($result_delete['code'] == 200) {
                    session()->put('sum_cart', 0);
                }
            }  
            $res = true; 
        }

        return $res;
    }

    public function get_banner($store_id)
    {
        $res = $this->api_get('/store/' . $store_id . '/banner');
        // print_r($res);
        // die();

        return $res;
    }

    public function get_expedition()
    {
        $res = $this->api_get('/shipping/expedition');
        // print_r($res);
        // die();

        return $res;
    }

    public function get_ship()
    {
        $res = $this->api_get('/shipping');
        // print_r($res);
        // die();

        return $res;
    }

    public function get_payment()
    {
        $res = $this->api_get('/payment/channel');
        // print_r($res);
        // die();

        return $res;
    }

    public function get_promo($store_id)
    {
        $htmlpromo = '';
        $arrpromo = $this->api_get('/product/' . $store_id . '/promo?type=list');
        // print_r($arrpromo);
        // die();
        if ($arrpromo['code'] == 200 && $arrpromo['data']) {
            foreach ($arrpromo['data'] as $promo) {
                // print_r($promo);
                // die();
                $htmlpromo .= '<div class="col-lg-6">';
                $htmlpromo .= '    <div class="wn__team text-center">';
                $htmlpromo .= '        <div class="thumb">';
                $htmlpromo .= '            <a href="' . url('product/promo_detail/' . $store_id . '/' . $promo['id']) . '"><img src="' . $promo['image'] . '" alt="Team images"></a>';
                $htmlpromo .= '        </div>';
                $htmlpromo .= '        <div class="content" style="text-align: left;">';
                $htmlpromo .= '            <h4>' . $promo['title'] . '</h4>';
                $htmlpromo .= '            <p>Periode : <br> ' . date('d F Y - H:i:s', strtotime($promo['start_date'])) . ' s/d ' . date('d F Y - H:i:s', strtotime($promo['end_date'])) . '</p>';
                $htmlpromo .= '        </div>';
                $htmlpromo .= '    </div>';
                $htmlpromo .= '</div>';
            }
        }else{
            $htmlpromo = 'Promo Not Found';
        }
        // print_r($htmlpromo);
        //         die();
        return $htmlpromo;
    }

    public function get_profile()
    {
        $retuser = '';
        $arruser = $this->api_get('/user');
        // print_r($arruser);
        // die();
        if ($arruser) {
            $retuser = $arruser['data'];
        }

        return $retuser;
    }

    public function get_category($store_id)
    {
        $arrcategory = $this->api_get('/category/all');
        // print_r($arrcategory);
        // die();
        $htmlcat = '';
        $htmlcat_prod = '';
        if ($arrcategory['code'] == 200) {
            $i = 0;
            foreach ($arrcategory['data'] as $cat) {
                // $arrdetcat = $this->api_get('/category?cat_id=' . $cat['id']);
                // print_r($cat);
                // die();

                if (count($cat['sub_category']) > 0) {
                    $htmlcat .= '<li class="label2"><a>' . $cat['name'] . '</a>';
                    $htmlcat .= '    <ul>';

                    $htmlcat_prod .= '<li>';
                    $htmlcat_prod .= '<a href="#Submenu_' . $cat['id'] . '" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle"><strong>' . $cat['name'] . '</strong></a>';
                    $htmlcat_prod .= '<ul class="collapse list-unstyled" id="Submenu_' . $cat['id'] . '">';

                    foreach ($cat['sub_category'] as $det) {
                        $htmlcat .= '        <li><a href="' . action('ProductController@category', ['store_id' => $store_id, 'product_id' => $det['id']]) . '">' . $det['name'] . '</a></li>';

                        $htmlcat_prod .= '<li style="padding-left: 15px;"><a href="' . action('ProductController@category', ['store_id' => $store_id, 'product_id' => $det['id']]) . '">' . $det['name'] . '</a></li>';
                    }
                    
                    $htmlcat .= '    </ul>';
                    $htmlcat .= '</li>';

                    $htmlcat_prod .= '    </ul>';
                    $htmlcat_prod .= '</li>';
                } else {
                    $htmlcat .= '<li><a href="' . action('ProductController@category', ['store_id' => $store_id, 'product_id' => $cat['id']]) . '">' . $cat['name'] . '</a></li>';
                    $htmlcat_prod .= '<li><a href="' . action('ProductController@category', ['store_id' => $store_id, 'product_id' => $cat['id']]) . '">' . $cat['name'] . '</a></li>';
                }
            }   
        }

        $arrret = ['cat' => $htmlcat, 'cat_prod' => $htmlcat_prod];

        return $arrret;
    }

    public function get_product($store_id, $cat = '', $bblm = '', $page = '', $name = "", $promo = "")
    {
        $get = '';
        if ($cat != '' ) {
            $get .= '?cat_id=' . $cat;
        }

        if ($bblm != '') {
            if ($get == '') {
                $get .= '?bblm=' . $bblm;
            }else{
                $get .= '&bblm=' . $bblm;
            }
        }

        if ($page != '') {
            if ($get == '') {
                $get .= '?page=' . $page;
            } else {
                $get .= '&page=' . $page;
            }
        }

        if ($name != '') {
            $name = urlencode($name);
            if ($get == '') {
                $get .= '?name=' . $name;
            } else {
                $get .= '&name=' . $name;
            }
        }

        if ($promo != '') {
            $get .= '/promo?promo_id=' . $promo;
        }

        // print_r(session('token'));
        // print_r('/product/' . $store_id . $get);
        // die();

        $res = $this->api_get('/product/' . $store_id . $get);
        // print_r($res);
        // die();

        return $res;
    }

    public function get_productdetail($store_id, $product_id = '', $barcode = '', $is_bblm = '')
    {
        $get = '';
        if ($product_id != '') {
            $get .= '?id=' . $product_id;
        }

        if ($barcode != '') {
            if ($get == '') {
                $get .= '?barcode=' . $barcode;
            } else {
                $get .= '&barcode=' . $barcode;
            }
        }

        if ($is_bblm) {
            $res = $this->api_get('/product/' . $store_id . '/bblm' . $get);
        }else{
            $res = $this->api_get('/product/' . $store_id . '/detail' . $get);
        }

        // print_r($res);
        // die();

        return $res;
    }

    public function get_productimg($product_id = '', $barcode = '')
    {
        $get = '';
        if ($product_id != '') {
            $get .= '?id=' . $product_id;
        }

        if ($barcode != '') {
            if ($get == '') {
                $get .= '?barcode=' . $barcode;
            } else {
                $get .= '&barcode=' . $barcode;
            }
        }

        $res = $this->api_get('/product/image' . $get);
        // print_r($res);
        // die();

        return $res;
    }

    public function get_cart()
    {
        $res = $this->api_get('/cart');
        // print_r($res);
        // die();

        return $res;
    }

    public function get_history($history_id = '')
    {
        if ($history_id == '') {
            $res = $this->api_get('/transaction');   
        }else{
            $res = $this->api_get('/transaction/' . $history_id);
        }
        // print_r($res);
        // die();

        return $res;
    }

    public function get_history_detail($trans_id)
    {
        $res = $this->api_get('/transaction/' . $trans_id);
        // print_r($res);
        // die();

        return $res;
    }

    public function get_list_voucher($store_id)
    {
        $res = $this->api_get('/voucher/' . $store_id);
        // print_r($res);
        // die();

        return $res;
    }

    public function get_list_voucher_detail($store_id)
    {
        $res = $this->api_get('/voucher/' . $store_id . '/detail');
        // print_r($res);
        // die();

        return $res;
    }

    public function get_search_voucher($store_id, $code_txt)
    {
        $code_txt = urlencode($code_txt);
        $res = $this->api_get('/voucher/' . $store_id . '/' . $code_txt);
        // print_r($res);
        // die();

        return $res;
    }

    public function get_search_maps($value)
    {
        $value = urlencode($value);
        $url = 'https://maps.googleapis.com/maps/api/place/autocomplete/json?input=' . $value . '&language=ID&key=' . $this->keymaps;
        #Send Reponse To FireBase Server
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        // curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        $result = curl_exec($ch);
        curl_close($ch);

        #Echo Result Of FireBase Server
        $json = json_decode($result, True);

        return $json;
    }

}
