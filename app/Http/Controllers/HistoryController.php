<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Helpers\Helper;
use App\Service;

class HistoryController extends Controller
{
    public function __construct()
    {
        $this->service = new Service();
        $this->data_header = array(
            'upsource' => array(),
            'down_plugins' => array(),
            'down_scripts' => array()
        );
    }

    public function detail($store_id, $history_id)
    {
        if (!session('token')) {
            Helper::logout();
            return redirect()->action('LoginController@index');
        }
        #Include script
        $data_header = $this->data_header;

        //ui_sweetalert.html
        array_push($data_header['upsource'], asset('templates/plugins/bootstrap-sweetalert/sweetalert.css'));
        array_push($data_header['down_plugins'], asset('templates/plugins/bootstrap-sweetalert/sweetalert.min.js'));

        //form wizard
        array_push($data_header['upsource'], asset('templates/plugins/form-wizard/css/style.css'));
        array_push($data_header['down_plugins'], asset('templates/plugins/form-wizard/js/main.js'));

        //animate.css
        array_push($data_header['upsource'], asset('css/animate.css'));

        // table set
        array_push($data_header['down_scripts'], asset('js/table.js'));

        $source['upsource'] = $data_header['upsource'];
        $source['down_plugins'] = $data_header['down_plugins'];
        $source['down_scripts'] = $data_header['down_scripts'];

        $htmlres = Helper::storedata($store_id);
        // print_r($htmlres);
        // die();

        // $rescat = $this->service->get_category($store_id);
        // print_r($arrcategory);
        // die();

        $reshis = $this->service->get_history($history_id);
        // print_r($reshis);
        // die();

        $htmlhis = '';
        $btnpayment = '';
        foreach ($reshis['data']['products'] as $prod) {
            $total_price = $prod['price'] * $prod['total'];

            $htmlhis .= '<div class="post__itam" id="his_number_' . $prod['id'] . '">';
            $htmlhis .= '    <div class="content" style="padding: 20px 20px 15px;">';
            $htmlhis .= '        <div class="row">';
            $htmlhis .= '            <div class="col-md-3 col-sm-3 ol-lg-3">';
            $htmlhis .= '                <div class="thumb">';
            $htmlhis .= '                    <img src="' . $prod['image_url'] . '" alt="product images">';
            $htmlhis .= '                </div>';
            $htmlhis .= '            </div>';
            $htmlhis .= '            <div class="col-md-9 col-sm-9 ol-lg-9">';
            $htmlhis .= $prod['title'];
            $htmlhis .= '                <br> <small>Rp. ' . number_format($prod['price']) . '\satuan</small>';
            $htmlhis .= '                <br> <small>' . $prod['total'] . ' Barang</small>';
            $htmlhis .= '                <br> <span style="color: #e80916;">Rp. ' . number_format($total_price) . '</span>';
            $htmlhis .= '                <br>';
            $htmlhis .= '                <span id="notes_span_' . $prod['id'] . '">' . $prod['note'] . '</span>';
            $htmlhis .= '                <input type="hidden" id="notes_' . $prod['id'] . '" name="DATA[' . $prod['id'] . '][note]" value="' . $prod['note'] . '"> ';
            $htmlhis .= '                <input type="hidden" id="prod_id_' . $prod['id'] . '" name="DATA[' . $prod['id'] . '][product_id]" value="' . $prod['id'] . '"> ';
            $htmlhis .= '                <input type="hidden" id="store_id_' . $prod['id'] . '" name="DATA[' . $prod['id'] . '][store_id]" value="' . $store_id . '"> ';
            $htmlhis .= '                <input type="hidden" id="total_' . $prod['id'] . '" name="DATA[' . $prod['id'] . '][total]" value="' . $prod['total'] . '"> ';
            $htmlhis .= '            </div>';
            $htmlhis .= '        </div>';
            $htmlhis .= '    </div>';
            $htmlhis .= '</div>';
        }

        $htmlstat = '<ul id="progressbar">';
        $no = 1;

        $arrservice = $this->service->api_get('/transaction/status/' . $history_id);
        // print_r($arrservice);
        // die();
        if (array_key_exists('payment_status', $arrservice['data'])) {
            $htmlstat .= '    <li class="active" id="cust_1"><strong>' . $arrservice['data']['payment_status']['state_name'] . ' : ' . $arrservice['data']['payment_status']['status_name'] . ' <br> ' . date('d F Y - H:i:s', strtotime($arrservice['data']['payment_status']['created_at'])) . '</strong></li>';   
        }else{
            $htmlstat .= '    <li class="active" id="cust_1"><strong> PAYMENT : - </strong></li>';
        }

        if (array_key_exists('process_status', $arrservice['data'])) {
            $htmlstat .= '    <li class="active" id="cust_2"><strong>' . $arrservice['data']['process_status']['state_name'] . ' : ' . $arrservice['data']['process_status']['status_name'] . ' <br> ' . date('d F Y - H:i:s', strtotime($arrservice['data']['process_status']['created_at'])) . '</strong></li>';
        }else{
            $htmlstat .= '    <li class="active" id="cust_2"><strong> PROCESS : - </strong></li>';
        }

        if (array_key_exists('ship_status', $arrservice['data'])) {
            $htmlstat .= '    <li class="active" id="cust_3"><strong>' . $arrservice['data']['ship_status']['state_name'] . ' : ' . $arrservice['data']['ship_status']['status_name'] . ' <br> ' . date('d F Y - H:i:s', strtotime($arrservice['data']['ship_status']['created_at'])) . '</strong></li>';
        }else{
            $htmlstat .= '    <li class="active" id="cust_3"><strong> DELIVERY : - </strong></li>';
        }
        
        if (array_key_exists('final_status', $arrservice['data'])) {
            $htmlstat .= '    <li class="active" id="cust_4"><strong>' . $arrservice['data']['final_status']['state_name'] . ' : ' . $arrservice['data']['final_status']['status_name'] . ' <br> ' . date('d F Y - H:i:s', strtotime($arrservice['data']['final_status']['created_at'])) . '</strong></li>';
        }else{
            $htmlstat .= '    <li class="active" id="cust_4"><strong> RECEIVE : - </strong></li>';
        }

        $stat_arr = $reshis['data']['status'];
        // print_r($stat_arr);
        // die();
        foreach ($stat_arr as $stat) {
            if (count($stat) > 0) {
                if ($stat['status_id'] == 13 && $stat['state_id'] == 1 && $reshis['data']['payment']['id'] != 9) {
                    $btnpayment = '';
                    $btnpayment .= '<div class="col-sm-12 text-center-cust">';
                    $btnpayment .= '    <button type="button" data-url="' . $reshis['data']['payment_url'] . '" onclick="load_payment_modal($(this))" style="width: 100%;" class="btn-cust" div_now="div_tamu" href="javascript:void(0)" style="width: 100%;">Lakukan Pembayaran</button>';
                    $btnpayment .= '</div>';  
                }else if ($stat['state_id'] == 3 && $stat['status_id'] == 11) {
                    $btnpayment = '';
                    $btnpayment .= '<div class="col-sm-12 text-center-cust">';
                    $btnpayment .= '    <button type="button" data-url="' . url('history/setstatus/') . '/' . $reshis['data']['transaction_id'] . '/11' . '" onclick="setstatus($(this))" style="width: 100%;" class="btn-cust" href="javascript:void(0)">Konfirmasi Pickup</button>';
                    $btnpayment .= '</div>';
                }else if ($stat['state_id'] == 4 && $stat['status_id'] == 14) {
                    $btnpayment = '';
                    $btnpayment .= '<div class="col-sm-12 text-center-cust">';
                    $btnpayment .= '    <button type="button" data-url="' . url('history/setstatus/') . '/' . $reshis['data']['transaction_id'] . '/14' . '" onclick="setstatus($(this))" style="width: 100%;" class="btn-cust" href="javascript:void(0)">Konfirmasi Delivery</button>';
                    $btnpayment .= '</div>';
                }else if ($stat['state_id'] == 5 && $stat['status_id'] == 5) {
                    $btnpayment = '';
                    $btnpayment .= '<div class="col-sm-12 text-center-cust">';
                    $btnpayment .= '    <button type="button" data-url="' . url('cart/add_to_cart_again') . '" onclick="add_to_cart_again(form_cart_all, $(this))" style="width: 100%;" class="btn-cust" href="javascript:void(0)">Beli Lagi</button>';
                    $btnpayment .= '</div>';
                }
            }
        }

        $htmlstat .= '</ul>';

        $datahis['htmlhis'] = $htmlhis;
        $datahis['transaction_id'] = $reshis['data']['transaction_id'];
        $datahis['shipping'] = $reshis['data']['shipping']['name'];
        $datahis['pickup_date'] = $reshis['data']['pickup_date'];
        $datahis['store_name'] = $reshis['data']['store']['name'];
        $datahis['payment'] = $reshis['data']['payment']['name'];
        $datahis['total_price'] = $reshis['data']['total_price'];
        $datahis['sub_total_price'] = $reshis['data']['sub_total_price'];
        $datahis['service_fee'] = $reshis['data']['service_fee'];
        $datahis['shipping_price'] = $reshis['data']['shipping_price'];
        $datahis['bank_service_fee'] = $reshis['data']['payment_service_fee'];
        $datahis['awb'] = $reshis['data']['awb'];
        $datahis['payment_va_no'] = $reshis['data']['payment_va_no'];
        $datahis['status'] = $htmlstat;
        $datahis['btnpayment'] = $btnpayment;

        return view('history.detail', ['source' => $source, 'htmlStore' => $htmlres, 'datahis' => $datahis]);
    }

    public function process($store_id)
    {
        if (!session('token')) {
            Helper::logout();
            return redirect()->action('LoginController@index');
        }
        #Include script
        $data_header = $this->data_header;

        //ui_sweetalert.html
        array_push($data_header['upsource'], asset('templates/plugins/bootstrap-sweetalert/sweetalert.css'));
        array_push($data_header['down_plugins'], asset('templates/plugins/bootstrap-sweetalert/sweetalert.min.js'));

        //animate.css
        array_push($data_header['upsource'], asset('css/animate.css'));

        // table set
        array_push($data_header['down_scripts'], asset('js/table.js'));

        $source['upsource'] = $data_header['upsource'];
        $source['down_plugins'] = $data_header['down_plugins'];
        $source['down_scripts'] = $data_header['down_scripts'];

        $htmlres = Helper::storedata($store_id);
        // print_r($htmlres);
        // die();

        // $rescat = $this->service->get_category($store_id);
        // print_r($arrcategory);
        // die();

        $reshis = $this->service->get_history();
        // print_r($reshis);
        // die();

        $datahis['htmlhis'] = $this->generate_his($reshis, 2);

        if ($datahis['htmlhis'] == '') {
            return view('empty', ['source' => $source, 'msg' => 'History Not Found']);
        }

        return view('history.default', ['source' => $source, 'htmlStore' => $htmlres, 'datahis' => $datahis]);
    }

    public function waiting($store_id)
    {
        if (!session('token')) {
            Helper::logout();
            return redirect()->action('LoginController@index');
        }
        #Include script
        $data_header = $this->data_header;

        //ui_sweetalert.html
        array_push($data_header['upsource'], asset('templates/plugins/bootstrap-sweetalert/sweetalert.css'));
        array_push($data_header['down_plugins'], asset('templates/plugins/bootstrap-sweetalert/sweetalert.min.js'));

        //animate.css
        array_push($data_header['upsource'], asset('css/animate.css'));

        // table set
        array_push($data_header['down_scripts'], asset('js/table.js'));

        $source['upsource'] = $data_header['upsource'];
        $source['down_plugins'] = $data_header['down_plugins'];
        $source['down_scripts'] = $data_header['down_scripts'];

        $htmlres = Helper::storedata($store_id);
        // print_r($htmlres);
        // die();

        // $rescat = $this->service->get_category($store_id);
        // print_r($arrcategory);
        // die();

        $reshis = $this->service->get_history();
        // print_r($reshis);
        // die();

        $datahis['htmlhis'] = $this->generate_his($reshis, 1);
        // print_r($reshis);
        // die();

        if ($datahis['htmlhis'] == '') {
            return view('empty', ['source' => $source, 'msg' => 'History Not Found']);
        }

        return view('history.default', ['source' => $source, 'htmlStore' => $htmlres, 'datahis' => $datahis]);
    }

    public function delivered($store_id)
    {
        if (!session('token')) {
            Helper::logout();
            return redirect()->action('LoginController@index');
        }
        #Include script
        $data_header = $this->data_header;

        //ui_sweetalert.html
        array_push($data_header['upsource'], asset('templates/plugins/bootstrap-sweetalert/sweetalert.css'));
        array_push($data_header['down_plugins'], asset('templates/plugins/bootstrap-sweetalert/sweetalert.min.js'));

        //animate.css
        array_push($data_header['upsource'], asset('css/animate.css'));

        // table set
        array_push($data_header['down_scripts'], asset('js/table.js'));

        $source['upsource'] = $data_header['upsource'];
        $source['down_plugins'] = $data_header['down_plugins'];
        $source['down_scripts'] = $data_header['down_scripts'];

        $htmlres = Helper::storedata($store_id);
        // print_r($htmlres);
        // die();

        // $rescat = $this->service->get_category($store_id);
        // print_r($arrcategory);
        // die();

        $reshis = $this->service->get_history();
        // print_r($reshis);
        // die();

        $datahis['htmlhis'] = $this->generate_his($reshis, 3);

        if ($datahis['htmlhis'] == '') {
            return view('empty', ['source' => $source, 'msg' => 'History Not Found']);
        }

        return view('history.default', ['source' => $source, 'htmlStore' => $htmlres, 'datahis' => $datahis]);
    }

    public function failed($store_id)
    {
        if (!session('token')) {
            Helper::logout();
            return redirect()->action('LoginController@index');
        }
        #Include script
        $data_header = $this->data_header;

        //ui_sweetalert.html
        array_push($data_header['upsource'], asset('templates/plugins/bootstrap-sweetalert/sweetalert.css'));
        array_push($data_header['down_plugins'], asset('templates/plugins/bootstrap-sweetalert/sweetalert.min.js'));

        //animate.css
        array_push($data_header['upsource'], asset('css/animate.css'));

        // table set
        array_push($data_header['down_scripts'], asset('js/table.js'));

        $source['upsource'] = $data_header['upsource'];
        $source['down_plugins'] = $data_header['down_plugins'];
        $source['down_scripts'] = $data_header['down_scripts'];

        $htmlres = Helper::storedata($store_id);
        // print_r($htmlres);
        // die();

        // $rescat = $this->service->get_category($store_id);
        // print_r($arrcategory);
        // die();

        $reshis = $this->service->get_history();
        // print_r($reshis);
        // die();

        $datahis['htmlhis'] = $this->generate_his($reshis, 4);

        if ($datahis['htmlhis'] == '') {
            return view('empty', ['source' => $source, 'msg' => 'History Not Found']);
        }

        return view('history.default', ['source' => $source, 'htmlStore' => $htmlres, 'datahis' => $datahis]);
    }

    public function generate_his($reshis, $type)
    {
        // 1. waiting
        // 2. process
        // 3. delivery
        // 4. falied

        // print_r($reshis);
        // die();
        $datahis = [];
        $htmlhis = '';

        if ($reshis['code'] == 200) {
            foreach ($reshis['data'] as $his) {
                
                if ($type == 1) {
                    if ($his['state']['id'] == 1 && $his['status']['id'] == 13) {
                        $htmlhis .= $this->html_his($his);
                        // print_r($htmlhis);
                        // die();
                    }   
                }elseif ($type == 2) {
                    if ($his['state']['id'] == 1 && $his['status']['id'] == 12) {
                        $htmlhis .= $this->html_his($his);
                    } elseif ($his['state']['id'] == 2) {
                        $htmlhis .= $this->html_his($his);
                    } elseif ($his['state']['id'] == 4) {
                        $htmlhis .= $this->html_his($his);
                    } elseif ($his['state']['id'] == 3) {
                        $htmlhis .= $this->html_his($his);
                    }
                }elseif ($type == 3) {
                    if ($his['state']['id'] == 5 && $his['status']['id'] == 5) {
                        $htmlhis .= $this->html_his($his);
                    } elseif ($his['state']['id'] == 4 && $his['status']['id'] == 14) {
                        $htmlhis .= $this->html_his($his);
                    } elseif ($his['state']['id'] == 3 && $his['status']['id'] == 11) {
                        $htmlhis .= $this->html_his($his);
                    }
                }elseif ($type == 4) {
                    if ($his['status']['id'] == 4) {
                        $htmlhis .= $this->html_his($his);   
                    }
                }else{
                    $htmlhis .= '<div class="post__itam">';
                    $htmlhis .= '    <div class="content" style="padding: 20px 20px 15px;">';
                    $htmlhis .= '        <h3 style="text-align: center;">History Not Found</h3> ';
                    $htmlhis .= '    </div>';
                    $htmlhis .= '</div>';
                }

                // print_r($htmlhis);
                // die();
            }
        }

        // print_r($htmlhis);
        // die();

        return $htmlhis;
    }

    public function html_his($his)
    {
        // print_r($his);
        // die();
        $htmlhis = '';
        // $htmlhis .= '<a href="' . url('history/detail/' . $his['store']['id'] . '/' . $his['transaction_id']) . '">';
        // $htmlhis .= '<div class="post__itam">';
        // $htmlhis .= '    <div class="content" style="padding: 20px 20px 15px;">';
        // $htmlhis .= '        <h3>LOTTE GROSIR ' . $his['store']['name'] . '</h3>';
        // $htmlhis .= '        <p style="color: #e80916;"> Transaksi ' . $his['transaction_id'];
        // $htmlhis .= '            <br> Metode Pembayaran ' . $his['payment']['name'] . ' </p>';
        // $htmlhis .= '        <div class="post-meta" style="padding: 15px;">';
        // $htmlhis .= '            <ul>';
        // $htmlhis .= '                <li>STATUS PENGIRIMAN : ' . $his['status']['name'] . '</li>';
        // $htmlhis .= '            </ul>';
        // // $htmlhis .= '            <ul>';
        // // $htmlhis .= '                <li>Pickup Date</li>';
        // // $htmlhis .= '                <li>' . date('D, M Y - H I s', strtotime($his['pickup_date'])) . '</li>';
        // // $htmlhis .= '            </ul>';
        // $htmlhis .= '            <ul>';
        // $htmlhis .= '                <li>Total Price</li>';
        // $htmlhis .= '                <li>Rp. ' . number_format($his['total_price']) . '</li>';
        // $htmlhis .= '            </ul>';
        // $htmlhis .= '        </div>';
        // $htmlhis .= '    </div>';
        // $htmlhis .= '</div>';
        // $htmlhis .= '</a>';

        $htmlhis .= '<a href="' . url('history/detail/' . $his['store']['id'] . '/' . $his['transaction_id']) . '">';
        $htmlhis .= '    <div class="post__itam">';
        $htmlhis .= '        <div class="content" style="padding: 20px 20px 15px;">';
        $htmlhis .= '            <div class="row">';
        $htmlhis .= '                <div class="col-md-6 col-sm-6 ol-lg-6"> ';
        $htmlhis .= '                    <p style="color: #e80916;"> ';
        $htmlhis .= '                        Transaksi ID : ' . $his['transaction_id'];         
        $htmlhis .= '                        <br> Metode Pembayaran : ' . $his['payment']['name'] . ' ';
        $htmlhis .= '                    </p>';
        $htmlhis .= '                </div>';
        $htmlhis .= '                <div class="col-md-6 col-sm-6 ol-lg-6" style="text-align: right;"> ';
        $htmlhis .= '                    <h3>LOTTE GROSIR ' . $his['store']['name'] . '</h3>';
        $htmlhis .= '                </div>';
        $htmlhis .= '            </div>';
        $htmlhis .= '            <div class="post-meta" style="padding: 15px;">';
        $htmlhis .= '                <ul>';
        $htmlhis .= '                    <li>STATUS : ' . $his['state']['name'] . ' ' . $his['status']['name'] . '</li>';
        $htmlhis .= '                </ul>';
        $htmlhis .= '                <ul>';
        $htmlhis .= '                    <li>Total Price</li>';
        $htmlhis .= '                    <li>Rp. ' . number_format($his['total_price']) . '</li>';
        $htmlhis .= '                </ul>';
        $htmlhis .= '            </div>';
        $htmlhis .= '        </div>';
        $htmlhis .= '    </div>';
        $htmlhis .= '</a>';

        return $htmlhis;
    }

    public function setstatus($transaction_id, $status)
    {
        $arrret = array(
            'res' => false,
            'title' => 'Konfirmasi Pesanan Gagal',
            'msg' => 'Mohon Lakukan Ulang',
        );

        $req['transaction_id'] = $transaction_id;
        $jsonreq = json_encode($req);
        // print_r($jsonreq);
        // die();

        $res = $this->service->api('/transaction', $jsonreq, session('token'));
        // print_r($res);
        // die();

        if ($res['code'] == 200) {
            $arrret = array(
                'res' => true,
                'msg' => 'Konfirmasi Pesanan Berhasil'
            );
        }

        return $arrret;
    }


    // public function generate_his_old($reshis, $arrstate)
    // {
    //     $datahis = [];
    //     $htmlhis = '';
    //     if ($reshis['code'] == 200) {
    //         foreach ($reshis['data'] as $his) {

    //             if (in_array($his['state']['id'], $arrstate)) {

    //                 $resprod = $this->service->get_history_detail($his['transaction_id']);
    //                 // print_r($resprod);
    //                 // die();

    //                 if ($resprod['code'] == 200) {

    //                     $datadetail = $resprod['data'];

    //                     $htmlhis .= '<div class="post__itam">';
    //                     $htmlhis .= '	<div class="content" style="padding: 20px 20px 15px;">';
    //                     $htmlhis .= '   <h3>LOTTE GROSIR ' . $his['store']['name'] . '</h3>';
    //                     $htmlhis .= '   <p style="color: #e80916;"> Transaksi ' . $his['transaction_id'] . '  <br> Metode Pembayaran ' . $datadetail['payment']['name'] . ' </p>';
    //                     $htmlhis .= '		<div class="post-meta" style="padding: 15px;">';
    //                     $htmlhis .= '			<ul>';
    //                     $htmlhis .= '				<li>STATUS PENGIRIMAN : ' . $his['status']['name'] . '</li>';
    //                     $htmlhis .= '			</ul>';

    //                     if ($his['status']['id'] == 13) {
    //                         $htmlhis .= '			<ul>';
    //                         $htmlhis .= '              <button type="button" data-url="' . $datadetail['payment_url'] . '" onclick="load_payment_modal($(this))" style="width: 100%;" class="btn-cust" div_now="div_tamu" href="javascript:void(0)" style="width: 100%;">
    //                                                 Lakukan Pembayaran
    //                                                 </button>';
    //                         $htmlhis .= '			</ul>';
    //                     }

    //                     $htmlhis .= '		</div>';

    //                     // foreach ($resprod['data']['products'] as $prod) {

    //                     //     $htmlhis .= '<div class="post__itam" id="his_number_' . $prod['id'] . '">';
    //                     //     $htmlhis .= '    <div class="content" style="padding: 20px 20px 15px;">';
    //                     //     $htmlhis .= '        <div class="row">';
    //                     //     $htmlhis .= '            <div class="col-md-3 col-sm-3 ol-lg-3">';
    //                     //     $htmlhis .= '                <div class="thumb">';
    //                     //     $htmlhis .= '                    <img src="' . $prod['image_url'] . '" alt="product images">';
    //                     //     $htmlhis .= '                </div>';
    //                     //         $htmlhis .= '            </div>';
    //                     //     $htmlhis .= '            <div class="col-md-9 col-sm-9 ol-lg-9">';
    //                     //     $htmlhis .= $prod['title'];
    //                     //     $htmlhis .= '                <br> <span style="color: #e80916;">Rp. ' . number_format($prod['price']) . '</span>';
    //                     //     $htmlhis .= '                <br>';
    //                     //     $htmlhis .= '                <input type="hidden" name="price_' . $prod['id'] . '" id="price_' . $prod['id'] . '" value="' . $prod['title'] . '"> <span id="notes_span_' . $prod['id'] . '">' . $prod['note'] . '</span>';
    //                     //     $htmlhis .= '                <input type="hidden" id="notes_' . $prod['id'] . '" name="his[' . $prod['id'] . '][note]" value="' . $prod['note'] . '"> ';
    //                     //     $htmlhis .= '            </div>';
    //                     //     $htmlhis .= '        </div>';
    //                     //     $htmlhis .= '    </div>';
    //                     //     $htmlhis .= '</div>';
    //                     // }
    //                 }


    //                 $htmlhis .= '		<div class="post__itam">';
    //                 $htmlhis .= '			<div class="content" style="padding: 20px 20px 15px;"> <span class="day" style="color: #e80916;">Ringkasan Belanja</span>';
    //                 $htmlhis .= '				<div class="post-meta">';
    //                 $htmlhis .= '					<ul>';
    //                 $htmlhis .= '						<li>Pickup Date</li>';
    //                 $htmlhis .= '						<li>' . date('d F Y - H i s', strtotime($his['pickup_date'])) . '</li>';
    //                 $htmlhis .= '					</ul>';
    //                 $htmlhis .= '					<ul>';
    //                 $htmlhis .= '						<li>Total Price</li>';
    //                 $htmlhis .= '						<li>Rp. ' . number_format($his['total_price']) . '</li>';
    //                 $htmlhis .= '					</ul>';
    //                 $htmlhis .= '				</div>';
    //                 $htmlhis .= '			</div>';
    //                 $htmlhis .= '		</div>';
    //                 $htmlhis .= '	</div>';
    //                 $htmlhis .= '</div>';

    //                 // print_r($htmlhis);
    //                 // die();
    //             }
    //         }
    //     }else{
    //         $htmlhis = 'History Not Found';
    //     }

    //     return $htmlhis;
    // }
}
