<?php

namespace App\Http\Controllers;

use App\Helpers\Helper;
use App\Service;
use Illuminate\Http\Request;

class HomeController extends Controller
{

    public function __construct()
    {
        $this->service = new Service();
        $this->data_header = array(
            'upsource' => array(),
            'down_plugins' => array(),
            'down_scripts' => array()
        );

    }

    public function index($store_id)
    {
        if (!session('token')) {
            Helper::logout();
            return redirect()->action('LoginController@index');
        }

        $session_store = $this->service->get_store_data($store_id);
        if (!$session_store) {
            Helper::logout();
            return redirect()->action('LoginController@index');
        }
        // print_r(session()->all());
        // die();
        #Include script
        $data_header = $this->data_header;

        //ui_sweetalert.html
        array_push($data_header['upsource'], asset('templates/plugins/bootstrap-sweetalert/sweetalert.css'));
        array_push($data_header['down_plugins'], asset('templates/plugins/bootstrap-sweetalert/sweetalert.min.js'));

        //custom js
        array_push($data_header['down_scripts'], asset('js/product.js'));

        //animate.css
        array_push($data_header['upsource'], asset('css/animate.css'));

        // autocomplete
        array_push($data_header['down_plugins'], asset('templates/plugins/EasyAutocomplete/jquery.easy-autocomplete.min.js'));
        array_push($data_header['upsource'], asset('templates/plugins/EasyAutocomplete/easy-autocomplete.min.css'));
        array_push($data_header['upsource'], asset('templates/plugins/EasyAutocomplete/easy-autocomplete.themes.min.css'));

        $source['upsource'] = $data_header['upsource'];
        $source['down_plugins'] = $data_header['down_plugins'];
        $source['down_scripts'] = $data_header['down_scripts'];

        // $rescat = $this->service->get_category($store_id);
        // print_r(session()->all());
        // die();
        
        $htmlres = Helper::storedata();
        // print_r($htmlres);
        // die();

        $arrcart = $this->service->get_cart();
        // print_r($arrcart);
        // die();
        if ($arrcart && $arrcart['code'] == 200) {
            $sum_cart = count($arrcart['data']['products']);
            session()->put('sum_cart', $sum_cart);
        }

        $arrpromo = $this->service->get_banner($store_id);
        // print_r($arrpromo);
        // die();
        
        if (is_array($arrpromo) && $arrpromo['code'] == 200) {
            $Allpromo = $arrpromo['data'];
        } else {
            $Allpromo = [];
        }

        // params get_product
        // 1. $store_id 
        // 2. $cat = category id, 
        // 3. $bblm = 1 if bblm, 
        // 4. $page = page ke-, 
        // 5. $name = for search

        $arrprod = $this->service->get_product($store_id);
        // print_r($arrprod);
        // die();

        $htmlprod = Helper::generate_product($store_id, $arrprod);
        // print_r($htmlprod);
        // die();

        return view('default', ['source' => $source, 'htmlStore' => $htmlres, 'arrpromo' => $Allpromo, 'htmlProd' => $htmlprod, 'search' => '']);
    }

    public function about()
    {
        #Include script
        $data_header = $this->data_header;

        //ui_sweetalert.html
        array_push($data_header['upsource'], asset('templates/plugins/bootstrap-sweetalert/sweetalert.css'));
        array_push($data_header['down_plugins'], asset('templates/plugins/bootstrap-sweetalert/sweetalert.min.js'));

        //animate.css
        array_push($data_header['upsource'], asset('css/animate.css'));

        $source['upsource'] = $data_header['upsource'];
        $source['down_plugins'] = $data_header['down_plugins'];
        $source['down_scripts'] = $data_header['down_scripts'];

        return view('about', ['data' => '', 'source' => $source]);
    }

    public function profile()
    {
        if (!session('token')) {
            Helper::logout();
            return redirect()->action('LoginController@index');
        }

        // print_r(session()->all());
        // die();
        #Include script
        $data_header = $this->data_header;

        //ui_sweetalert.html
        array_push($data_header['upsource'], asset('templates/plugins/bootstrap-sweetalert/sweetalert.css'));
        array_push($data_header['down_plugins'], asset('templates/plugins/bootstrap-sweetalert/sweetalert.min.js'));

        //custom js
        array_push($data_header['down_scripts'], asset('js/product.js'));

        //animate.css
        array_push($data_header['upsource'], asset('css/animate.css'));

        $source['upsource'] = $data_header['upsource'];
        $source['down_plugins'] = $data_header['down_plugins'];
        $source['down_scripts'] = $data_header['down_scripts'];

        // $rescat = $this->service->get_category($store_id);
        // print_r(session()->all());
        // die();
        
        $htmlres = Helper::storedata();
        // print_r($htmlres);
        // die();

        $arrcart = $this->service->get_cart();
        // print_r($arrcart);
        // die();
        if ($arrcart && $arrcart['code'] == 200) {
            $sum_cart = count($arrcart['data']['products']);
            session()->put('sum_cart', $sum_cart);
        }
        
        // params get_product
        // 1. $store_id 
        // 2. $cat = category id, 
        // 3. $bblm = 1 if bblm, 
        // 4. $page = page ke-, 
        // 5. $name = for search

        $arrprfile = $this->service->get_profile();
        // print_r($arrprfile);
        // die();

        if ($arrprfile['card_grade_id'] == 1) {
            $arrprfile['color'] = 'gold';
        }else if ($arrprfile['card_grade_id'] == 2) {
            $arrprfile['color'] = 'blue';
        } else if ($arrprfile['card_grade_id'] == 4) {
            $arrprfile['color'] = 'silver';
        }

        return view('profile', ['source' => $source, 'htmlStore' => $htmlres, 'profile' => $arrprfile, 'search' => '']);
    }

    public function skb()
    {
        #Include script
        $data_header = $this->data_header;

        //ui_sweetalert.html
        array_push($data_header['upsource'], asset('templates/plugins/bootstrap-sweetalert/sweetalert.css'));
        array_push($data_header['down_plugins'], asset('templates/plugins/bootstrap-sweetalert/sweetalert.min.js'));

        //animate.css
        array_push($data_header['upsource'], asset('css/animate.css'));

        $source['upsource'] = $data_header['upsource'];
        $source['down_plugins'] = $data_header['down_plugins'];
        $source['down_scripts'] = $data_header['down_scripts'];

        return view('skb', ['data' => '', 'source' => $source]);
    }

    public function privasi()
    {
        #Include script
        $data_header = $this->data_header;

        //ui_sweetalert.html
        array_push($data_header['upsource'], asset('templates/plugins/bootstrap-sweetalert/sweetalert.css'));
        array_push($data_header['down_plugins'], asset('templates/plugins/bootstrap-sweetalert/sweetalert.min.js'));

        //animate.css
        array_push($data_header['upsource'], asset('css/animate.css'));

        $source['upsource'] = $data_header['upsource'];
        $source['down_plugins'] = $data_header['down_plugins'];
        $source['down_scripts'] = $data_header['down_scripts'];

        return view('privasi', ['data' => '', 'source' => $source]);
    }

    public function faq()
    {
        #Include script
        $data_header = $this->data_header;

        //ui_sweetalert.html
        array_push($data_header['upsource'], asset('templates/plugins/bootstrap-sweetalert/sweetalert.css'));
        array_push($data_header['down_plugins'], asset('templates/plugins/bootstrap-sweetalert/sweetalert.min.js'));

        //animate.css
        array_push($data_header['upsource'], asset('css/animate.css'));

        $source['upsource'] = $data_header['upsource'];
        $source['down_plugins'] = $data_header['down_plugins'];
        $source['down_scripts'] = $data_header['down_scripts'];

        return view('faq', ['data' => '', 'source' => $source]);
    }

    public function get_cust_product($store_id, $page)
    {
        // params get_product
        // 1. $store_id 
        // 2. $cat = category id, 
        // 3. $bblm = 1 if bblm, 
        // 4. $page = page ke-, 
        // 5. $name = for search

        $arrprod = $this->service->get_product($store_id, '', '', $page);
        // print_r($arrprod);
        // die();

        $htmlprod = Helper::generate_product($store_id, $arrprod);
        print_r($htmlprod);
        die();

        echo json_encode($htmlprod);
    }

    public function category($store_id)
    {
        $rescat = $this->service->get_category($store_id);
        // print_r($rescat);
        // die();
        echo json_encode($rescat);
    }

    // public function generate_product($store_id, $cat = '', $bblm = '', $page = '', $name = '')
    // {
    //     $htmlprod = '';
    //     $first = 0;
    //     $sec = 0;


    //     foreach ($Allprod as $prod) {
    //         $url_photo = '';
    //         // $Allphotoprod = Product_photo::all()->toArray();
    //         $arrprodimg = $this->service->get_productimg($prod['product_id']);
    //         // print_r($arrprodimg);
    //         // die();
    //         if ($arrprodimg['code'] == 200) {
    //             $url_photo = $arrprodimg['data'][0]['image'];
    //         }

    //         if ($first == 0) {
    //             $htmlprod .= '<div class="single__product">';
    //             $htmlprod .= '        <div class="col-lg-3 col-md-4 col-sm-6 col-12">';
    //             $htmlprod .= '            <div class="product product__style--3">';
    //             $htmlprod .= '                <div class="product__thumb">';
    //             $htmlprod .= '                    <a class="first__img" href="' . action('ProductController@detail', ['store_id' => $store_id, 'product_id' => $prod['product_id']]) . '"><img src="' . $url_photo . '" alt="product image"></a>';
    //             $htmlprod .= '                    <a class="second__img animation1" href="' . action('ProductController@detail', ['store_id' => $store_id, 'product_id' => $prod['product_id']]) . '"><img src="' . $url_photo . '" alt="product image"></a>';
    //             $htmlprod .= '                    <div class="hot__box">';
    //             $htmlprod .= '                        <span class="hot-label">BBLM</span>';
    //             $htmlprod .= '                    </div>';
    //             $htmlprod .= '                </div>';
    //             $htmlprod .= '                <div class="product__content content--center content--center">';
    //             $htmlprod .= '                    <h4><a href="' . action('ProductController@detail', ['store_id' => $store_id, 'product_id' => $prod['product_id']]) . '">' . $prod['product_name'] . '</a></h4>';
    //             $htmlprod .= '                    <div class="product__hover--content">';
    //             $htmlprod .= '                        <h3><a href="' . action('ProductController@detail', ['store_id' => $store_id, 'product_id' => $prod['product_id']]) . '">Pilih</a></h3>';
    //             $htmlprod .= '                    </div>';
    //             $htmlprod .= '                </div>';
    //             $htmlprod .= '            </div>';
    //             $htmlprod .= '        </div>';

    //             $first = 1;
    //             $sec = 0;
    //         } elseif ($sec == 0) {
    //             $htmlprod .= '   <div class="col-lg-3 col-md-4 col-sm-6 col-12">';
    //             $htmlprod .= '            <div class="product product__style--3">';
    //             $htmlprod .= '                <div class="product__thumb">';
    //             $htmlprod .= '                    <a class="first__img" href="' . action('ProductController@detail', ['store_id' => $store_id, 'product_id' => $prod['product_id']]) . '"><img src="' . $url_photo . '" alt="product image"></a>';
    //             $htmlprod .= '                    <a class="second__img animation1" href="' . action('ProductController@detail', ['store_id' => $store_id, 'product_id' => $prod['product_id']]) . '"><img src="' . $url_photo . '" alt="product image"></a>';
    //             $htmlprod .= '                    <div class="hot__box">';
    //             $htmlprod .= '                        <span class="hot-label">BBLM</span>';
    //             $htmlprod .= '                    </div>';
    //             $htmlprod .= '                </div>';
    //             $htmlprod .= '                <div class="product__content content--center content--center">';
    //             $htmlprod .= '                    <h4><a href="' . action('ProductController@detail', ['store_id' => $store_id, 'product_id' => $prod['product_id']]) . '">' . $prod['product_name'] . '</a></h4>';
    //             $htmlprod .= '                    <div class="product__hover--content">';
    //             $htmlprod .= '                        <h3><a a href="' . action('ProductController@detail', ['store_id' => $store_id, 'product_id' => $prod['product_id']]) . '">Pilih</a></h3>';
    //             $htmlprod .= '                    </div>';
    //             $htmlprod .= '                </div>';
    //             $htmlprod .= '            </div>';
    //             $htmlprod .= '        </div>';
    //             $htmlprod .= '</div>';

    //             $first = 0;
    //             $sec = 1;
    //         }
    //     }

    //     return $htmlprod;
    // }
}
