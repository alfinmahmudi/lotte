<?php

namespace App\Http\Controllers;

use App\Helpers\Helper;
use App\Service;
use Illuminate\Http\Request;

class CartController extends Controller
{

    public function __construct()
    {
        $this->service = new Service();
        $this->data_header = array(
            'upsource' => array(),
            'down_plugins' => array(),
            'down_scripts' => array()
        );
    }

    public function index($store_id)
    {
        if (!session('token')) {
            Helper::logout();
            return redirect()->action('LoginController@index');
        }
        #Include script
        $data_header = $this->data_header;

        //ui_sweetalert.html
        array_push($data_header['upsource'], asset('templates/plugins/bootstrap-sweetalert/sweetalert.css'));
        array_push($data_header['down_plugins'], asset('templates/plugins/bootstrap-sweetalert/sweetalert.min.js'));

        //animate.css
        array_push($data_header['upsource'], asset('css/animate.css'));

        // for main cart js
        array_push($data_header['down_scripts'], asset('js/cart.js'));

        // numeral convert number js
        array_push($data_header['down_scripts'], asset('js/numeral.min.js'));

        //components_select2.html
        array_push($data_header['upsource'], asset('templates/plugins/select2/css/select2.min.css'));
        array_push($data_header['upsource'], asset('templates/plugins/select2/css/select2-bootstrap.min.css'));
        array_push($data_header['down_plugins'], asset('templates/plugins/select2/js/select2.full.min.js'));
        array_push($data_header['down_scripts'], asset('js/component_select2.js'));

        $source['upsource'] = $data_header['upsource'];
        $source['down_plugins'] = $data_header['down_plugins'];
        $source['down_scripts'] = $data_header['down_scripts'];

        $htmlres = Helper::storedata($store_id);
        // print_r($htmlres);
        // die();

        // $rescat = $this->service->get_category($store_id);
        // print_r($arrcategory);
        // die();

        $arrcart = $this->service->get_cart();
        // print_r($arrcart);
        // die();
        $htmlcart = '';

        $i = 0;
        $total_quant_cart = 0;
        $total_amount_cart = 0;
        $price_prod = 0;
        $decimal_price = 0;
        $decimal_total = 0;
        if ($arrcart['code'] == 200) {
            foreach ($arrcart['data']['products'] as $cart) {
                $is_bblm = $cart['is_bblm'];
                if ($cart['is_bblm'] && $cart['bblm_price'] != 0) {
                    $price_prod = $cart['bblm_price'];
                    $total_cart = $cart['bblm_total_price'];
                }else{
                    $price_prod = $cart['price'];
                    $total_cart = $cart['total_price'];
                }
                $decimal_price = strlen(substr(strrchr($price_prod, "."), 1));
                $decimal_total = strlen(substr(strrchr($total_cart, "."), 1));

                $htmlcart .= '<div class="post__itam" style="padding-top: 10px;" id="cart_number_' . $cart['product_id'] . '">';
                $htmlcart .= '    <div class="content" style="padding: 20px 20px 15px;">';
                $htmlcart .= '        <div class="post-meta">';
                $htmlcart .= '            <ul>';
                $htmlcart .= '                <li>';
                $htmlcart .= '                    <div class="box-tocart d-flex">';
                $htmlcart .= '                        <input class="checkbox-cust" id="chk_prod_' . $cart['product_id'] . '" name="CART[' . $cart['product_id'] . '][product_id]" value="' . $cart['product_id'] . '" checked type="checkbox"> </div>';
                $htmlcart .= '                </li>';
                $htmlcart .= '            </ul>';
                $htmlcart .= '        </div>';
                $htmlcart .= '        <div class="row">';
                $htmlcart .= '            <div class="col-md-3 col-sm-3 ol-lg-3">';
                $htmlcart .= '                <div class="thumb">';
                $htmlcart .= '                    <a href="' . url('/product/detail/' . $cart['store_id'] . '/' . $cart['product_id'] . '/' . $is_bblm) . '"><img src="' . $cart['image_small'] . '" alt="product images"></a>';
                $htmlcart .= '                </div>';
                $htmlcart .= '            </div>';
                $htmlcart .= '            <div class="col-md-9 col-sm-9 ol-lg-9">';
                $htmlcart .= '                <input type="hidden" class="total_cart" name="total_cart_' . $cart['product_id'] . '" id="total_cart_' . $cart['product_id'] . '" value="' . $total_cart . '">';
                $htmlcart .= '                <a href="' . url('/product/detail/' . $cart['store_id'] . '/' . $cart['product_id'] . '/' . $is_bblm) . '">' . $cart['name'] . '</a><br>';
                $htmlcart .= '                <br> <span id="price_txt_' . $cart['product_id'] . '" style="color: #e80916;">Rp. ' . number_format((float)$price_prod, $decimal_price, ',', '.') . '</span>';
                $htmlcart .= '                <br>';
                $htmlcart .= '                <input type="hidden" name="base_price_' . $cart['product_id'] . '" id="base_price_' . $cart['product_id'] . '" value="' . $cart['base_price'] . '"> <span id="notes_span_' . $cart['product_id'] . '">' . $cart['note'] . '</span>';
                $htmlcart .= '                <input type="hidden" id="notes_' . $cart['product_id'] . '" name="CART[' . $cart['product_id'] . '][note]" value="' . $cart['note'] . '">';
                $htmlcart .= '                <div class="post__time_cust"> <a href="javascript:void(0)" data-product="' . $cart['product_id'] . '" data-store="' . $cart['store_id'] . '" onclick="edit_notes($(this))"><span class="day" style="color: #e80916;">Edit Notes</span></a>';
                $htmlcart .= '                    <div class="post-meta">';
                $htmlcart .= '                        <ul>';
                $htmlcart .= '                            <li><a href="javascript:void(0)"><i class="fa fa-trash fa-lg" data-cart="' . $cart['product_id'] . '" data-prod="' . $cart['product_id'] . '" data-store="' . $cart['store_id'] . '" onclick="delete_qty($(this))"></i></a></li>';
                $htmlcart .= '                            <li><a href="javascript:void(0)"><i class="fa fa-minus fa-lg" data-prod="' . $cart['product_id'] . '" onclick="minus_qty_cart($(this), \'qty_' . $cart['product_id'] . '\', form_cart_all)"></i></a></li>';
                $htmlcart .= '                            <li>';
                $htmlcart .= '                                <div class="box-tocart d-flex">';
                $htmlcart .= '                                    <input id="qty_' . $cart['product_id'] . '" name="CART[' . $cart['product_id'] . '][total]" onchange="check_num($(this), \'' . $cart['product_id'] . '\', form_cart_all)" class="cart-box-cust" min="1" value="' . $cart['total'] . '" title="Qty" type="text"> </div>';
                $htmlcart .= '                            </li>';
                $htmlcart .= '                            <li><a href="javascript:void(0)"><i class="fa fa-plus fa-lg"  data-prod="' . $cart['product_id'] . '" onclick="plus_qty_cart($(this), \'qty_' . $cart['product_id'] . '\', form_cart_all)"></i></a></li>';
                $htmlcart .= '                        </ul>';
                $htmlcart .= '                    </div>';
                $htmlcart .= '                </div>';
                $htmlcart .= '            </div>';
                $htmlcart .= '        </div>';
                $htmlcart .= '    </div>';
                $htmlcart .= '</div>';

                $total_quant_cart = $total_quant_cart + $cart['total'];
                $total_amount_cart = $total_amount_cart + $total_cart;
                $i++;
            }   
        }else{
            return view('empty', ['source' => $source, 'htmlStore' => $htmlres, 'msg' => 'Cart Not Found']);
        }

        $rescart['htmlcart'] = $htmlcart;
        $rescart['total_quant_cart'] = $total_quant_cart;
        $rescart['total_amount_cart'] = number_format($total_amount_cart);

        return view('cart/cart', ['source' => $source, 'htmlStore' => $htmlres, 'datacart' => $rescart]);
    }

    public function get_data_prod($store_id, $product_id)
    {
        $arrproddet = $this->service->get_productdetail($store_id, $product_id);
        print_r($arrproddet);
        die();
    }

    public function checkout($store_id)
    {
        if (!session('token')) {
            Helper::logout();
            return redirect()->action('LoginController@index');
        }
        #Include script
        $data_header = $this->data_header;

        // date picker
        array_push($data_header['down_scripts'], asset('templates/plugins/bootstrap-datetimepicker/js/main.js'));
        array_push($data_header['down_plugins'], asset('templates/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js'));
        array_push($data_header['upsource'], asset('templates/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css'));

        //ui_sweetalert.html
        array_push($data_header['upsource'], asset('templates/plugins/bootstrap-sweetalert/sweetalert.css'));
        array_push($data_header['down_plugins'], asset('templates/plugins/bootstrap-sweetalert/sweetalert.min.js'));

        //animate.css
        array_push($data_header['upsource'], asset('css/animate.css'));

        // for main cart js
        array_push($data_header['down_scripts'], asset('js/cart.js'));

        //components_select2.html
        array_push($data_header['upsource'], asset('templates/plugins/select2/css/select2.min.css'));
        array_push($data_header['upsource'], asset('templates/plugins/select2/css/select2-bootstrap.min.css'));
        array_push($data_header['down_plugins'], asset('templates/plugins/select2/js/select2.full.min.js'));
        array_push($data_header['down_scripts'], asset('js/component_select2.js'));

        $source['upsource'] = $data_header['upsource'];
        $source['down_plugins'] = $data_header['down_plugins'];
        $source['down_scripts'] = $data_header['down_scripts'];

        $htmlres = Helper::storedata($store_id);
        // print_r($htmlres);
        // die();'htmlStore' => $htmlres,

        // $rescat = $this->service->get_category($store_id);
        // print_r(session()->all());
        // die();

        $arrcart = $this->service->get_cart();
        // print_r($arrcart);
        // die();

        $htmlcart = '';

        $i = 0;
        $total_quant_cart = 0;
        $total_amount_cart = 0;
        $price_prod = 0;
        $decimal_price = 0;
        $decimal_total = 0;
        if ($arrcart['code'] == 200) {
            foreach ($arrcart['data']['products'] as $cart) {
                $is_bblm = $cart['is_bblm'];
                if ($cart['is_bblm'] && $cart['bblm_price'] != 0) {
                    $price_prod = $cart['bblm_price'];
                    $total_cart = $cart['bblm_total_price'];
                }else{
                    $price_prod = $cart['price'];
                    $total_cart = $cart['total_price'];
                }
                $decimal_price = strlen(substr(strrchr($price_prod, "."), 1));
                $decimal_total = strlen(substr(strrchr($total_cart, "."), 1));

                $htmlcart .= '<div class="post__itam" style="padding-top: 10px;" id="cart_number_' . $cart['product_id'] . '">';
                $htmlcart .= '    <div class="row">';
                $htmlcart .= '        <div class="col-lg-3 col-sm-3">';
                $htmlcart .= '            <div class="thumb">';
                $htmlcart .= '                <a href="' . url('/product/detail/' . $cart['store_id'] . '/' . $cart['product_id'] . '/' . $is_bblm) . '"><img src="' . $cart['image_small'] . '" alt="product images"></a>';
                $htmlcart .= '            </div>';
                $htmlcart .= '        </div>';
                $htmlcart .= '        <div class="col-lg-9 col-sm-9">';
                $htmlcart .= '            <div class="content" style="padding: 20px 20px 15px;">';
                $htmlcart .= '                <input type="hidden" class="total_cart" name="total_cart_' . $cart['product_id'] . '" id="total_cart_' . $cart['product_id'] . '" value="10000"> <a href="' . url('/product/detail/' . $cart['store_id'] . '/' . $cart['product_id'] . '/' . $is_bblm) . '">' . $cart['name'] . '</a>';
                $htmlcart .= '                <br> <small>Rp. ' . number_format((float)$price_prod, $decimal_price, ',', '.') . '</small>';
                $htmlcart .= '                <input type="hidden" id="notes_' . $cart['product_id'] . '" name="CART[' . $cart['product_id'] . '][note]" value="' . $cart['note'] . '">';
                $htmlcart .= '                <br>';
                $htmlcart .= '                <input type="hidden" id="product_id_' . $cart['product_id'] . '" name="CART[' . $cart['product_id'] . '][product_id]" value="' . $cart['product_id'] . '">';
                $htmlcart .= '                <input type="hidden" id="weight_' . $cart['product_id'] . '" name="CART[' . $cart['product_id'] . '][weight]" value="' . $cart['weight'] . '">';
                $htmlcart .= '                <small>' . $cart['total'] . ' Barang</small>';
                $htmlcart .= '                <br><span style="color: #e80916;">Rp. ' . number_format((float)$total_cart, $decimal_total, ',', '.') . '</span>';
                $htmlcart .= '                <br>';
                $htmlcart .= '                <input type="hidden" name="base_price_' . $cart['product_id'] . '" id="base_price_' . $cart['product_id'] . '" value="' . $cart['price'] . '"> <span id="notes_span_' . $cart['product_id'] . '">' . $cart['note'] . '</span>';
                $htmlcart .= '            </div>';
                $htmlcart .= '        </div>';
                $htmlcart .= '    </div>';
                $htmlcart .= '</div>';

                //     print_r($htmlcart);
                // die();

                $total_quant_cart = $total_quant_cart + $total_cart;
                $total_amount_cart = $total_amount_cart + $total_cart;
                $i++;
            }
        } else {
            return view('empty', ['source' => $source, 'htmlStore' => $htmlres, 'msg' => 'Cart Not Found']);
        }

        $arrship = $this->service->get_ship();
        // print_r($arrship);
        // die();

        $htmlship = '<option value="0">&nbsp;</option>';
        if ($arrship['code'] == 200) {
            foreach ($arrship['data'] as $ship) {
                $htmlship .= '<option value="' . $ship['id'] . '">' . $ship['title'] . '</option>';
            }
        }

        $rescart['htmlcart'] = $htmlcart;
        $rescart['total_quant_cart'] = $total_quant_cart;
        $rescart['total_amount_cart'] = $total_amount_cart;

        return view('cart/checkout', ['source' => $source, 'htmlStore' => $htmlres, 'htmlship' => $htmlship, 'datacart' => $rescart]);
    }

    public function preview(Request $request)
    {
        // print_r($request->all());
        // die();

        if (!session('token')) {
            Helper::logout();
            return redirect()->action('LoginController@index');
        }

        if ($_SERVER['REQUEST_METHOD'] === 'GET')
        {
            return redirect()->action('CartController@checkout', [session('storedata.id')]);
        }

        #Include script
        $data_header = $this->data_header;

        // date picker
        array_push($data_header['down_scripts'], asset('templates/plugins/bootstrap-datetimepicker/js/main.js'));
        array_push($data_header['down_plugins'], asset('templates/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js'));
        array_push($data_header['upsource'], asset('templates/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css'));

        //ui_sweetalert.html
        array_push($data_header['upsource'], asset('templates/plugins/bootstrap-sweetalert/sweetalert.css'));
        array_push($data_header['down_plugins'], asset('templates/plugins/bootstrap-sweetalert/sweetalert.min.js'));

        //animate.css
        array_push($data_header['upsource'], asset('css/animate.css'));

        // for main cart js
        array_push($data_header['down_scripts'], asset('js/cart.js'));

        //components_select2.html
        array_push($data_header['upsource'], asset('templates/plugins/select2/css/select2.min.css'));
        array_push($data_header['upsource'], asset('templates/plugins/select2/css/select2-bootstrap.min.css'));
        array_push($data_header['down_plugins'], asset('templates/plugins/select2/js/select2.full.min.js'));
        array_push($data_header['down_scripts'], asset('js/component_select2.js'));

        $source['upsource'] = $data_header['upsource'];
        $source['down_plugins'] = $data_header['down_plugins'];
        $source['down_scripts'] = $data_header['down_scripts'];

        $htmlres = Helper::storedata($request['store_id']);
        // print_r($htmlres);
        // die();

        // $rescat = $this->service->get_category($request['store_id']);
        // print_r($arrdata);
        // die();

        $arrpayment = $this->service->get_payment();
        // print_r($arrship);
        // die();
        $htmlpayment = '<option value="0">&nbsp;</option>';
        if ($arrpayment['code'] == 200) {
            foreach ($arrpayment['data'] as $pay) {
                $htmlpayment .= '<option value="' . $pay['id'] . '">' . $pay['name'] . '</option>';
            }
        }

        $arrcart = $this->service->get_cart();
        // print_r($arrcart);
        // die();

        if ($arrcart['code'] == 200) {
            $htmlcart = '';

            $i = 0;
            $total_quant_cart = 0;
            $total_amount_cart = 0;
            $price_prod = 0;
            foreach ($arrcart['data']['products'] as $cart) {
                $is_bblm = $cart['is_bblm'];
                if ($cart['is_bblm'] && $cart['bblm_price'] != 0) {
                    $price_prod = $cart['bblm_price'];
                    $total_cart = $cart['bblm_total_price'];
                }else{
                    $price_prod = $cart['price'];
                    $total_cart = $cart['total_price'];
                }
                $decimal_price = strlen(substr(strrchr($price_prod, "."), 1));
                $decimal_total = strlen(substr(strrchr($total_cart, "."), 1));

                $htmlcart .= '<div class="post__itam" style="padding-top: 10px;" id="cart_number_' . $cart['product_id'] . '">';
                $htmlcart .= '    <div class="row">';
                $htmlcart .= '        <div class="col-lg-3 col-sm-3">';
                $htmlcart .= '            <div class="thumb">';
                $htmlcart .= '                <a href="' . url('/product/detail/' . $cart['store_id'] . '/' . $cart['product_id'] . '/' . $is_bblm) . '"><img src="' . $cart['image_small'] . '" alt="product images"></a>';
                $htmlcart .= '            </div>';
                $htmlcart .= '        </div>';
                $htmlcart .= '        <div class="col-lg-9 col-sm-9">';
                $htmlcart .= '            <div class="content" style="padding: 20px 20px 15px;">';
                $htmlcart .= '                <input type="hidden" class="total_cart" name="total_cart_' . $cart['product_id'] . '" id="total_cart_' . $cart['product_id'] . '" value="10000"> <a href="' . url('/product/detail/' . $cart['store_id'] . '/' . $cart['product_id'] . '/' . $is_bblm) . '">' . $cart['name'] . '</a>';
                $htmlcart .= '                <br> <small>Rp. ' . number_format((float)$price_prod, $decimal_price, ',', '.') . '</small>';
                $htmlcart .= '                <input type="hidden" id="notes_' . $cart['product_id'] . '" name="CART[' . $cart['product_id'] . '][note]" value="' . $cart['note'] . '">';
                $htmlcart .= '                <br>';
                $htmlcart .= '                <input type="hidden" id="product_id_' . $cart['product_id'] . '" name="CART[' . $cart['product_id'] . '][product_id]" value="' . $cart['product_id'] . '">';
                $htmlcart .= '                <input type="hidden" id="weight_' . $cart['product_id'] . '" name="CART[' . $cart['product_id'] . '][weight]" value="' . $cart['weight'] . '">';
                $htmlcart .= '                <small>' . $cart['total'] . ' Barang</small>';
                $htmlcart .= '                <br><span style="color: #e80916;">Rp. ' . number_format((float)$total_cart, $decimal_total, ',', '.') . '</span>';
                $htmlcart .= '                <br>';
                $htmlcart .= '                <input type="hidden" name="base_price_' . $cart['product_id'] . '" id="base_price_' . $cart['product_id'] . '" value="' . $cart['price'] . '"> <span id="notes_span_' . $cart['product_id'] . '">' . $cart['note'] . '</span>';
                $htmlcart .= '            </div>';
                $htmlcart .= '        </div>';
                $htmlcart .= '    </div>';
                $htmlcart .= '</div>';

                //     print_r($htmlcart);
                // die();

                $total_quant_cart = $total_quant_cart + $total_cart;
                $total_amount_cart = $total_amount_cart + $total_cart;
                $i++;
            }
        } else {
            return view('empty', ['source' => $source, 'htmlStore' => $htmlres, 'msg' => 'Cart Not Found']);
        }

        $rescart['additional_info'] = '';
        $rescart['expidition'] = '';
        $temp_addrs = '';

        $rescart['shipp_method_txt'] = $request['shipp_method_txt'];
        $rescart['shipp_method'] = $request['shipp_method'];
        $rescart['customer_address_id'] = 0;

        if ($rescart['shipp_method'] == 2) {
            $rescart['customer_address_id'] = $request['customer_address_id'];
            $arraddrs = $this->service->check_address($rescart['customer_address_id']);
            // print_r($arraddrs);
            // die();

            $temp_addrs .= '<tr>';
            $temp_addrs .= '    <td>Durasi Pengiriman</td>';
            $temp_addrs .= '    <td>:</td>';
            $temp_addrs .= '    <td>';
            $temp_addrs .= $request['durasi_txt'];
            $temp_addrs .= '</td>';
            $temp_addrs .= '</tr>';

            if ($arraddrs['code'] == 200) {
                $dataaddrs = $arraddrs['data'];
                $temp_addrs .= '<tr>';
                $temp_addrs .= '    <td>Penerima</td>';
                $temp_addrs .= '    <td>:</td>';
                $temp_addrs .= '    <td>';

                $temp_addrs .= '<input type="hidden" name="zip_code_origin" id="zip_code_origin" value="' . $dataaddrs['zip_code'] . '">';
                $temp_addrs .= '<input type="hidden" name="customer_address_id" id="customer_address_id" value="' . $dataaddrs['id'] . '">';
                $temp_addrs .= '<div class="post_header">';
                $temp_addrs .= '    <div>';
                $temp_addrs .= '        <ul>';
                $temp_addrs .= '            <li>' . $dataaddrs['customer_name'] . ' <small style="color:#e80916;">' . $dataaddrs['title'] . '</small></li>';
                $temp_addrs .= '        </ul>';
                $temp_addrs .= '    </div>';
                $temp_addrs .= '</div>';
                $temp_addrs .= '<div class="post_content"> <p>';
                $temp_addrs .= $dataaddrs['phone'] . ' <br> ';
                $temp_addrs .= $dataaddrs['address'] . ' <br> ';
                $temp_addrs .= $dataaddrs['regency']['name'] . ', ' . $dataaddrs['province']['name'] . ', ' . $dataaddrs['zip_code'] . ' </p>';
                $temp_addrs .= '</div>';

                $temp_addrs .= '</td>';
                $temp_addrs .= '</tr>';
            }

            $rescart['additional_info'] = $temp_addrs;
            $rescart['li_up_deliv'] = '<li id="li_txt_deliv">Ongkos Kirim</li>';
            $rescart['li_mid_deliv'] = '<li id="li_txt_deliv"><strong>Rp. </strong></li>';
            $rescart['li_down_deliv'] = '<li id="total_amount_delivery">' . number_format($request['real_delivery']) . '</li>';

            $disc_ongkir = $request['total_delivery'] - $request['real_delivery'];
            if ($disc_ongkir != 0) {
                $rescart['li_up_deliv_disc'] = '<li id="li_txt_deliv_disc">Diskon Ongkos Kirim</li>';
                $rescart['li_mid_deliv_disc'] = '<li id="li_txt_deliv_disc"><strong>Rp. </strong></li>';
                $rescart['li_down_deliv_disc'] = '<li id="total_amount_delivery_disc">' . number_format($request['total_delivery']) . '</li>';    
            }else{
                $rescart['li_up_deliv_disc'] = '<li id="li_txt_deliv_disc">Diskon Ongkos Kirim</li>';
                $rescart['li_mid_deliv_disc'] = '<li id="li_txt_deliv_disc"><strong>Rp. </strong></li>';
                $rescart['li_down_deliv_disc'] = '<li id="total_amount_delivery_disc">0</li>';
            }
            
        } elseif ($rescart['shipp_method'] == 1) {
            $rescart['customer_address_id'] = session('dataaddress.id');
            $rescart['additional_info'] = $temp_addrs;
            $rescart['li_up_deliv'] = '';
            $rescart['li_mid_deliv'] = '';
            $rescart['li_down_deliv'] = '';

            $rescart['li_up_deliv_disc'] = '';
            $rescart['li_mid_deliv_disc'] = '';
            $rescart['li_down_deliv_disc'] = '';
        }

        $rescart['htmlcart'] = $htmlcart;
        $rescart['total_quant_cart'] = $total_quant_cart;
        $rescart['sub_total_price'] = $total_amount_cart;
        $total_deliv = (int) str_replace(",", "", $request['total_delivery']);
        $rescart['deliv_price'] = $request['total_delivery'];
        $rescart['total_price'] = (int) str_replace(",", "", $request['total_amount_cart']);
        $rescart['expidition'] = $request['expidition'];
        $rescart['durasi'] = $request['durasi'];
        $rescart['real_delivery'] = $request['real_delivery'];
        $rescart['total_diskon'] = $request['total_diskon'];

        return view('cart/preview', ['source' => $source, 'htmlStore' => $htmlres, 'htmlpayment' => $htmlpayment, 'datacart' => $rescart]);
    }

    public function add_to_cart(Request $request)
    {
        $arrret = array(
            'res' => false,
            'msg' => 'Failed Add To Cart',
            'datacart' => ''
        );

        $arrinsert = $request->input('DATA');
        $arrinsert['total'] = (int)$arrinsert['total'];
        $arrinsert = json_encode($arrinsert);
        // print_r($arrinsert);
        // die();

        $respost = $this->service->api('/cart/product', $arrinsert, session('token'));
        // print_r($respost);
        // die();

        if ($respost['code'] == 200) {
            $arrcart = $this->service->get_cart();
            if ($arrcart['code'] == 200) {
                $sum_cart = count($arrcart['data']['products']);
                session()->put('sum_cart', $sum_cart);
                $datacart = $this->gen_cart_html($arrcart);

                $arrret = array(
                    'res' => true,
                    'msg' => 'Success Add To Cart',
                    'datacart' => $datacart
                );
            }
        }

        return $arrret;
    }

    public function add_to_cart_again(Request $request)
    {
        $arrret = array(
            'res' => false,
            'msg' => 'Failed Add To Cart',
            'datacart' => ''
        );

        $arrinsert = $request->input('DATA');
        $flags = false;
        // print_r($arrinsert);
        // die();

        foreach ($arrinsert as $insert) {
            $insert['total'] = (int)$insert['total'];
            $jsonInsert = json_encode($insert);
            $respost = $this->service->api('/cart/product', $jsonInsert, session('token'));
            // print_r($respost);
            // die();

            if ($respost['code'] == 200) {
                $flags = true;
            }
        }

        if ($flags) {
            $arrcart = $this->service->get_cart();
            if ($arrcart['code'] == 200) {
                $sum_cart = count($arrcart['data']['products']);
                session()->put('sum_cart', $sum_cart);
                $datacart = $this->gen_cart_html($arrcart);
                $arrret = array(
                    'res' => true,
                    'msg' => 'Success Add To Cart',
                    'datacart' => $datacart
                );   
            }
        }

        return $arrret;
    }

    public function get_cart()
    {
        $arrret = array(
            'res' => false,
            'msg' => 'Failed Get Cart',
            'datacart' => ''
        );

        $htmlprod = '';

        $arrcart = $this->service->get_cart();
        // print_r($arrcart);
        // die();
        $datacart = $this->gen_cart_html($arrcart);
        if (count($datacart) > 0) {
            $arrret = array(
                'res' => true,
                'msg' => '',
                'datacart' => $datacart
            );   
        }

        return $arrret;
    }

    public function gen_cart_html($arrcart)
    {
        $arrret = [];
        $htmlprod = '';
        $total_quant_cart = 0;
        $total_amount_cart = 0;
        $price_prod = 0;
        if ($arrcart['code'] == 200) {
            if (count($arrcart['data']['products']) > 0) {
                foreach ($arrcart['data']['products'] as $cart) {
                    $total_cart = 0;
                    $is_bblm = $cart['is_bblm'];
                    if ($cart['is_bblm'] && $cart['bblm_price'] != 0) {
                        $price_prod = $cart['bblm_price'];
                        $total_cart = $cart['bblm_total_price'];
                    }else{
                        $price_prod = $cart['price'];
                        $total_cart = $cart['total_price'];
                    }
                    $decimal_price = strlen(substr(strrchr($price_prod, "."), 1));
                    $decimal_total = strlen(substr(strrchr($total_cart, "."), 1));
                    
                    $htmlprod .= '<div class="item01 d-flex">';
                    $htmlprod .= '    <div class="thumb">';
                    $htmlprod .= '        <a href="' . url('/product/detail/' . $arrcart['data']['store']['id'] . '/' . $cart['product_id'] . '/' . $is_bblm) . '"><img src="' . $cart['image_small'] . '" alt="product images"></a>';
                    $htmlprod .= '    </div>';
                    $htmlprod .= '    <div class="content">';
                    $htmlprod .= '        <h6><a href="' . url('/product/detail/' . $arrcart['data']['store']['id'] . '/' . $cart['product_id'] . '/' . $is_bblm) . '">'  . $cart['name'] . '</a></h6>';
                    $htmlprod .= '        <span class="prize">'  . number_format((float)$price_prod, $decimal_price, ',', '.') . '</span>';
                    $htmlprod .= '        <div class="product_prize d-flex justify-content-between">';
                    $htmlprod .= '            <span class="qun">Qty: '  . $cart['total'] . '</span>';
                    $htmlprod .= '        </div>';
                    $htmlprod .= '    </div>';
                    $htmlprod .= '</div>';

                    $total_quant_cart = $total_quant_cart + $total_cart;
                    $total_amount_cart = $total_amount_cart + $total_cart;
                }


                $arrret['store_id'] = $arrcart['data']['store']['id'];
                $arrret['htmlprod'] = $htmlprod;
                $arrret['total_item'] = count($arrcart['data']['products']);
                $arrret['total_quant_cart'] = $total_quant_cart;
                $arrret['total_amount_cart'] = $total_amount_cart;
                $arrret['total_amount_cart_num'] = number_format($total_amount_cart);
            }
        }

        return $arrret;
    }

    public function delete_cart($store_id, $product_id)
    {
        $arrret = array(
            'res' => false,
            'msg' => 'Delete Cart Failed'
        );

        $arrdelete['store_id'] = $store_id;
        $arrdelete['product_id'] = $product_id;

        $arrdelete = json_encode($arrdelete);
        // print_r($arrdelete);
        // die();

        $arrres = $this->service->api_delete('/cart/product', $arrdelete);
        // print_r($arrres);
        // die();
        if ($arrres['code'] == 200) {
            $arrret = array(
                'res' => true,
                'msg' => 'Delete Cart Success'
            );
        }

        $arrcart = $this->service->get_cart();
        // print_r($arrcart);
        // die();
        if ($arrcart['code'] == 200) {
            $sum_cart = count($arrcart['data']['products']);
            session()->put('sum_cart', $sum_cart);
        }

        return $arrret;
    }

    public function delete_cart_all()
    {
        $arrret = array(
            'res' => false,
            'msg' => 'Delete Cart Failed'
        );

        $arrres = $this->service->api_delete_all('/cart');
        // print_r($arrres);
        // die();
        if ($arrres['code'] == 200) {
            session()->put('sum_cart', 0);
            $arrret = array(
                'res' => true,
                'msg' => 'Delete Cart Success',
                'url' => url('store/home')
            );
        }

        return $arrret;
    }

    public function update_notes(Request $request)
    {
        $arrret = array(
            'res' => false,
            'msg' => 'Update Cart Failed'
        );

        // print_r($request->all());
        // die();

        $arrupdt['store_id'] = $request->input('store_modal');
        $arrupdt['product_id'] = $request->input('product_modal');
        $arrupdt['total'] = (int)$request->input('qty_modal');
        $arrupdt['note'] = $request->input('note_modal');

        $arrupdt = json_encode($arrupdt);

        $arrres = $this->service->api_update('/cart/product', $arrupdt);
        // print_r($arrres);
        // die();
        if ($arrres['code'] == 200) {
            $arrret = array(
                'res' => true,
                'msg' => 'Update Cart Success'
            );
        }

        return $arrret;
    }

    public function set_final(Request $request)
    {
        // print_r($request->all());
        // die();
        $i = 0;
        $store_id = $request->input('store_id');
        $datacart = $request->input('CART');
        $flag_preview = $request->input('flag');

        $arrret = array(
            'res' => true,
            'msg' => 'Process Cart Success',
            'url' => url('/cart/checkout/' . $store_id)
        );

        foreach ($datacart as $cart) {
            if (array_key_exists('product_id', $cart)) {
                $cart['store_id'] = $store_id;
                $cart['total'] = (int)$cart['total'];
                // print_r($cart);
                // die();     
                $arrupdt = json_encode($cart);

                $arrres = $this->service->api_update('/cart/product', $arrupdt);
                // print_r($arrres);
                if ($arrres['code'] != 200) {
                    $arrret = array(
                        'res' => false,
                        'msg' => 'Process Cart Failed'
                    );
                    return $arrret;
                }
            }else{
                $arrprod = array_keys($datacart);
                $arrdelete['store_id'] = $store_id;
                $arrdelete['product_id'] = (string)$arrprod[$i];
                // print_r($arrdelete);
                // die();

                $arrdelete = json_encode($arrdelete);

                $arrres = $this->service->api_delete('/cart/product', $arrdelete);
                // print_r($arrres);

                if ($arrres['code'] != 200) {
                    $arrret = array(
                        'res' => false,
                        'msg' => 'Process Cart Failed'
                    );
                    return $arrret;
                }
            }
            $i++;
        }

        $arrcart = $this->service->get_cart();
        // print_r($arrcart);
        // die();
        if ($arrcart['code'] == 200) {
            $sum_cart = count($arrcart['data']['products']);
            session()->put('sum_cart', $sum_cart);
        }

        if ($flag_preview == 1) {
            echo json_encode($arrcart['data']['products']);
        }else{
            return $arrret;
        }
    }

    public function get_delivery_attr()
    {

        $arrexpi = $this->service->get_expedition();
        // print_r($arrexpi);
        // die();

        $htmlexpi = '<option value="0">&nbsp;</option>';

        if ($arrexpi['code'] == 200) {
                
            foreach ($arrexpi['data'] as $expi) {

                $htmlexpi .= '<option value="' . $expi['Id'] . '">' . $expi['Name'] . '</option>';

            }

        }

        $htmladdrs = '';
        $arraddrs = session('dataaddress');
        if (count($arraddrs) > 0 && $arraddrs['id'] != '') {

            $htmladdrs .= '<input type="hidden" name="zip_code_origin" id="zip_code_origin" value="' . $arraddrs['zip_code'] . '">';
            $htmladdrs .= '<div class="post_header">';
            $htmladdrs .= '    <div>';
            $htmladdrs .= '        <ul>';
            $htmladdrs .= '            <li>' . $arraddrs['customer_name'] . ' <small style="color:#e80916;">' . $arraddrs['title'] . '</small></li>';
            $htmladdrs .= '        </ul>';
            $htmladdrs .= '    </div>';
            $htmladdrs .= '</div>';
            $htmladdrs .= '<div class="post_content" style="border-bottom: 1px solid black;"> <p>';
            $htmladdrs .= $arraddrs['phone'] . ' <br> ';
            $htmladdrs .= $arraddrs['address'] . ' <br> ';
            $htmladdrs .= $arraddrs['regency']['name'] . ', ' . $arraddrs['province']['name'] . ', ' . $arraddrs['zip_code'] . ' </p>';
            $htmladdrs .= '</div>';

        }else{
            $arrret = array(
                'res' => false,
                'msg' => 'Please Choose The Default Address',
                'url' => url('/login/get_full_address')
            );
            return $arrret;
        }

        $arrdata['htmlexpi'] = $htmlexpi;
        $arrdata['htmladdrs'] = $htmladdrs;
        $arrdata['address_id'] = $arraddrs['id'];

        $arrret = array(
            'res' => true,
            'html' => $arrdata,
        );

        return $arrret;
    }

    public function set_chenge_addrs(Request $request)
    {
        // print_r($request->all());
        // die();
        $product_id = $request->input('PROD');
        $arraddrs = $this->service->check_address($product_id[0]);
        // print_r($arraddrs);
        // die();

        if ($arraddrs['code'] != 200) {
            $arrret = array(
                'res' => false,
                'msg' => 'Get Data Failed',
            );
            return $arrret;
        }

        $htmladdrs = '';

        if ($arraddrs['code'] == 200) {
            $dataaddrs = $arraddrs['data'];

            $htmladdrs .= '<input type="hidden" name="zip_code_origin" id="zip_code_origin" value="' . $dataaddrs['zip_code'] . '">';
            $htmladdrs .= '<div class="post_header">';
            $htmladdrs .= '    <div>';
            $htmladdrs .= '        <ul>';
            $htmladdrs .= '            <li>' . $dataaddrs['customer_name'] . ' <small style="color:#e80916;">' . $dataaddrs['title'] . '</small></li>';
            $htmladdrs .= '        </ul>';
            $htmladdrs .= '    </div>';
            $htmladdrs .= '</div>';
            $htmladdrs .= '<div class="post_content" style="border-bottom: 1px solid black;"> <p>';
            $htmladdrs .= $dataaddrs['phone'] . ' <br> ';
            $htmladdrs .= $dataaddrs['address'] . ' <br> ';
            $htmladdrs .= $dataaddrs['regency']['name'] . ', ' . $dataaddrs['province']['name'] . ', ' . $dataaddrs['zip_code'] . ' </p>';
            $htmladdrs .= '</div>';
        }

        $arrdata['htmladdrs'] = $htmladdrs;
        $arrdata['address_id'] = $arraddrs['data']['id'];

        $arrret = array(
            'res' => true,
            'html' => $arrdata,
        );

        return $arrret;
    }

    public function change_addrs_deliv()
    {
        $arraddrs = $this->service->check_address();
        // print_r($arraddrs);
        // die();

        if ($arraddrs['code'] != 200) {
            $arrret = array(
                'res' => false,
                'msg' => 'Get Data Failed',
            );
            return $arrret;
        }

        $htmladdrs = '';

        if ($arraddrs['code'] == 200) {

            foreach ($arraddrs['data'] as $dataaddrs) {
                $htmladdrs .= '<div class="post-meta">';
                $htmladdrs .= '<ul>';
                $htmladdrs .= '    <li>';
                $htmladdrs .= '        <div class="box-tocart d-flex">';
                $htmladdrs .= '            <input class="checkbox-cust" id="chk_prod" name="PROD[]" value="' . $dataaddrs['id'] . '" type="checkbox">';
                $htmladdrs .= '        </div>';
                $htmladdrs .= '    </li>';
                $htmladdrs .= '</ul>';
                $htmladdrs .= '</div>';
                $htmladdrs .= '<div class="post_header">';
                $htmladdrs .= '        <ul>';
                $htmladdrs .= '            <li>' . $dataaddrs['customer_name'] . ' <small style="color:#e80916;">' . $dataaddrs['title'] . '</small></li>';
                $htmladdrs .= '        </ul>';
                $htmladdrs .= '</div>';
                $htmladdrs .= '<div class="post_content"> <p>';
                $htmladdrs .= $dataaddrs['phone'] . ' <br> ';
                $htmladdrs .= $dataaddrs['address'] . ' <br> ';
                $htmladdrs .= $dataaddrs['regency']['name'] . ', ' . $dataaddrs['province']['name'] . ', ' . $dataaddrs['zip_code'] . ' </p>';
                $htmladdrs .= '</div>';  
            }

        }

        $arrdata['htmladdrs'] = $htmladdrs;

        $arrret = array(
            'res' => true,
            'html' => $arrdata
        );

        return $arrret;
    }

    public function get_durasi($service_id)
    {
        $arrret = array(
            'res' => false,
            'msg' => 'Delete Cart Failed'
        );

        $arrexpi = $this->service->get_expedition();
        // print_r($arrexpi);
        // die();
        $htmlexpi = '';

        if ($arrexpi['code'] == 200) {

            foreach ($arrexpi['data'] as $expi) {
                if ($expi['Id'] == $service_id) {
                    foreach ($expi['Services'] as $service) {
                        // $htmlexpi .= '<option value="' . $expi['Id'] . '-' . $service['code'] . '-' . $service['id'] . '">' . $service['name'] . ' - ' . $service['description'] . '</option>';
                        // $htmlexpi .= '<option value="' . $service['id'] . '-' . $service['code'] . '">' . $service['name'] . ' - ' . $service['description'] . '</option>';
                        
                        $htmlexpi .= '<div class="post-meta">';
                        $htmlexpi .= '<ul>';
                        $htmlexpi .= '    <li>';
                        $htmlexpi .= '        <div class="box-tocart d-flex">';
                        $htmlexpi .= '            <input class="checkbox-cust" data-action="' . url('/cart/get_delivery_price') . '" onclick="check_price($(this), form_deliv)" name="durasi" id="durasi" value="' . $service['id'] . '-' . $service['code'] . '" type="checkbox">';
                        $htmlexpi .= '        </div>';
                        $htmlexpi .= '    </li>';
                        $htmlexpi .= '</ul>';
                        $htmlexpi .= '</div>';
                        $htmlexpi .= '<div class="post_header">';
                        $htmlexpi .= '    <div>';
                        $htmlexpi .= '        <ul>';
                        $htmlexpi .= '            <li id="durasi_txt_' . $service['id'] . '">' . $service['name'] . '</li>';
                        $htmlexpi .= '        </ul>';
                        $htmlexpi .= '    </div>';
                        $htmlexpi .= '</div>';
                        $htmlexpi .= '<div class="post_content" style="border-bottom: 1px solid black;"> <p>';
                        $htmlexpi .= $service['description'] . ' </p>';
                        $htmlexpi .= '</div>';
                    }
                }
            }
        }

        $arrdata['htmlexpi'] = $htmlexpi;
        $arrret = array(
            'res' => true,
            'html' => $arrdata,
        );

        return $arrret;
    }

    public function get_delivery_price(Request $request)
    {
        // print_r($request->all());
        // die();

        $arrcheck = [];
        $amount_cart = 0;
        $weight = 0;
        $store_id = $request->input('store_id');
        $datacart = $request->input('CART');
        $customer_address_id = $request->input('customer_address_id');
        $store_zip_code = $request->input('store_zip_code');
        $zip_code_origin = $request->input('zip_code_origin');
        $durasi = $request->input('durasi');
        $arrtype = explode('-', $durasi);

        foreach ($datacart as $cart) {
            $weight = $weight + $cart['weight'];
        }

        $arrcheck['customer_address_id'] = $customer_address_id;
        $arrcheck['store_id'] = $store_id;
        $arrcheck['zip_code_origin'] = $store_zip_code;
        $arrcheck['zip_code_destination'] = $zip_code_origin;
        $arrcheck['type'] = $arrtype[1];
        $arrcheck['weight'] = (int)$weight;

        $arrcheck = json_encode($arrcheck);
        // print_r($arrcheck);
        // die();

        $respost = $this->service->api('/shipping/expedition', $arrcheck, session('token'));
        // print_r($respost);
        // die();

        if ($respost['code'] != 200) {
            $arrret = array(
                'res' => false,
                'title' => 'Mohon maaf jarak maksimum pengiriman 15 kilometer',
                'msg' => 'Anda dapat memilih toko terdekat untuk tetap terlayani. Selamat berbelanja kembali'
            );
            return $arrret;
        }

        $price = 0;
        $disc = 0;
        if ($respost['data']['is_promo'] == 1) {
            $price = $respost['data']['price'];
            $disc = $respost['data']['price'] - $respost['data']['discount_price'];
        }else{
            $price = $respost['data']['price'];
            $disc = 0;
        }

        $amount_cart = str_replace(',', '', $request->input('total_amount_cart')) + $price - $disc;
        $arrdata['amount_disc'] = $disc;
        $arrdata['amount_deliv'] = $price;
        $arrdata['amount_cart'] = $amount_cart;
        // print_r($arrdata);
        // die();
        $arrret = array(
            'res' => true,
            'html' => $arrdata
        );

        return $arrret;

    }

    public function checkout_final(Request $request)
    {
        // print_r($request->all());
        // die();

        $arrret = array(
            'res' => false,
            'msg' => 'Failed To Checkout',
            'url' => ''
        );

        $arrdeliv = [];
        $shipp_method = $request->input('shipp_method');
        $expidition = $request->input('expidition');
        $durasi = $request->input('durasi');
        $arrexp = explode('-', $durasi);
        $arrinsert['payment_channel_id'] = (int)$request->input('payment_method');
        $arrinsert['shipping']['id'] = (int)$shipp_method;
        $arrinsert['customer_address_id'] = $request->input('customer_address_id');
        
        if ($shipp_method == 2) {
            $arrdeliv['id'] = (int) $expidition;
            $price = $request->input('total_delivery');
            $price = str_replace(',', '', $price);
            $arrdeliv['price'] = (int)$price;
            $arrdeliv['service_id'] = (int)$arrexp[0];
            $arrdeliv['service_code'] = $arrexp[1];

            $arrinsert['shipping']['delivery'] = $arrdeliv;
        }else{
            // $arrinsert['shipping']['pickup_at'] = $request->input('pickup_at');
            $arrinsert['shipping']['pickup_at'] = '';
        }
        
        $arrinsert = json_encode($arrinsert);
        // print_r($arrinsert);
        // die();

        $respost = $this->service->api('/checkout', $arrinsert, session('token'));
        // print_r($respost);
        // die();

        if ($respost['code'] == 200) {
            session()->forget('sum_cart');
            $arrret = array(
                'res' => true,
                'transaction_id' => $respost['data']['transaction_id'],
                'url' => $respost['data']['payment']
            );
        }

        return $arrret;
    }
    
    public function preview_data($request)
    {
        $arrdeliv = [];
        $shipp_method = $request->input('shipp_method');
        $expidition = $request->input('expidition');
        $durasi = $request->input('durasi');
        $arrexp = explode('-', $durasi);
        $arrinsert['payment_channel_id'] = (int) $request->input('payment_method');
        $arrinsert['shipping']['id'] = (int) $shipp_method;
        $arrinsert['customer_address_id'] = $request->input('customer_address_id');

        if ($shipp_method == 2) {
            $arrdeliv['id'] = (int) $expidition;
            $price = $request->input('total_delivery');
            $price = str_replace(',', '', $price);
            $arrdeliv['price'] = (int) $price;
            $arrdeliv['service_id'] = (int) $arrexp[0];
            $arrdeliv['service_code'] = $arrexp[1];

            $arrinsert['shipping']['delivery'] = $arrdeliv;
        } else {
            // $arrinsert['shipping']['pickup_at'] = $request->input('pickup_at');
            $arrinsert['shipping']['pickup_at'] = '';
        }

        return $arrinsert;
    }

    public function preview_final(Request $request)
    {
        // print_r($request->all());
        // die();

        $arrret = array(
            'res' => false,
            'msg' => 'Failed To Checkout',
            'url' => ''
        );

        $arrinsert = $this->preview_data($request);
        $arrinsert = json_encode($arrinsert);
        // print_r($arrinsert);
        // die();

        $respost = $this->service->api('/checkout/preview', $arrinsert, session('token'));
        // print_r($respost);
        // die();
        
        if ($respost['code'] == 200) {
            $arrret = array(
                'res' => true,
                'arr' => $respost['data']
            );
        }

        return $arrret;
    }

    public function list_voucher($store_id)
    {
        $arrret = array(
            'res' => false,
            'msg' => 'Voucher Not Found',
            'html' => 'Voucher Not Found',
            'html_detail' => 'Voucher Not Found',
            'code' => ''
        );

        $arrvoucher = $this->service->get_list_voucher($store_id);
        // print_r($arrvoucher);
        // die();
        $htmlexpi = '';
        $res = false;
        $arrcode = [];

        if ($arrvoucher['code'] == 200) {
            foreach ($arrvoucher['data'] as $voucher) {
                $htmlexpi .= '<div class="col-lg-12" id="list_' . $voucher['code'] . '">';
                $htmlexpi .= '    <div class="wn__team text-center">';
                $htmlexpi .= '        <div class="thumb">';
                $htmlexpi .= '            <a href="javascript:void(0)" onclick="open_promo_detail(\'' . $voucher['code'] . '\', \'' . $voucher['id'] . '\')">';
                $htmlexpi .= '                <img src="' . $voucher['image'] . '" alt="Team images">';
                $htmlexpi .= '            </a>';
                $htmlexpi .= '        </div>';
                $htmlexpi .= '        <div class="content" style="text-align: left;">';
                $htmlexpi .= '            <h4>' . $voucher['name'] . '</h4>';
                $htmlexpi .= '            <p>Periode : <br> ' . date('D, M Y - H I s', strtotime($voucher['start_date'])) . ' s/d ' . date('D, M Y - H I s', strtotime($voucher['end_date'])) . '</p>';
                $htmlexpi .= '        </div>';
                $htmlexpi .= '    </div>';
                $htmlexpi .= '</div>';

                $arrcode[] = $voucher['code'];
            }

            // $htmlexpi .= '<div class="col-lg-12" id="list_AAA">';
            // $htmlexpi .= '    <div class="wn__team text-center">';
            // $htmlexpi .= '        <div class="thumb">';
            // $htmlexpi .= '            <a href="javascript:void(0)" onclick="open_promo_detail(\'AAA\')">';
            // $htmlexpi .= '                <img src="https://devlottecms.ngobrol.co.id/upload/products/1017616000/512/1017616000.jpg" alt="Team images">';
            // $htmlexpi .= '            </a>';
            // $htmlexpi .= '        </div>';
            // $htmlexpi .= '        <div class="content" style="text-align: left;">';
            // $htmlexpi .= '            <h4>AAA</h4>';
            // $htmlexpi .= '            <p>Periode : <br> TEST s/d TEST</p>';
            // $htmlexpi .= '        </div>';
            // $htmlexpi .= '    </div>';
            // $htmlexpi .= '</div>';

            // $htmlexpi .= '<div class="col-lg-12" id="list_BBB">';
            // $htmlexpi .= '    <div class="wn__team text-center">';
            // $htmlexpi .= '        <div class="thumb">';
            // $htmlexpi .= '            <a href="javascript:void(0)" onclick="open_promo_detail(\'BBB\')">';
            // $htmlexpi .= '                <img src="https://devlottecms.ngobrol.co.id/upload/stores-banners/06003/512/1101a4ea1d6a8d3ff2fa4af35e4d205d1568347798.png" alt="Team images">';
            // $htmlexpi .= '            </a>';
            // $htmlexpi .= '        </div>';
            // $htmlexpi .= '        <div class="content" style="text-align: left;">';
            // $htmlexpi .= '            <h4>BBB</h4>';
            // $htmlexpi .= '            <p>Periode : <br> TEST s/d TEST</p>';
            // $htmlexpi .= '        </div>';
            // $htmlexpi .= '    </div>';
            // $htmlexpi .= '</div>';

            $res = true;
        }

        $arrvoucherdetail = $this->service->get_list_voucher_detail($store_id);
        // print_r($arrvoucherdetail);
        // die();
        $htmldetail = '';

        if ($arrvoucherdetail['code'] == 200) {

            foreach ($arrvoucherdetail['data'] as $detail) {
                $htmldetail .= '<div class="wn__team text-center" id="detail_' . $detail['code'] . '" style="display: none;">';
                $htmldetail .= '    <div class="thumb">';
                $htmldetail .= '        <a href="javascript:void(0)"><img src="' . $detail['image'] . '" alt="Team images"></a>';
                $htmldetail .= '    </div>';
                $htmldetail .= '    <div class="content" style="text-align: left;">';
                $htmldetail .= '        <h3>' . $detail['name'] . '</h3>';
                $htmldetail .= '        <div class="row">';
                $htmldetail .= '            <div class="col-lg-6">';
                $htmldetail .= '                <i style="color: #ea242e;" class="fa fa-clock-o fa-lg"></i><span> <b> ' . date('D, M Y - H I s', strtotime($detail['start_date'])) . ' s/d ' . date('D, M Y - H I s', strtotime($detail['end_date'])) . '</b> </span>';
                $htmldetail .= '            </div>';
                $htmldetail .= '            <div class="col-lg-6">';
                $htmldetail .= '            <i style="color: #ea242e;" class="fa fa-money fa-lg"></i><span> <b>Minimun Transaksi : Rp. ' . $detail['min_purchase'] . '</b> </span>';
                $htmldetail .= '            <i style="color: #ea242e;" class="fa fa-money fa-lg"></i><span> <b>Minimun Quantity : ' . $detail['min_qty'] . '</b> </span>';
                $htmldetail .= '            </div>';
                $htmldetail .= '        </div>';
                $htmldetail .= '        <h4>Syarat & Ketentuan</h4>';
                $htmldetail .= '        <p>';
                $htmldetail .= $detail['description'];
                $htmldetail .= '        </p>';
                $htmldetail .= '    </div>';
                $htmldetail .= '</div>';
            }

        }

        $arrret = array(
            'res' => $res,
            'html' => $htmlexpi,
            'html_detail' => $htmldetail,
            'code' => $arrcode
        );

        return $arrret;
    }

    public function search_voucher($store_id, $code_txt)
    {
        $arrret = array(
            'res' => false,
            'msg' => 'Voucher Not Found',
            'html' => 'Voucher Not Found',
            'html_detail' => 'Voucher Not Found'
        );

        $arrvoucher = $this->service->get_search_voucher($store_id, $code_txt);
        // print_r($arrvoucher);
        // die();
        $htmlexpi = '';
        $res = false;

        if ($arrvoucher['code'] == 200 && $arrvoucher['data'] != '') {
            foreach ($arrvoucher['data'] as $voucher) {
                $htmlexpi .= '<div class="col-lg-12">';
                $htmlexpi .= '    <div class="wn__team text-center">';
                $htmlexpi .= '        <div class="thumb">';
                $htmlexpi .= '            <a href="javascript:void(0)" onclick="open_promo_detail(\'' . $voucher['code'] . '\')">';
                $htmlexpi .= '                <img src="' . $voucher['image'] . '" alt="Team images">';
                $htmlexpi .= '            </a>';
                $htmlexpi .= '        </div>';
                $htmlexpi .= '        <div class="content" style="text-align: left;">';
                $htmlexpi .= '            <h4>' . $voucher['name'] . '</h4>';
                $htmlexpi .= '            <p>Periode : <br> ' . date('D, M Y - H I s', strtotime($voucher['start_date'])) . ' s/d ' . date('D, M Y - H I s', strtotime($voucher['end_date'])) . '</p>';
                $htmlexpi .= '        </div>';
                $htmlexpi .= '    </div>';
                $htmlexpi .= '</div>';   
            }

            $res = true;
        }

        $arrvoucherdetail = $this->service->get_list_voucher_detail($store_id);
        // print_r($arrvoucherdetail);
        // die();
        $htmldetail = '';

        if ($arrvoucherdetail['code'] == 200 && count($arrvoucherdetail['data']) > 0) {

            foreach ($arrvoucherdetail['data'] as $detail) {
                $htmldetail .= '<div class="wn__team text-center" id="detail_' . $detail['code'] . '" style="display: none;">';
                $htmldetail .= '    <div class="thumb">';
                $htmldetail .= '        <a href="javascript:void(0)"><img src="' . $detail['image'] . '" alt="Team images"></a>';
                $htmldetail .= '    </div>';
                $htmldetail .= '    <div class="content" style="text-align: left;">';
                $htmldetail .= '        <h3>' . $detail['name'] . '</h3>';
                $htmldetail .= '        <div class="row">';
                $htmldetail .= '            <div class="col-lg-6">';
                $htmldetail .= '                <i style="color: #ea242e;" class="fa fa-clock-o fa-lg"></i><span> <b> ' . date('D, M Y - H I s', strtotime($detail['start_date'])) . ' s/d ' . date('D, M Y - H I s', strtotime($detail['end_date'])) . '</b> </span>';
                $htmldetail .= '            </div>';
                $htmldetail .= '            <div class="col-lg-6">';
                $htmldetail .= '            <i style="color: #ea242e;" class="fa fa-money fa-lg"></i><span> <b>Minimun Transaksi : Rp. ' . $detail['min_purchase'] . '</b> </span>';
                $htmldetail .= '            <i style="color: #ea242e;" class="fa fa-money fa-lg"></i><span> <b>Minimun Quantity : ' . $detail['min_qty'] . '</b> </span>';
                $htmldetail .= '            </div>';
                $htmldetail .= '        </div>';
                $htmldetail .= '        <h4>Syarat & Ketentuan</h4>';
                $htmldetail .= '        <p>';
                $htmldetail .= $detail['description'];
                $htmldetail .= '        </p>';
                $htmldetail .= '    </div>';
                $htmldetail .= '</div>';
            }

        }

        if ($res) {
            $arrret = array(
                'res' => $res,
                'html' => $htmlexpi,
                'html_detail' => $htmldetail
            );   
        }

        return $arrret;
    }

    public function use_voucher(Request $request)
    {
        // print_r($request->input('payment_method'));
        // die();

        $arrret = array(
            'res' => false,
            'msg' => 'Gagal Menggunakan Voucher',
            'url' => ''
        );

        if ($request->input('payment_method') == '' || $request->input('payment_method') == 0) {
            $arrret = array(
                'res' => false,
                'msg' => 'Silahkan pilih metode pembayaran terlebih dahulu',
                'url' => ''
            );
        }

        $arrdeliv = [];
        $store_id = $request->input('store_id');
        $id_voucher = $request->input('id');
        $code_voucher = $request->input('code');
        $durasi = $request->input('durasi');
        $arrexp = explode(',', $durasi);
        $arrinsert['voucher']['id'] = $id_voucher;
        $arrinsert['voucher']['code'] = $code_voucher;
        $arrinsert['payment_channel_id'] = (int) $request->input('payment_method');
        $arrinsert['shipping']['expedition_id'] = (int) $request->input('expidition');
        $arrinsert['shipping']['service_id'] = (int) $arrexp[0];
        $arrinsert['shipping']['price'] = (float) $request->input('total_delivery');

        $arrinsert = json_encode($arrinsert);
        // print_r($arrinsert);
        // die();

        $respost_voucher = $this->service->api('/voucher/' . $store_id . '/validate', $arrinsert, session('token'));
        // print_r($respost_voucher);
        // die();

        $arrinsert = $this->preview_data($request);
        $arrinsert['voucher']['voucher_id'] = $respost_voucher['data']['id'];
        $arrinsert['voucher']['voucher_code'] = $code_voucher;
        $arrinsert = json_encode($arrinsert);
        // print_r($arrinsert);
        // die();

        $respost = $this->service->api('/checkout/preview', $arrinsert, session('token'));
        // print_r($respost);
        // die();
        $respost['data']['txt'] = $respost_voucher['data']['message'];
        
        if ($respost['code'] == 200) {
            $arrret = array(
                'res' => true,
                'arr' => $respost['data']
            );
        }

        return $arrret;
    }


}
