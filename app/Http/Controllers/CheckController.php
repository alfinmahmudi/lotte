<?php

namespace App\Http\Controllers;

use Session;

class CheckController extends Controller
{
    public function __construct()
    {
        $this->check_login();
    }

    public function check_login()
    {
        // print_r(session('token'));
        // die();
        // return redirect()->action('LoginController@index');

        // return Session::get('is_login');

        if (!session('token')) {
            return redirect()->action('LoginController@index');
        }
    }
}
