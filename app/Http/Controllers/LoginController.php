<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Service;
use Session;
use Helper;

class LoginController extends Controller
{

    public function __construct()
    {
        $this->service = new Service();
        $this->data_header = array(
            'upsource' => array(),
            'down_plugins' => array(),
            'down_scripts' => array()
        );
    }

    public function index()
    {
        #Include script
        $data_header = $this->data_header;

        array_push($data_header['down_scripts'], asset('js/allpage.js'));
        array_push($data_header['down_scripts'], asset('js/login.js'));
        array_push($data_header['upsource'], asset('css/store_cust.css'));

        //ui_sweetalert.html
        array_push($data_header['upsource'], asset('templates/plugins/bootstrap-sweetalert/sweetalert.css'));
        array_push($data_header['down_plugins'], asset('templates/plugins/bootstrap-sweetalert/sweetalert.min.js'));

        //components_select2.html
        array_push($data_header['upsource'], asset('templates/plugins/select2/css/select2.min.css'));
        array_push($data_header['upsource'], asset('templates/plugins/select2/css/select2-bootstrap.min.css'));
        array_push($data_header['down_plugins'], asset('templates/plugins/select2/js/select2.full.min.js'));
        array_push($data_header['down_scripts'], asset('js/component_select2.js'));

        // autocomplete
        array_push($data_header['down_plugins'], asset('js/autocomplete.js'));
        array_push($data_header['upsource'], asset('css/autocomplete.css'));

        $source['upsource'] = $data_header['upsource'];
        $source['down_plugins'] = $data_header['down_plugins'];
        $source['down_scripts'] = $data_header['down_scripts'];


        return view('login', ['source' => $source]);
    }

    public function full_address()
    {
        if (!session('token')) {
            Helper::logout();
            return redirect()->action('LoginController@index');
        }
        #Include script
        $data_header = $this->data_header;

        // array_push($data_header['down_scripts'], asset('js/allpage.js'));
        // array_push($data_header['down_scripts'], asset('js/login.js'));
        array_push($data_header['down_scripts'], asset('js/maps.js'));

        //ui_sweetalert.html
        array_push($data_header['upsource'], asset('templates/plugins/bootstrap-sweetalert/sweetalert.css'));
        array_push($data_header['down_plugins'], asset('templates/plugins/bootstrap-sweetalert/sweetalert.min.js'));

        //components_select2.html
        array_push($data_header['upsource'], asset('templates/plugins/select2/css/select2.min.css'));
        array_push($data_header['upsource'], asset('templates/plugins/select2/css/select2-bootstrap.min.css'));
        array_push($data_header['down_plugins'], asset('templates/plugins/select2/js/select2.full.min.js'));
        array_push($data_header['down_scripts'], asset('js/component_select2.js'));

        // autocomplete
        array_push($data_header['down_plugins'], asset('templates/plugins/EasyAutocomplete/jquery.easy-autocomplete.min.js'));
        array_push($data_header['upsource'], asset('templates/plugins/EasyAutocomplete/easy-autocomplete.min.css'));
        array_push($data_header['upsource'], asset('templates/plugins/EasyAutocomplete/easy-autocomplete.themes.min.css'));

        $source['upsource'] = $data_header['upsource'];
        $source['down_plugins'] = $data_header['down_plugins'];
        $source['down_scripts'] = $data_header['down_scripts'];

        $arraddrs = $this->service->check_address();
        // print_r($arraddrs);
        // die();

        $htmlres = Helper::storedata();
        // print_r($htmlres);
        // die();

        $htmladdrs = 'Data Not Found';

        if ($arraddrs && $arraddrs['code'] == 200) {
            $htmladdrs = '';
            $htmldefault = '';
            foreach ($arraddrs['data'] as $addrs) {
                if ($addrs['is_default'] == 1) {
                    $htmldefault = ' <small style="color: red;">Default Address</small>';   
                }else{
                    $htmldefault = '';
                }

                $htmladdrs .= '<article class="blog__post d-flex flex-wrap">';
                $htmladdrs .= '    <div class="content" style="flex-basis: 100%;">';
                $htmladdrs .= '        <h4>' . $addrs['title'] . ' ' . $htmldefault . '</h4>';
                $htmladdrs .= '        <ul class="post__meta">';
                $htmladdrs .= '            <li>' . $addrs['customer_name'] . '</li> ';
                $htmladdrs .= '            <li class="post_separator">/</li>';
                $htmladdrs .= '            <li>' . $addrs['email'] . '</li> ';
                $htmladdrs .= '            <li class="post_separator">/</li>';
                $htmladdrs .= '            <li>' . $addrs['phone'] . '</li> ';
                $htmladdrs .= '        </ul>';
                $htmladdrs .= '        <p>' . $addrs['address'] . ' <br> ' . $addrs['province']['name'] . ', ' . $addrs['regency']['name'] . ' <br> ' . $addrs['zip_code'] . '</p>';
                $htmladdrs .= '        <ul class="post__meta">';
                $htmladdrs .= '            <li><a href="javascript:void(0)" data-url="' . url('/login/get_address_detail/') . '" data-id="' . $addrs['id'] . '" onclick="show_addrs($(this), 1)">Edit</a></li> ';
                $htmladdrs .= '            <li class="post_separator">/</li>';
                $htmladdrs .= '            <li><a href="javascript:void(0)" data-id="' . $addrs['id'] . '" onclick="delete_address($(this))">Delete</a></li> ';
                $htmladdrs .= '            <li class="post_separator">/</li>';
                $htmladdrs .= '            <li><a href="javascript:void(0)" data-id="' . $addrs['id'] . '" onclick="makedefault($(this))">Make As Default</a></li> ';
                $htmladdrs .= '        </ul>';
                $htmladdrs .= '    </div>';
                $htmladdrs .= '</article>';
            }
        }

        return view('address', ['source' => $source, 'htmlStore' => $htmlres, 'htmladdrs' => $htmladdrs]);
    }

    public function login_otp(Request $request)
    {
        $arrret = array(
            'res' => false,
            'title' => 'Cek Nomor OTP Gagal',
            'msg' => 'Nomor OTP Anda Salah, Mohon Periksa Nomor OTP Anda.',
        );
        
        // print_r($request->all());
        // die();
        $input = $request->input('OTP');

        if (count($input) > 0) {
            $req['code'] = implode('', $input);
            $jsonreq = json_encode($req);
            // print_r($jsonreq);
            // die();

            $res = $this->service->api('/auth/token', $jsonreq, session('token'));
            // print_r($res);
            // die();
            if ($res['code'] == 200 && $res['data']['token']) {
                Session::put('token', $res['data']['token']);
                Session::put('is_login', 1);

                $check_user = $this->service->check_user();
                // print_r($check_addrs);
                // die();

                if ($check_user['code'] == 200) {
                    Session::put('datauser', $check_user['data']);
                }

                $check_addrs = $this->service->check_address();
                // print_r($check_addrs);
                // die();

                if ($check_addrs['code'] == 404) {
                    $arrprov = $this->service->get_province();
                    // print_r($arrprov);
                    // die();

                    $htmlprov = '';
                    foreach ($arrprov['data'] as $prov) {
                        $htmlprov .= '<option value="' . $prov['id'] . '">' . $prov['name'] . '</option>';
                    }

                    $arrret = array(
                        'res' => true,
                        'msg' => 'Login Success',
                        'addrs' => '',
                        'htmlprov' => $htmlprov,
                        'url' => url('store/home')
                    );
                } else {
                    $sum_addrs = count($check_addrs['data']);
                    session()->put('sum_addrs', $sum_addrs);
                    $is_default_flag = 0;
                    foreach ($check_addrs['data'] as $addrs) {
                        if ($addrs['is_default'] == 1) {
                            session()->put('dataaddress', $addrs);
                            $is_default_flag = 1;
                        }
                    }

                    if ($is_default_flag == 0) {
                        $arrret = array(
                            'res' => true,
                            'msg' => 'Mohon Memilih Alamat Default',
                            'addrs' => $check_addrs['data'],
                            'htmlprov' => '',
                            'url' => url('login/get_full_address')
                        );
                    }else{
                        $arrret = array(
                            'res' => true,
                            'msg' => 'Login Success',
                            'addrs' => $check_addrs['data'],
                            'htmlprov' => '',
                            'url' => url('store/home')
                        );
                    }
                }
            }

        }
        echo json_encode($arrret);
    }

    public function login_auto($arrReq)
    {
        $arrret = array(
            'res' => false,
            'title' => 'Cek Nomor Kartu Gagal',
            'msg' => 'Nomor Kartu Anda Tidak Terdafar, Mohon Periksa Kembali Nomor Kartu Anda',
        );
        
        // print_r(url('store/home'));
        // die();
        $input = $arrReq;

        if (count($input) > 0) {
            $jsonreq = json_encode($input);
            $res = $this->service->api('/auth/login', $jsonreq);
            // print_r($res);
            // die();
            if ($res['code'] == 200) {
                Session::put('token', $res['data']['request_token']);
                $arrret = array(
                    'res' => true,
                    'msg' => 'Login Success',
                    'url' => url('store/home')
                );
            }
        }
        return $arrret;
    }

    public function login_member(Request $request)
    {
        $arrret = array(
            'res' => false,
            'title' => 'Cek Nomor Kartu Gagal',
            'msg' => 'Nomor Kartu Anda Tidak Terdafar, Mohon Periksa Kembali Nomor Kartu Anda',
        );
        
        // print_r(url('store/home'));
        // die();
        $input = $request->all();

        if (count($input) > 0) {
            $jsonreq = json_encode($input);
            $res = $this->service->api('/auth/login', $jsonreq);
            // print_r($res);
            // die();
            if ($res['code'] == 200) {
                Session::put('token', $res['data']['request_token']);
                $arrret = array(
                    'res' => true,
                    'msg' => 'Login Success',
                    'url' => url('store/home')
                );
            }
        }

        echo json_encode($arrret);
    }

    public function regis_guest(Request $request)
    {
        $arrret = array(
            'res' => false,
            'title' => 'Registrasi Gagal',
            'msg' => '',
        );

        $input = $request->all();

        if (count($input) > 0) {
            $input['gender'] = (int) $input['gender'];
            $jsonreq = json_encode($input);
            $res = $this->service->api('/auth/visitor', $jsonreq);
            // print_r($res);
            // die();
            if ($res['code'] == 200) {
                $arrret = array(
                    'res' => true,
                    'msg' => 'Registrasi Berhasil',
                    'data' => $res['data']
                );
            }else if ($res['code'] == 302) {
                $arrret = array(
                    'res' => true,
                    'title' => 'Nomor Hp Sudah Terdaftar',
                    'msg' => 'silahkan login dengan nomor kartu yang kami kirim via sms',
                    'url' => url('')
                );
            }
        }

        echo json_encode($arrret);
    }

    public function get_regency($prov_id)
    {
        $arrret = array(
            'res' => false,
            'msg' => 'Failed To Get Regency',
            'data' => ''
        );

        $res = $this->service->get_regency($prov_id);
        if ($res['code'] == 200) {
            $htmlregen = '<option value="0">&nbsp;</option>';
            foreach ($res['data'] as $regen) {
                $htmlregen .= '<option value="' . $regen['id'] . '">' . $regen['name'] . '</option>';
            }

            $arrret = array(
                'res' => true,
                'htmlregen' => $htmlregen,
                'msg' => ''
            );
        }
        echo json_encode($arrret);
    }

    public function set_address(Request $request)
    {
        $arrret = array(
            'res' => false,
            'msg' => 'Add Address Failed',
            'data' => ''
        );

        $input = $request->all();
        if ($input["longitude"] != '' && $input["latitude"]) {
            $input["longitude"] = (float)$input["longitude"];
            $input["latitude"] = (float)$input["latitude"];
        }else{
            $arrret = array(
                'res' => false,
                'msg' => 'Please Choose The Point Maps',
                'data' => ''
            );
            echo json_encode($arrret);
            exit();
        }
        // print_r($input);
        // die();

        if (count($input) > 0) {
            $input["is_default"] = (int)$input["is_default"];
            $jsonreq = json_encode($input);
            // print_r($jsonreq);
            // die();

            $res = $this->service->api('/user/address', $jsonreq, session('token'));
            // print_r($res);
            // die();
            if ($res['code'] == 200) {
                $resaddrs = $this->service->check_address();
                // print_r($resaddrs);
                // die();
                if ($resaddrs['code'] == 200) {
                    $sum_addrs = count($resaddrs['data']);
                    session()->put('sum_addrs', $sum_addrs);
                    foreach ($resaddrs['data'] as $addrs) {
                        if ($addrs['is_default']) {
                            session()->put('dataaddress', $addrs);   
                        }
                    }
                }
                
                if ($input['flag'] == 1) {
                    $arrret = array(
                        'res' => true,
                        'msg' => 'Add Address Success',
                        'data' => $res['data'],
                        'url' => ''
                    );
                }else{
                    $arrret = array(
                        'res' => true,
                        'msg' => 'Add Address Success',
                        'data' => $res['data'],
                        'url' => url('login/get_full_address')
                    );
                }
            }
        }

        echo json_encode($arrret);
    }

    public function get_address()
    {
        $arrret = array(
            'res' => false,
            'msg' => 'Failed To Get Address',
            'data' => ''
        );

        $res = $this->service->check_address();
        // print_r($res);
        // die();
        if ($res['code'] == 200) {
            $htmlregen = '';
            foreach ($res['data'] as $regen) {
                $is_default = '';
                if ($regen['is_default'] == 1) {
                    $is_default = '<small>Default Address</small>';
                }
                $htmlregen .= '<div class="item01 d-flex">';
                $htmlregen .= '    <div class="content" style="flex-basis: 100%;">';
                $htmlregen .= '        <h6>' . $regen['title'] . ' ' . $is_default . '</h6>';
                $htmlregen .= '        <span class="prize">' . $regen['address'] . '</span>';
                $htmlregen .= '        <div class="product_prize d-flex justify-content-between">';
                $htmlregen .= '            <span class="qun">' . $regen['phone'] . '</span>';
                $htmlregen .= '        </div>';
                $htmlregen .= '    </div>';
                $htmlregen .= '</div>';
            }

            $arrret = array(
                'res' => true,
                'htmlregen' => $htmlregen,
                'msg' => ''
            );
        }
        echo json_encode($arrret);
    }

    public function get_province()
    {
        $arrprov = $this->service->get_province();
        print_r($arrprov);
        die();
        $htmlprov = '';
        if (count($arrprov) > 0) {
            foreach ($arrprov['data'] as $prov) {
                $htmlprov .= '<option value="' . $prov['id'] . '">' . $prov['name'] . '</option>';
            }
        }
        
        return $htmlprov;
    }

    public function get_address_detail($addrs_id='', $flag='')
    {
        if ($addrs_id == "0") {
            $res = $this->service->check_address();
            $views = 'addres_new';    
        }else{
            $res = $this->service->check_address($addrs_id);
            $views = 'addres_edit';
        }

        // print_r($res);
        // die();
        if ($res['code'] == 200) {
            $dataaddrs = $res['data'];
        }else{
            $dataaddrs = '';
        }
        $arrprov = $this->service->get_province();
        // print_r($arrprov);
        // die();

        $htmlprov = '<option value="">&nbsp;</option>';
        $htmlreg = '';
        
        if ($addrs_id != '' && $addrs_id != "0") {
            foreach ($arrprov['data'] as $prov) {
                $checked = '';
                if ($res['data']['province']['id'] == $prov['id']) {
                    $checked = 'selected';
                }
                $htmlprov .= '<option value="' . $prov['id'] . '" ' . $checked . '>' . $prov['name'] . '</option>';
            }

            $arrreg = $this->service->get_regency($res['data']['province']['id']);
            // print_r($arrprov);
            // die();

            foreach ($arrreg['data'] as $reg) {
                $checked = '';
                if ($res['data']['regency']['id'] == $reg['id']) {
                    $checked = 'selected';
                }
                $htmlreg .= '<option value="' . $reg['id'] . '" ' . $checked . '>' . $reg['name'] . '</option>';
            }
        } else {
            foreach ($arrprov['data'] as $prov) {
                $htmlprov .= '<option value="' . $prov['id'] . '">' . $prov['name'] . '</option>';
            }
        }

        return view('parts.' . $views, ['data' => $dataaddrs, 'prov' => $htmlprov, 'reg' => $htmlreg, 'flag'=>$flag]);
    }

    public function update_address(Request $request)
    {
        $input = $request->all();
        // print_r($input);
        // die();
        $addres_id = $input['address_id'];
        $arrupdt = $input['ADDRS'];
        $arrupdt["longitude"] = (float)$arrupdt["longitude"];
        $arrupdt["latitude"] = (float)$arrupdt["latitude"];
        $arrupdt["is_default"] = (int)$arrupdt["is_default"];
        // print_r($arrupdt);
        // die();
        
        $arrret = array(
            'res' => false,
            'msg' => 'Failed To Update Address',
            'data' => ''
        );

        $arrupdt = json_encode($arrupdt);
        // print_r($input);
        // die();

        $arrres = $this->service->api_update('/user/address/' . $addres_id, $arrupdt);
        // print_r($arrres);
        // die();

        if ($arrres['code'] == 200) {

            $resaddrs = $this->service->check_address();
            // print_r($resaddrs);
            // die();
            if ($resaddrs['code'] == 200) {
                $sum_addrs = count($resaddrs['data']);
                session()->put('sum_addrs', $sum_addrs);
                foreach ($resaddrs['data'] as $addrs) {
                    if ($addrs['is_default']) {
                        session()->put('dataaddress', $addrs);   
                    }
                }
            }

            $arrret = array(
                'res' => true,
                'msg' => 'Add Address Success',
                'url' => url('login/get_full_address')
            );
        }

        echo json_encode($arrret);
    }

    public function delete_address($address_id)
    {
        $arrret = array(
            'res' => false,
            'msg' => 'Failed To Delete Address',
            'data' => ''
        );

        $arrres = $this->service->api_delete('/user/address/' . $address_id);
        // print_r($arrres);
        // die();

        if ($arrres['code'] == 200) {
            $resaddrs = $this->service->check_address();
            // print_r($res);
            // die();
            $sum_addrs = count($resaddrs['data']);
            session()->put('dataaddress', $resaddrs['data']);
            session()->put('sum_addrs', $sum_addrs);
            
            $arrret = array(
                'res' => true,
                'msg' => 'Delete Address Success',
                'url' => url('login/get_full_address')
            );
        }

        echo json_encode($arrret);
    }

    public function makedefault($address_id)
    {
        $arrret = array(
            'res' => false,
            'msg' => 'Failed To Make Default Address',
            'data' => ''
        );

        $resaddrs = $this->service->check_address($address_id);
        // print_r($resaddrs);
        // var_dump((float)$resaddrs['data']['longitude']);
        // die();

        if ($resaddrs && $resaddrs['code'] == 200) {
            $arrupdt = $resaddrs['data'];
            unset($arrupdt['id']);
            $arrupdt['province_id'] = $resaddrs['data']['province']['id'];
            $arrupdt['regency_id'] = $resaddrs['data']['regency']['id'];
            $arrupdt['longitude'] = (float)$resaddrs['data']['longitude'];
            $arrupdt['latitude'] = (float)$resaddrs['data']['latitude'];
            $arrupdt['is_default'] = 1;
            $arrupdt = json_encode($arrupdt);
            // print_r($arrupdt);
            // die();

            // $testupdt = {(
            //     [title] => alfin
            //     [customer_name] => mahmudi
            //     [phone] => 6281262216970
            //     [address] => mall artha gading
            //     [email] => utamaacipadu@gmail.com
            //     [zip_code] => 11782
            //     [province_id] => 31
            //     [regency_id] => 3175
            //     [is_default] => 1
            //     [latitude] => -6.1458457266166
            //     [longitude] => 106.89186573029
            // )};
            
            $arrres = $this->service->api_update('/user/address/' . $address_id, $arrupdt);
            // print_r($arrres);
            // die();

            if ($arrres['code'] == 200) {
                session()->put('dataaddress', $resaddrs['data']);
                $arrret = array(
                    'res' => true,
                    'msg' => 'Make Default Address Success',
                    'url' => url('login/get_full_address')
                );
            }
            
        }

        echo json_encode($arrret);
    }

    public function logout(Request $request)
    {
        $request->session()->flush();
        return redirect()->action('LoginController@index');
    }

    public function search_maps($value)
    {
        $arrret = [];
        $i = 0;
        $arrmaps = $this->service->get_search_maps($value);
        // print_r($arrmaps);
        // die();  
        if (count($arrmaps['predictions']) > 0) {
            foreach ($arrmaps['predictions'] as $maps) {
                $arrret[$i]['desc'] = $maps['description'];
                $arrret[$i]['map_id'] = $maps['place_id'];
                $i++;
            }
        }

        echo json_encode($arrret);
    }
}
