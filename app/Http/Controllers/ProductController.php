<?php

namespace App\Http\Controllers;

use App\Helpers\Helper;
use App\Service;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ProductController extends Controller
{

    public function __construct()
    {
        $this->service = new Service();
        $this->data_header = array(
            'upsource' => array(),
            'down_plugins' => array(),
            'down_scripts' => array()
        );
    }

    public function index($store_id)
    {
        if (!session('token')) {
            Helper::logout();
            return redirect()->action('LoginController@index');
        }
        #Include script
        $data_header = $this->data_header;

        //ui_sweetalert.html
        array_push($data_header['upsource'], asset('templates/plugins/bootstrap-sweetalert/sweetalert.css'));
        array_push($data_header['down_plugins'], asset('templates/plugins/bootstrap-sweetalert/sweetalert.min.js'));

        //animate.css
        array_push($data_header['upsource'], asset('css/animate.css'));

        // table set
        array_push($data_header['down_scripts'], asset('js/table.js'));
        array_push($data_header['down_scripts'], asset('js/product.js'));

        // autocomplete
        array_push($data_header['down_plugins'], asset('templates/plugins/EasyAutocomplete/jquery.easy-autocomplete.min.js'));
        array_push($data_header['upsource'], asset('templates/plugins/EasyAutocomplete/easy-autocomplete.min.css'));
        array_push($data_header['upsource'], asset('templates/plugins/EasyAutocomplete/easy-autocomplete.themes.min.css'));

        $source['upsource'] = $data_header['upsource'];
        $source['down_plugins'] = $data_header['down_plugins'];
        $source['down_scripts'] = $data_header['down_scripts'];

        $htmlres = Helper::storedata($store_id);
        // print_r($htmlres);
        // die();

        // $rescat = $this->service->get_category($store_id);
        // print_r($arrcategory);
        // die();

        $arrprod = $this->service->get_product($store_id, '', '', '1');
        // print_r($arrprod);
        // die();

        $htmlprod = Helper::generate_product($store_id, $arrprod);
        // print_r($htmlprod);
        // die();

        $htmlpaging = '';
        
        return view('product.default', ['source' => $source, 'htmlStore' => $htmlres, 'htmlProd' => $htmlprod, 'htmlpaging' => $htmlpaging, 'search' => '']);
    }

    public function category($store_id, $cat_id)
    {
        if (!session('token')) {
            Helper::logout();
            return redirect()->action('LoginController@index');
        }
        #Include script
        $data_header = $this->data_header;

        //ui_sweetalert.html
        array_push($data_header['upsource'], asset('templates/plugins/bootstrap-sweetalert/sweetalert.css'));
        array_push($data_header['down_plugins'], asset('templates/plugins/bootstrap-sweetalert/sweetalert.min.js'));

        //animate.css
        array_push($data_header['upsource'], asset('css/animate.css'));

        // table set
        array_push($data_header['down_scripts'], asset('js/table.js'));
        array_push($data_header['down_scripts'], asset('js/product.js'));

        // autocomplete
        array_push($data_header['down_plugins'], asset('templates/plugins/EasyAutocomplete/jquery.easy-autocomplete.min.js'));
        array_push($data_header['upsource'], asset('templates/plugins/EasyAutocomplete/easy-autocomplete.min.css'));
        array_push($data_header['upsource'], asset('templates/plugins/EasyAutocomplete/easy-autocomplete.themes.min.css'));

        $source['upsource'] = $data_header['upsource'];
        $source['down_plugins'] = $data_header['down_plugins'];
        $source['down_scripts'] = $data_header['down_scripts'];

        $htmlres = Helper::storedata($store_id);
        // print_r($htmlres);
        // die();

        // $rescat = $this->service->get_category($store_id);
        // print_r($arrcategory);
        // die();

        $htmlpaging = '';
        $arrprod = $this->service->get_product($store_id, $cat_id);
        // print_r($arrprod);
        // die();

        $htmlprod = Helper::generate_product($store_id, $arrprod);
        // print_r($htmlprod);
        // die();

        return view('product.default', ['source' => $source, 'htmlStore' => $htmlres, 'htmlProd' => $htmlprod, 'htmlpaging' => $htmlpaging, 'search' => '']);
    }

    public function promo($store_id)
    {
        if (!session('token')) {
            Helper::logout();
            return redirect()->action('LoginController@index');
        }
        #Include script
        $data_header = $this->data_header;

        //ui_sweetalert.html
        array_push($data_header['upsource'], asset('templates/plugins/bootstrap-sweetalert/sweetalert.css'));
        array_push($data_header['down_plugins'], asset('templates/plugins/bootstrap-sweetalert/sweetalert.min.js'));

        //animate.css
        array_push($data_header['upsource'], asset('css/animate.css'));

        // table set
        array_push($data_header['down_scripts'], asset('js/table.js'));
        array_push($data_header['down_scripts'], asset('js/product.js'));

        // autocomplete
        array_push($data_header['down_plugins'], asset('templates/plugins/EasyAutocomplete/jquery.easy-autocomplete.min.js'));
        array_push($data_header['upsource'], asset('templates/plugins/EasyAutocomplete/easy-autocomplete.min.css'));
        array_push($data_header['upsource'], asset('templates/plugins/EasyAutocomplete/easy-autocomplete.themes.min.css'));

        $source['upsource'] = $data_header['upsource'];
        $source['down_plugins'] = $data_header['down_plugins'];
        $source['down_scripts'] = $data_header['down_scripts'];

        $htmlres = Helper::storedata($store_id);
        // print_r($htmlres);
        // die();

        // $rescat = $this->service->get_category($store_id);
        // print_r($arrcategory);
        // die();

        $htmlpaging = '';
        $arrprod['code'] = 404;
        // print_r($arrprod);
        // die();

        $respromo = $this->service->get_promo($store_id);
        // print_r($respromo);
        // die();

        return view('product.promo', ['source' => $source, 'htmlStore' => $htmlres, 'htmlPromo' => $respromo, 'htmlpaging' => $htmlpaging, 'search' => '']);
    }

    public function promo_detail($store_id, $promo_id)
    {
        if (!session('token')) {
            Helper::logout();
            return redirect()->action('LoginController@index');
        }
        #Include script
        $data_header = $this->data_header;

        //ui_sweetalert.html
        array_push($data_header['upsource'], asset('templates/plugins/bootstrap-sweetalert/sweetalert.css'));
        array_push($data_header['down_plugins'], asset('templates/plugins/bootstrap-sweetalert/sweetalert.min.js'));

        //animate.css
        array_push($data_header['upsource'], asset('css/animate.css'));

        // table set
        array_push($data_header['down_scripts'], asset('js/table.js'));
        array_push($data_header['down_scripts'], asset('js/product.js'));

        // autocomplete
        array_push($data_header['down_plugins'], asset('templates/plugins/EasyAutocomplete/jquery.easy-autocomplete.min.js'));
        array_push($data_header['upsource'], asset('templates/plugins/EasyAutocomplete/easy-autocomplete.min.css'));
        array_push($data_header['upsource'], asset('templates/plugins/EasyAutocomplete/easy-autocomplete.themes.min.css'));

        $source['upsource'] = $data_header['upsource'];
        $source['down_plugins'] = $data_header['down_plugins'];
        $source['down_scripts'] = $data_header['down_scripts'];

        $htmlres = Helper::storedata($store_id);
        // print_r($htmlres);
        // die();

        // $rescat = $this->service->get_category($store_id);
        // print_r($arrcategory);
        // die();

        $htmlpaging = '';
        $arrprod = $arrprod = $this->service->get_product($store_id, '', '', '', "", $promo_id);
        // print_r($arrprod);
        // die();

        $htmlprod = Helper::generate_product($store_id, $arrprod);
        // print_r($htmlprod);
        // die();

        return view('product.default', ['source' => $source, 'htmlStore' => $htmlres, 'htmlProd' => $htmlprod, 'htmlpaging' => $htmlpaging, 'search' => '']);
    }

    public function bblm($store_id)
    {
        if (!session('token')) {
            Helper::logout();
            return redirect()->action('LoginController@index');
        }
        #Include script
        $data_header = $this->data_header;

        //ui_sweetalert.html
        array_push($data_header['upsource'], asset('templates/plugins/bootstrap-sweetalert/sweetalert.css'));
        array_push($data_header['down_plugins'], asset('templates/plugins/bootstrap-sweetalert/sweetalert.min.js'));

        //animate.css
        array_push($data_header['upsource'], asset('css/animate.css'));

        // table set
        array_push($data_header['down_scripts'], asset('js/table.js'));
        array_push($data_header['down_scripts'], asset('js/product.js'));

        // autocomplete
        array_push($data_header['down_plugins'], asset('templates/plugins/EasyAutocomplete/jquery.easy-autocomplete.min.js'));
        array_push($data_header['upsource'], asset('templates/plugins/EasyAutocomplete/easy-autocomplete.min.css'));
        array_push($data_header['upsource'], asset('templates/plugins/EasyAutocomplete/easy-autocomplete.themes.min.css'));

        $source['upsource'] = $data_header['upsource'];
        $source['down_plugins'] = $data_header['down_plugins'];
        $source['down_scripts'] = $data_header['down_scripts'];

        $htmlres = Helper::storedata($store_id);
        // print_r($htmlres);
        // die();

        // $rescat = $this->service->get_category($store_id);
        // print_r($arrcategory);
        // die();

        $htmlpaging = '';
        $arrprod = $arrprod = $this->service->get_product($store_id, '', '1');
        // print_r($arrprod);
        // die();

        $htmlprod = Helper::generate_product($store_id, $arrprod);
        // print_r($htmlprod);
        // die();

        return view('product.default', ['source' => $source, 'htmlStore' => $htmlres, 'htmlProd' => $htmlprod, 'htmlpaging' => $htmlpaging, 'search' => '']);
    }

    public function search_results($value)
    {
        $arrret = [];
        $i = 0;
        $arrprod = $arrprod = $this->service->get_product(session('store_id'), '', '', '', $value);
        // print_r($arrprod);
        // die();

        if (count($arrprod) > 0 && $arrprod['code'] == 200) {
            foreach ($arrprod['data'] as $res) {
                $arrret[$i]['product_id'] = $res['product_id'];
                $arrret[$i]['product_name'] = $res['product_name'];
                $i++;
            }
        }

        echo json_encode($arrret);
    }

    public function search($store_id, $value)
    {
        // print_r($value);
        // die();
        if (!session('token')) {
            Helper::logout();
            return redirect()->action('LoginController@index');
        }
        #Include script
        $data_header = $this->data_header;

        //ui_sweetalert.html
        array_push($data_header['upsource'], asset('templates/plugins/bootstrap-sweetalert/sweetalert.css'));
        array_push($data_header['down_plugins'], asset('templates/plugins/bootstrap-sweetalert/sweetalert.min.js'));

        //animate.css
        array_push($data_header['upsource'], asset('css/animate.css'));

        // table set
        array_push($data_header['down_scripts'], asset('js/table.js'));
        array_push($data_header['down_scripts'], asset('js/product.js'));

        // autocomplete
        array_push($data_header['down_plugins'], asset('templates/plugins/EasyAutocomplete/jquery.easy-autocomplete.min.js'));
        array_push($data_header['upsource'], asset('templates/plugins/EasyAutocomplete/easy-autocomplete.min.css'));
        array_push($data_header['upsource'], asset('templates/plugins/EasyAutocomplete/easy-autocomplete.themes.min.css'));

        $source['upsource'] = $data_header['upsource'];
        $source['down_plugins'] = $data_header['down_plugins'];
        $source['down_scripts'] = $data_header['down_scripts'];

        $htmlres = Helper::storedata($store_id);
        // print_r($htmlres);
        // die();

        // $rescat = $this->service->get_category($store_id);
        // print_r($arrcategory);
        // die();

        $htmlpaging = '';
        $arrprod = $this->service->get_product($store_id, '', '', '', $value);
        // print_r($arrprod);
        // die();

        $htmlprod = Helper::generate_product($store_id, $arrprod);
        // print_r($htmlprod);
        // die();

        return view('product.default', ['source' => $source, 'htmlStore' => $htmlres, 'htmlProd' => $htmlprod, 'htmlpaging' => $htmlpaging, 'search' => $value]);
    }

    public function detail($store_id, $product_id, $is_bblm)
    {
        if (!session('token')) {
            Helper::logout();
            return redirect()->action('LoginController@index');
        }
        #Include script
        $data_header = $this->data_header;

        //ui_sweetalert.html
        array_push($data_header['upsource'], asset('templates/plugins/bootstrap-sweetalert/sweetalert.css'));
        array_push($data_header['down_plugins'], asset('templates/plugins/bootstrap-sweetalert/sweetalert.min.js'));

        //animate.css
        array_push($data_header['upsource'], asset('css/animate.css'));

        // autocomplete
        array_push($data_header['down_plugins'], asset('templates/plugins/EasyAutocomplete/jquery.easy-autocomplete.min.js'));
        array_push($data_header['upsource'], asset('templates/plugins/EasyAutocomplete/easy-autocomplete.min.css'));
        array_push($data_header['upsource'], asset('templates/plugins/EasyAutocomplete/easy-autocomplete.themes.min.css'));

        $source['upsource'] = $data_header['upsource'];
        $source['down_plugins'] = $data_header['down_plugins'];
        $source['down_scripts'] = $data_header['down_scripts'];

        $htmlres = Helper::storedata($store_id);
        // print_r($htmlres);
        // die();

        // $rescat = $this->service->get_category($store_id);
        // print_r($arrcategory);
        // die();

        $arrproddet = $this->service->get_productdetail($store_id, $product_id);
        // print_r($arrproddet);
        // die();
        $htmlbblm = '';
        $display = 'style="display: none;"';
        if ($is_bblm) {
            $arrbblm = $this->service->get_productdetail($store_id, $product_id, '', $is_bblm);
            // print_r($arrbblm);
            // die();
            
            if ($arrbblm && $arrbblm['code'] == 200) {
                $display = '';
                foreach ($arrbblm['data'] as $bblm) {
                    $htmlbblm .= '<div class="row"';
                    $htmlbblm .= '<div class="col-lg-12 col-12">';
                    $htmlbblm .= '    <table>';
                    $htmlbblm .= '        <tr>';
                    $htmlbblm .= '            <td colspan="3"><b>Level ' . $bblm['level'] . '</b></td>';
                    $htmlbblm .= '        </tr>';
                    $htmlbblm .= '        <tr>';
                    $htmlbblm .= '            <td>Info Pembelian</td>';
                    $htmlbblm .= '            <td>:</td>';
                    $htmlbblm .= '            <td>' . $bblm['info'] . '</td>';
                    $htmlbblm .= '        </tr>';
                    $htmlbblm .= '        <tr>';
                    $htmlbblm .= '            <td>Harga Persatuan</td>';
                    $htmlbblm .= '            <td>:</td>';
                    $htmlbblm .= '            <td>Rp. ' . number_format($bblm['price']) . '</td>';
                    $htmlbblm .= '        </tr>';
                    $htmlbblm .= '    </table>';
                    $htmlbblm .= '</div>';
                    $htmlbblm .= '</div>';
                }
            }
        }

        $arrprodimg = $this->service->get_productimg($product_id);
        // print_r($arrprodimg);
        // die();

        $htmlimgprod = '';
        if ($arrprodimg) {
            foreach ($arrprodimg['data'] as $prodimg) {
                $htmlimgprod .= '<a href="1.jpg"><img src="' . $prodimg['image'] . '" alt=""></a>';
            }
        }

        return view('product.detail', ['htmlStore' => $htmlres, 'display' => $display, 'htmlbblm' => $htmlbblm, 'source' => $source, 'data_prod' => $arrproddet['data'], 'img_prod' => $htmlimgprod, 'search' => '']);
    }

    // public function generate_product($store_id, $arrprod)
    // {
    //     $htmlprod = '';
    //     if (is_array($arrprod) && $arrprod['code'] == 200) {
    //         $Allprod = $arrprod['data'];
    //         foreach ($Allprod as $prod) {
    //             $url_photo = '';
    //             // $Allphotoprod = Product_photo::all()->toArray();
    //             $arrprodimg = $this->service->get_productimg($prod['product_id']);
    //             // print_r($arrprodimg);
    //             // die();
    //             if ($arrprodimg['code'] == 200) {
    //                 $url_photo = $arrprodimg['data'][0]['image'];
    //             }

    //             $htmlprod .= '<div class="product product__style--3 col-lg-4 col-md-4 col-sm-6 col-12">';
    //             $htmlprod .= '    <div class="product__thumb">';
    //             $htmlprod .= '        <a class="first__img" href="detail"><img src="' . $url_photo . '" alt="product image"></a>';
    //             $htmlprod .= '        <a class="second__img animation1" href="' . action('ProductController@detail', ['store_id' => $store_id, 'product_id' => $prod['product_id']]) . '"><img src="' . $url_photo . '" alt="product image"></a>';

    //             if ($prod['is_bblm']) {
    //                 $htmlprod .= '        <div class="hot__box">';
    //                 $htmlprod .= '            <span class="hot-label">BBLM</span>';
    //                 $htmlprod .= '        </div>';
    //             }

    //             $htmlprod .= '    </div>';
    //             $htmlprod .= '    <div class="product__content content--center">';
    //             $htmlprod .= '        <h4><a href="' . action('ProductController@detail', ['store_id' => $store_id, 'product_id' => $prod['product_id']]) . '">' . $prod['product_name'] . '</a></h4>';
    //             $htmlprod .= '        <ul class="prize d-flex">';

    //             if ($prod['is_promo']) {
    //                 $htmlprod .= '            <li>Rp. ' . number_format($prod['price']) . '</li> ';
    //                 $htmlprod .= '            <li class="old_prize">Rp. ' . number_format($prod['base_price']) . '</li>';
    //             } else {
    //                 $htmlprod .= '            <li>Rp. ' . number_format($prod['base_price']) . '</li> ';
    //             }

    //             $htmlprod .= '        </ul>';
    //             $htmlprod .= '        <div class="action">';
    //             $htmlprod .= '            <div class="actions_inner">';
    //             $htmlprod .= '                <ul class="add_to_links">';
    //             $htmlprod .= '                    <li><a class="cart" href="cart.html"><i class="bi bi-shopping-bag4"></i></a></li>';
    //             $htmlprod .= '                    <li><a class="wishlist" href="javascript:void(0)" prod_price="' . number_format($prod['base_price']) . '" prod_quant="' . $prod['min_order'] . '" 
    //                                                 prod_name="' . $prod['product_name'] . '" prod_img="' . $url_photo . '" 
    //                                                 onclick="quantity_modal($(this), \'' . $store_id . '\', \'' . $prod['product_id'] . '\')">
    //                                                 <i class="bi bi-shopping-cart-full"></i></a>
    //                                                 </li>';
    //             $htmlprod .= '                    <li><a class="compare" href="#"><i class="bi bi-heart-beat"></i></a></li>';
    //             $htmlprod .= '                    <li><a data-toggle="modal" title="Quick View" class="quickview modal-view detail-link" href="#productmodal"><i class="bi bi-search"></i></a></li>';
    //             $htmlprod .= '                </ul>';
    //             $htmlprod .= '            </div> ';
    //             $htmlprod .= '        </div>';
    //             $htmlprod .= '    </div>';
    //             $htmlprod .= '</div>';
    //         } 
    //     }else {
    //         $htmlprod = 'Product Not Found';
    //     }

    //     return $htmlprod;
    // }

}
