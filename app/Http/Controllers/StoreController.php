<?php

namespace App\Http\Controllers;

use App\Service;
use Helper;
use Illuminate\Http\Request;

class StoreController extends CheckController
{

    public function __construct()
    {
        parent::__construct();
        // $this->check_login();
        $this->service = new Service();
        $this->data_header = array(
            'upsource' => array(),
            'down_plugins' => array(),
            'down_scripts' => array()
        );
    }

    public function check_login()
    {
        // print_r(session('token'));
        // die();
        // return redirect()->action('LoginController@index');

        // return Session::get('is_login');

        if (!session('token')) {
            return redirect()->action('LoginController@index');
        }
    }

    public function home()
    {
        if (!session('token')) {
            Helper::logout();
            return redirect()->action('LoginController@index');
        }
        #Include script
        $data_header = $this->data_header;

        array_push($data_header['down_scripts'], asset('js/allpage.js'));
        array_push($data_header['down_scripts'], asset('js/login.js'));
        array_push($data_header['upsource'], asset('css/store_cust.css'));

        //ui_sweetalert.html
        array_push($data_header['upsource'], asset('templates/plugins/bootstrap-sweetalert/sweetalert.css'));
        array_push($data_header['down_plugins'], asset('templates/plugins/bootstrap-sweetalert/sweetalert.min.js'));

        //components_select2.html
        array_push($data_header['upsource'], asset('templates/plugins/select2/css/select2.min.css'));
        array_push($data_header['upsource'], asset('templates/plugins/select2/css/select2-bootstrap.min.css'));
        array_push($data_header['down_plugins'], asset('templates/plugins/select2/js/select2.full.min.js'));
        array_push($data_header['down_scripts'], asset('js/component_select2.js'));

        $source['upsource'] = $data_header['upsource'];
        $source['down_plugins'] = $data_header['down_plugins'];
        $source['down_scripts'] = $data_header['down_scripts'];

        $resaddrs = $this->service->check_address();
        // print_r($resaddrs);
        // die();
        if ($resaddrs && $resaddrs['code'] == 200) {
            $sum_addrs = count($resaddrs['data']);
            session()->put('sum_addrs', $sum_addrs);
        }else{
            session()->put('sum_addrs', 0);
        }

        $arrstore = $this->service->get_store();
        // print_r($arrstore);
        // die();
        if (is_array($arrstore)) {
            $Allstore = $arrstore['data'];
        }else{
            $Allstore = [];
        }

        $total = count($Allstore);
        $first = 0;
        $sec = 0;
        $htmlres = '';

        foreach ($Allstore as $store) {
            if ($first == 0) {
                $htmlres .= '<div class="single__product">';
                $htmlres .= '        <div class="col-lg-3 col-md-4 col-sm-6 col-12">';
                $htmlres .= '            <div class="product product__style--3">';
                $htmlres .= '                <div class="product__thumb">';
                $htmlres .= '                    <a class="first__img" href="' . url('home') . '/' . $store['id'] . '"><img class="store" src="' . $store['image_path'] . '" alt="product image"></a>';
                $htmlres .= '                    <a class="second__img animation1" href="' . url('home') . '/' . $store['id'] . '"><img class="store" src="' . $store['image_path'] . '" alt="product image"></a>';
                $htmlres .= '                    <div class="hot__box">';
                $htmlres .= '                        <span class="hot-label">' . $store['name'] . '</span>';
                $htmlres .= '                    </div>';
                $htmlres .= '                </div>';
                $htmlres .= '                <div class="product__content content--center content--center">';
                $htmlres .= '                    <h4><a href="' . url('home') . '/' . $store['id'] . '">' . $store['name'] . '</a></h4>';
                $htmlres .= '                    <div class="product__hover--content">';
                $htmlres .= '                        <h3><a href="' . url('home') . '/'   . $store['id'] . '">Pilih</a></h3>';
                $htmlres .= '                    </div>';
                $htmlres .= '                </div>';
                $htmlres .= '            </div>';
                $htmlres .= '        </div>';

                $first = 1;
                $sec = 0;
            }elseif ($sec == 0) {
                $htmlres .= '<div class="col-lg-3 col-md-4 col-sm-6 col-12">';
                $htmlres .= '            <div class="product product__style--3">';
                $htmlres .= '                <div class="product__thumb">';
                $htmlres .= '                    <a class="first__img" href="' . url('home') . $store['id'] . '"><img class="store" src="' . $store['image_path'] . '" alt="product image"></a>';
                $htmlres .= '                    <a class="second__img animation1" href="' . url('home') . '/' . $store['id'] . '"><img class="store" src="' . $store['image_path'] . '" alt="product image"></a>';
                $htmlres .= '                    <div class="hot__box">';
                $htmlres .= '                        <span class="hot-label">' . $store['name'] . '</span>';
                $htmlres .= '                    </div>';
                $htmlres .= '                </div>';
                $htmlres .= '                <div class="product__content content--center content--center">';
                $htmlres .= '                    <h4><a href="' . url('home') . '/' . $store['id'] . '">' . $store['name'] . '</a></h4>';
                $htmlres .= '                    <div class="product__hover--content">';
                $htmlres .= '                        <h3><a a href="' . url('home') . '/' . $store['id'] . '">Pilih</a></h3>';
                $htmlres .= '                    </div>';
                $htmlres .= '                </div>';
                $htmlres .= '            </div>';
                $htmlres .= '        </div>';
                $htmlres .= '    </div>';

                $first = 0;
                $sec = 1;
            }
        }
        // print_r($htmlres);
        // die();

        return view('store.home', ['htmlStore' => $htmlres, 'source' => $source]);
    }
}
