<?php

// HOME CONTROLLER ROUTE START

Route::get('/store/home', 'StoreController@home');

Route::get('/home/{store_id}', 'HomeController@index');

Route::get('/about', 'HomeController@about');

Route::get('/profile', 'HomeController@profile');

Route::get('/faq', 'HomeController@faq');

Route::get('/skb', 'HomeController@skb');

Route::get('/privasi', 'HomeController@privasi');

Route::get('/get_cust_product/{store_id}/{page}', 'HomeController@get_cust_product');

Route::get('/default', 'HomeController@default');

Route::get('/category/{store_id}', 'HomeController@category');

// HOME CONTROLLER ROUTE END

// LOGIN CONTROLLER ROUTE START

Route::get('/', 'LoginController@index');

Route::get('/logout', 'LoginController@logout');

Route::post('/login/login_member', 'LoginController@login_member');

Route::post('/login/login_otp', 'LoginController@login_otp');

Route::post('/login/regis_guest', 'LoginController@regis_guest');

Route::post('/login/set_address', 'LoginController@set_address');

Route::get('/login/get_regency/{prov_id}', 'LoginController@get_regency');

Route::get('/login/get_address', 'LoginController@get_address');

Route::get('/login/get_full_address', 'LoginController@full_address');

Route::post('/login/update_address', 'LoginController@update_address');

Route::post('/login/get_province', 'LoginController@get_province');

Route::get('/login/get_address_detail/{address_id}/{flag}', 'LoginController@get_address_detail');

Route::get('/login/delete_address/{address_id}', 'LoginController@delete_address');

Route::get('/login/makedefault/{address_id}', 'LoginController@makedefault');

Route::get('/search_maps/{value}', 'LoginController@search_maps');

// LOGIN CONTROLLER ROUTE END

// PRODUCT CONTROLLER ROUTE END

Route::get('/product/{store_id}', 'ProductController@index');

Route::get('/product/category/{store_id}/{cat_id}', 'ProductController@category');

Route::get('/product/table/json', 'ProductController@table');

Route::get('/product/detail/{store_id}/{product_id}/{is_bblm}', 'ProductController@detail');

Route::get('/product/promo/{store_id}', 'ProductController@promo');

Route::get('/product/promo_detail/{store_id}/{promo_id}', 'ProductController@promo_detail');

Route::get('/product/bblm/{store_id}', 'ProductController@bblm');

Route::get('/product/search/{store_id}/{value}', 'ProductController@search');

Route::get('/search_results/{value}', 'ProductController@search_results');

// PRODUCT CONTROLLER ROUTE END

// CART CONTROLLER ROUTE START

Route::get('/cart/index/{store_id}', 'CartController@index');

Route::post('/cart/add_to_cart', 'CartController@add_to_cart');

Route::post('/cart/add_to_cart_again', 'CartController@add_to_cart_again');

Route::get('/cart/get_cart', 'CartController@get_cart');

Route::get('/cart/delete_cart/{store_id}/{product_id}', 'CartController@delete_cart');

Route::get('/cart/delete_cart_all', 'CartController@delete_cart_all');

Route::post('/cart/update_notes', 'CartController@update_notes');

Route::post('/cart/set_final', 'CartController@set_final');

Route::get('/cart/checkout/{store_id}', 'CartController@checkout');

Route::get('/cart/get_delivery_attr', 'CartController@get_delivery_attr');

Route::get('/cart/change_addrs_deliv', 'CartController@change_addrs_deliv');

Route::get('/cart/get_durasi/{service_id}', 'CartController@get_durasi');

Route::match(array('GET','POST'), '/cart/preview', 'CartController@preview');

Route::post('/cart/get_delivery_price', 'CartController@get_delivery_price');

Route::post('/cart/checkout_final', 'CartController@checkout_final');

Route::post('/cart/preview_final', 'CartController@preview_final');

Route::post('/cart/set_chenge_addrs', 'CartController@set_chenge_addrs');

Route::get('/cart/get_data_prod/{store_id}/{product_id}', 'CartController@get_data_prod');

Route::get('/cart/list_voucher/{store_id}', 'CartController@list_voucher');

Route::get('/cart/search_voucher/{store_id}/{code_txt}', 'CartController@search_voucher');

Route::post('/cart/use_voucher', 'CartController@use_voucher');

// CART CONTROLLER ROUTE END

// HISTORY CONTROLLER ROUTE START

Route::get('/history/process/{store_id}', 'HistoryController@process');

Route::get('/history/waiting/{store_id}', 'HistoryController@waiting');

Route::get('/history/delivered/{store_id}', 'HistoryController@delivered');

Route::get('/history/failed/{store_id}', 'HistoryController@failed');

Route::get('/history/detail/{store_id}/{history_id}', 'HistoryController@detail');

Route::get('/history/setstatus/{history_id}/{status_id}', 'HistoryController@setstatus');
// HISTORY CONTROLLER ROUTE END

?>
