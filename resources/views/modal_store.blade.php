<!-- Modal -->
<div id="modal-store" class="modal fade" role="dialog" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog modal-ku">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-body">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <section class="wn__bestseller__area bg--white pt--80  pb--30">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="section__title text-center">
                            <!-- <h2 class="title__be--2"><span class="color--theme">Lotte</span> <span style="color:#3cc8df">Grosir</span></h2> -->
                            <p>Pilih Toko Lotte Grosir Untuk Memulai Proses Belanja</p>
                        </div>
                    </div>
                </div>
                <div class="tab__container mt--60">
                    <!-- Start Single Tab Content -->
                    <div class="row single__tab tab-pane fade show active" id="nav-all">
                        <div class="product__indicator--4 arrows_style owl-carousel owl-theme">
                            <?= $htmlStore; ?>
                        </div>
                    </div>
                    <!-- End Single Tab Content -->
                </div>
            </div>
        </section>
      </div>
    </div>

  </div>
</div>