@extends('layout.master')

@section('title', 'Lotte Grosir | Selamat Datang di Lotte Grosir ' . session('storedata.name'))

@section('upsource', view('layout.upsource', ['source' => $source['upsource']]))

@section('nav', view('layout.nav'))

@section('mobilemenu', view('layout.mobilemenu', ['heads' => ['search', 'card', 'store', 'cart', 'address', 'logout'], 'search' => '']))

@section('headbar', view('layout.headbar', ['heads' => ['search', 'card', 'store', 'cart', 'address', 'logout'], 'search' => '']))

@section('footer', view('layout.footer'))

@section('downsource', view('layout.downsource', ['plugins' => $source['down_plugins'], 'scripts' => $source['down_scripts']]))

@section('header', view('layout.header', ['name' => session('storedata.name')]))

@if (count($arrpromo) > 0)
    @section('promo', view('layout.promo', ['arrpromo' => $arrpromo]))
@endif

@section('modal', view('modal_store', ['htmlStore' => $htmlStore]))

@section('modal_cart', view('parts.modal_cart'))

@section('content')

<section class="wn__bestseller__area bg--white pb--30">
	<input type="hidden" name="store_id" id="store_id" value="<?= session('storedata.id') ?>">
	<input type="hidden" name="page" id="page" value="2">
    <div class="container">
        <!-- <div class="row mt--50">
            <div class="col-md-12 col-lg-12 col-sm-12">
                <div class="product__nav nav justify-content-center" role="tablist">
                    <a class="nav-item nav-link">BBLM</a>
                    <a class="nav-item nav-link" href="<?= action('ProductController@index', ['store_id' => session('storedata.id')]); ?>">Semua Produk</a>
                </div>
            </div>
        </div> -->
        <div class="tab__container mt--30">
			<div class="shop-grid tab-pane fade show active" id="nav-grid" role="tabpanel">
				<div class="row" id="main_prod">
					<?= $htmlProd; ?>
				</div>
				<div id="down_prod">
					
				</div>
                <div id="loading_page" style="text-align: center; display: none;">
					<img style="width: 100px;" src="<?= url('/img/pacman.gif'); ?>" alt="">
				</div>
			</div>
		</div>
    </div>
</section>
@endsection