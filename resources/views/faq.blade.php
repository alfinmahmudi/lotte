@extends('layout.master')

@section('title', 'Lotte Grosir | Kenali Kami')

@section('upsource', view('layout.upsource', ['source' => $source['upsource']]))

@section('mobilemenu', view('layout.mobilemenu', ['heads' => ['search', 'card', 'store', 'cart', 'address', 'logout']]))

@if (session('is_login'))
    @section('headbar', view('layout.headbar', ['heads' => ['address', 'logout']]))
@else
    @section('headbar', view('layout.headbar', ['heads' => ['login']]))
@endif

@section('footer', view('layout.footer'))

@section('downsource', view('layout.downsource', ['plugins' => $source['down_plugins'], 'scripts' => $source['down_scripts']]))

@section('content')

<section class="wn__faq__area bg--white pt--80 pb--60">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="section__title--3 text-center pb--30">
                    <h2>FREQUENTLY ASKED QUESTION</h2>
                </div>
                <div class="wn__accordeion__content">
                    <h2>1.	Keanggotaan</h2>
                </div>
                <div id="accordion" class="wn_accordion" role="tablist">
                    <div class="card">
                        <div class="acc-header" role="tab" id="headingOne">
                            <h5>
                                <a data-toggle="collapse" href="#collapse1_1" role="button" aria-expanded="true" aria-controls="collapseOne">
                                    1.1	Bagaimana bila saya adalah pelanggan Lotte Grosir tetapi tidak dapat melakukan online order dengan nomer hp saya?
                                </a>
                            </h5>
                        </div>
                        <div id="collapse1_1" class="collapse " role="tabpanel" aria-labelledby="headingOne" data-parent="#accordion">
                            <div class="card-body">
                                Hubungi Pusat Pelayanan Pelanggan (CIS) atau Direct Line Lotte Grosir terdekat.
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="acc-header" role="tab" id="headingOne">
                            <h5>
                                <a data-toggle="collapse" href="#collapse1_2" role="button" aria-expanded="true" aria-controls="collapseOne">
                                    1.2	Bagaimana bila saya belum terdaftar sebagai member Lotte Grosir tapi ingin melakukan online order?
                                </a>
                            </h5>
                        </div>
                        <div id="collapse1_2" class="collapse " role="tabpanel" aria-labelledby="headingOne" data-parent="#accordion">
                            <div class="card-body">
                                Anda bisa log in sebagai tamu dan mendapatkan ID tamu sementara untuk log in
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="acc-header" role="tab" id="headingOne">
                            <h5>
                                <a data-toggle="collapse" href="#collapse1_3" role="button" aria-expanded="true" aria-controls="collapseOne">
                                    1.3	Setelah melakukan pendaftaran pada Apps Lotte Grosir , apakah saya dapat memperbaharui informasi data diri saya?	
                                </a>
                            </h5>
                        </div>
                        <div id="collapse1_3" class="collapse " role="tabpanel" aria-labelledby="headingOne" data-parent="#accordion">
                            <div class="card-body">
                                Ya, Anda dapat melakukan pembaharuan informasi data diri terbaru Anda melalui Akun Anda lalu klik Edit.
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="acc-header" role="tab" id="headingOne">
                            <h5>
                                <a data-toggle="collapse" href="#collapse1_4" role="button" aria-expanded="true" aria-controls="collapseOne">
                                    1.4	Apakah data diri saya aman ?		
                                </a>
                            </h5>
                        </div>
                        <div id="collapse1_4" class="collapse " role="tabpanel" aria-labelledby="headingOne" data-parent="#accordion">
                            <div class="card-body">
                                Kami menjamin kerahasiaan semua data informasi pribadi Anda.
                            </div>
                        </div>
                    </div>
                </div>
                <div class="wn__accordeion__content">
                    <h2>2.	Pemesanan</h2>
                </div>
                <div id="accordion" class="wn_accordion" role="tablist">
                    <div class="card">
                        <div class="acc-header" role="tab" id="headingOne">
                            <h5>
                                <a data-toggle="collapse" href="#collapse2_1" role="button" aria-expanded="true" aria-controls="collapseOne">
                                    2.1	Bagaimana cara saya untuk menemukan produk yang saya inginkan di Lotte Grosir Online Order?	
                                </a>
                            </h5>
                        </div>
                        <div id="collapse2_1" class="collapse " role="tabpanel" aria-labelledby="headingOne" data-parent="#accordion">
                            <div class="card-body">
                                Anda dapat dengan mudah mencari beragam produk yang Anda inginkan melalui Kategori Produk yang tersedia atau menggunakan fitur pencarian
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="acc-header" role="tab" id="headingOne">
                            <h5>
                                <a data-toggle="collapse" href="#collapse2_2" role="button" aria-expanded="true" aria-controls="collapseOne">
                                    2.2	Bagaimana cara mengurangi atau menambahkan produk-produk yang saya masukkan ke Keranjang Belanja?
                                </a>
                            </h5>
                        </div>
                        <div id="collapse2_2" class="collapse " role="tabpanel" aria-labelledby="headingOne" data-parent="#accordion">
                            <div class="card-body">
                                Dari Keranjang Belanja Anda, klik tanda plus (+) atau minus (-) untuk menambah atau mengurangi jumlah barang yang anda pesan. Atau klik Hapus, jika ingin membatalkan pesanan barang tersebut
                            </div>
                        </div>
                    </div>
                </div>
                <div class="wn__accordeion__content">
                    <h2>3.	Pengambilan Barang</h2>
                </div>
                <div id="accordion" class="wn_accordion" role="tablist">
                    <div class="card">
                        <div class="acc-header" role="tab" id="headingOne">
                            <h5>
                                <a data-toggle="collapse" href="#collapse3_1" role="button" aria-expanded="true" aria-controls="collapseOne">
                                    3.1	Kapan saya dapat mengambil barang yang saya pesan melalui Lotte Grosir Online Order?	
                                </a>
                            </h5>
                        </div>
                        <div id="collapse3_1" class="collapse " role="tabpanel" aria-labelledby="headingOne" data-parent="#accordion">
                            <div class="card-body">
                                a.	Pengambilan barang hanya bisa dilakukan sesuai jam operasional toko <br>
                                b.	Jadwal pengambilan barang hanya bisa dilakukan di satu toko, yaitu toko yang sudah ditentukan sebelumnya pada saat login pertama <br>
                                c.	Pengambilan barang di store dapat dilakukan 3 jam setelah konfirmasi pelunasan dan status pesanan berubah menjadi "Siap Diambil" (sesuai dengan jam operasional toko) <br>
                                d.	Adapun batas maksimum Lotte Grosir Online Order untuk mempersiapkan pesanan yaitu 7 (tujuh) hari kerja dari tanggal Konfirmasi Pemesanan.
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="acc-header" role="tab" id="headingOne">
                            <h5>
                                <a data-toggle="collapse" href="#collapse3_2" role="button" aria-expanded="true" aria-controls="collapseOne">
                                    3.2	Bagaimana cara pengambilan barang tersebut?
                                </a>
                            </h5>
                        </div>
                        <div id="collapse3_2" class="collapse " role="tabpanel" aria-labelledby="headingOne" data-parent="#accordion">
                            <div class="card-body">
                                a.	Pelanggan menunjukkan email konfirmasi pembayaran dan bukti transfer pada CIS<br>
                                b.	Tim CIS melakukan cross check disistem mengenai bukti transfer dan konfirmasi pembayaran yang dibawa atau ditunjukkan<br>
                                c.	Apabila status sudah OK (sudah dilakukan pembayaran) maka Tim PIC Online membuat update pada backend/dashboard Online Order mengenai pengambilan barang<br>
                                d.	Sistem akan mengirimkan notifikasi melalui sms untuk setiap transaksi online order yang sudah selesai
                            </div>
                        </div>
                    </div>
                </div>
                <div class="wn__accordeion__content">
                    <h2>4.	Pengiriman Barang</h2>
                </div>
                <div id="accordion" class="wn_accordion" role="tablist">
                    <div class="card">
                        <div class="acc-header" role="tab" id="headingOne">
                            <h5>
                                <a data-toggle="collapse" href="#collapse4_1" role="button" aria-expanded="true" aria-controls="collapseOne">
                                    4.1	Kapan dilakukan pengiriman barang yang saya pesan melalui Lotte Grosir Online Order?	
                                </a>
                            </h5>
                        </div>
                        <div id="collapse4_1" class="collapse " role="tabpanel" aria-labelledby="headingOne" data-parent="#accordion">
                            <div class="card-body">
                                a.	Jadwal pengiriman barang hanya dapat dilakukan di satu toko, yaitu toko yang sudah ditentukan sebelumnya pada saat login pertama <br>
                                b.	Pengiriman barang untuk konfirmasi pembelian sebelum pukul 14.00 WIB, maka pihak kurir/delivery yang dipilih akan melakukan pick up pada hari yang sama setelah pukul 16.00<br>
                                c.	Pengiriman barang untuk konfirmasi pembelian setelah pukul 14.00 WIB, maka kurir/delivery yang dipilih akan melakukan pick up H+1 setelah pukul 09.00<br>
                                d.	Untuk mengetahui status pengiriman secara detail, pelanggan dapat mengecek melalui Apps Lotte Grosir Online order
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="acc-header" role="tab" id="headingOne">
                            <h5>
                                <a data-toggle="collapse" href="#collapse4_2" role="button" aria-expanded="true" aria-controls="collapseOne">
                                    4.2	Armada apa yang digunakan untuk melakukan pengiriman barang?
                                </a>
                            </h5>
                        </div>
                        <div id="collapse4_2" class="collapse " role="tabpanel" aria-labelledby="headingOne" data-parent="#accordion">
                            <div class="card-body">
                                Lotte Grosir bekerjasama dengan armada Ritase dan The Lorry untuk melakukan pengiriman barang yang dipesan melalui Lotte Grosir Online Order
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="acc-header" role="tab" id="headingOne">
                            <h5>
                                <a data-toggle="collapse" href="#collapse4_3" role="button" aria-expanded="true" aria-controls="collapseOne">
                                    4.3	Apakah saya dapat membatalkan pesanan saya?		
                                </a>
                            </h5>
                        </div>
                        <div id="collapse4_3" class="collapse " role="tabpanel" aria-labelledby="headingOne" data-parent="#accordion">
                            <div class="card-body">
                                Pembeli dapat membatalkan pembelian setiap saat sebelum order dibayarkan. Ketika order dalam status konfirmasi pembelian, pesanan akan dianggap batal apabila tidak ada pembayaran dalam waktu 6 (enam) jam setelah konfirmasi pembelian dilakukan
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="acc-header" role="tab" id="headingOne">
                            <h5>
                                <a data-toggle="collapse" href="#collapse4_4" role="button" aria-expanded="true" aria-controls="collapseOne">
                                    4.4	Mengapa saya tidak dapat meneruskan proses pesanan saya?		
                                </a>
                            </h5>
                        </div>
                        <div id="collapse4_4" class="collapse " role="tabpanel" aria-labelledby="headingOne" data-parent="#accordion">
                            <div class="card-body">
                            Apabila tidak ada pembayaran dalam waktu 6 (enam) jam setelah konfirmasi pembelian maka pesanan dianggap batal
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="acc-header" role="tab" id="headingOne">
                            <h5>
                                <a data-toggle="collapse" href="#collapse4_5" role="button" aria-expanded="true" aria-controls="collapseOne">
                                    4.5	Apakah barang yang saya beli dapat ditukar			
                                </a>
                            </h5>
                        </div>
                        <div id="collapse4_5" class="collapse " role="tabpanel" aria-labelledby="headingOne" data-parent="#accordion">
                            <div class="card-body">
                                Pembeli tidak dapat mengajukan permintaan penukaran atau pengembalian barang atas pesanan sesuai dengan peraturan yang berlaku di toko pembelian	
                            </div>
                        </div>
                    </div>
                </div>
            </div> 
        </div> 
    </div> 
</section>
@endsection