@extends('layout.master')

@section('title', 'Lotte Grosir | Pilih Toko')

@section('upsource', view('layout.upsource', ['source' => $source['upsource']]))

@section('headbar', view('layout.headbar', ['heads' => ['address', 'logout']]))

@section('footer', view('layout.footer'))

@section('downsource', view('layout.downsource', ['scripts' => $source['down_scripts'], 'plugins' => $source['down_plugins']]))

@section('content')
<meta name="csrf-token" content="{{ csrf_token() }}" />

<!-- Start Best Seller Area -->
<section class="wn__bestseller__area bg--white pt--80  pb--30">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="section__title text-center">
                    <!-- <h2 class="title__be--2"><span class="color--theme">
                        Lotte</span> <span style="color:#3cc8df">Grosir</span></h2> -->
                    <p>Pilih Toko Lotte Grosir Untuk Memulai Proses Belanja</p>
                </div>
            </div>
        </div>
        <div class="tab__container mt--60">
            <!-- Start Single Tab Content -->
            <div class="row single__tab tab-pane fade show active" id="nav-all">
                <div class="product__indicator--4 arrows_style owl-carousel owl-theme">
                    <?= $htmlStore; ?>
                </div>
            </div>
            <!-- End Single Tab Content -->
        </div>
    </div>
</section>

<input type="hidden" id="is_login" value="<?= session('is_login'); ?>">

<div id="modal-login" class="modal fade" role="dialog" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-body">
        <button type="button" class="close" data-dismiss="modal" style="display: none;">&times;</button>
        <section class="bg--white">
            <div class="container" id="div_otp" style="display: none;">
                <form action="#">
                    <div class="row account__form" style="border: none;">
                        <div class="col-sm-12">
                            <p class="small-head">Verifikasi nomor telepon Anda</p>
                            <p class="small-p">Kami akan mengirimkan kode 6 digit lewat SMS untuk memverifikasi nomor telepon Anda.</p>
                        </div>
                        <div class="confirmation_code split_input small_top_margin" data-multi-input-code="true">
                            <div class="confirmation_code_group">
                                <div class="split_input_item input_wrapper"><input type="text" id="code1" onkeyup="movefocus(this.form, this, 'code2', event, '');" class="inline_input" maxlength="1"></div>
                                <div class="split_input_item input_wrapper"><input type="text" id="code2" onkeyup="movefocus(this.form, this, 'code3', event, 'code1');" class="inline_input" maxlength="1"></div>
                                <div class="split_input_item input_wrapper"><input type="text" id="code3" onkeyup="movefocus(this.form, this, 'code4', event, 'code2');" class="inline_input" maxlength="1"></div>
                            </div>

                            <div class="confirmation_code_span_cell">—</div>

                            <div class="confirmation_code_group">
                                <div class="split_input_item input_wrapper"><input type="text" id="code4" onkeyup="movefocus(this.form, this, 'code5', event, 'code3');" class="inline_input" maxlength="1"></div>
                                <div class="split_input_item input_wrapper"><input type="text" id="code5" onkeyup="movefocus(this.form, this, 'code6', event, 'code4');" class="inline_input" maxlength="1"></div>
                                <div class="split_input_item input_wrapper"><input type="text" id="code6" onkeyup="movefocus(this.form, this, '', event, 'code5');" class="inline_input" maxlength="1"></div>
                            </div>
                        </div>
                        <div class="col-sm-12 text-center-cust">
                            <button type="button" style="width: 100%;" class="btn-cust" div_now="div_otp" href="javascript:void(0)" onclick="showdiv($(this), 'div_acc')">Verifikasi</button>
                        </div>
                    </div>
                </form>
            </div>

            <div class="container" id="div_succ" style="display: none;">
                <form action="#">
                    <div class="row account__form" style="border: none;">
                        <div class="col-sm-12">
                            <p class="small-head">Registrasi Berhasil</p>
                            <p class="small-p">Terima kasih telah mendaftarkan diri anda sebagai TAMU. Berikut ini adalah ID Tamu anda :</p>
                        </div>
                        <div class="col-sm-12 text-center-cust pb--10 pt--10">
                            <h3 class="small-head" id="succ_card_no"></h3>
                        </div>    
                        <div class="col-sm-12">
                            <p class="small-p" style="color: red;">*Nomor ID Tamu ini bukan ID member Lotte Grosir</p>
                        </div>
                        <div class="col-sm-12 text-center-cust">
                            <button type="button" style="width: 100%;" class="btn-cust" div_now="div_succ" href="javascript:void(0)" onclick="showdiv($(this), 'div_acc')">Lanjutkan</button>
                        </div>
                    </div>
                </form>
            </div>
            
            <form action="{{ url('/login/login_member') }}" method="POST" name="form_login_member" id="form_login_member" enctype="multipart/form-data">
                <div class="container" id="div_login">
                    <div class="row account__form" style="border: none;">
                        <div class="col-sm-12 pb--10">
                            <h3 class="small-head">Masuk</h3>
                        </div>
                        <div class="input__box col-sm-12">
                            <label>Nomor Telepon<span>*</span></label>
                            <input type="text" name="phone" id="phone">
                        </div>
                        <div class="input__box col-sm-12">
                            <label>Nomor Kartu Member<span>*</span></label>
                            <input type="text" id="card_no" name="card_no">
                        </div>
                        <div class="col-sm-12 text-center-cust">
                            <button type="button" style="width: 100%;" class="btn-cust" div_now="div_login" href="javascript:void(0)" onclick="post_data($(this), 'div_acc', form_login_member)" style="width: 100%;">Selanjutnya</button>
                        </div>
                        <a class="forget_pass" div_now="div_login" href="javascript:void(0)" onclick="showdiv($(this), 'div_tamu')">Masuk Sebagai Tamu <i class="fa fa-chevron-right"></i></a>
                    </div>
                </div>
            </form>
            
            <form action="{{ url('/login/regis_guest') }}" method="POST" name="form_regis_guest" id="form_regis_guest" enctype="multipart/form-data">
                <div class="container" id="div_tamu" style="display: none;">
                    <div class="row account__form" style="border: none;">
                        <div class="col-sm-12">
                            <h3 class="small-head">Masuk Sebagai Tamu</h3>
                        </div>
                        <div class="input__box col-sm-12">
                            <label>Nama<span>*</span></label>
                            <input type="text" id="name" name="name">
                        </div>
                        <div class="input__box col-sm-12">
                            <label>Email<span>*</span></label>
                            <input type="text" id="email" name="email">
                        </div>
                        <div class="input__box col-sm-12">
                            <label>Nomor Telepon<span>*</span></label>
                            <input type="text" id="phone" name="phone">
                        </div>
                        <div class="input__box col-sm-12">
                            <label>Gender<span>*</span></label>
                            <select name="gender" id="gender">
                                <option value="1">Male</option>
                                <option value="2">Female</option>
                            </select>
                        </div>
                        <div class="col-sm-12 text-center-cust">
                            <button type="button" style="width: 100%;" class="btn-cust" div_now="div_tamu" href="javascript:void(0)" onclick="post_data($(this), 'div_succ', form_regis_guest)" style="width: 100%;">Selanjutnya</button>
                        </div>
                        <a class="forget_pass" div_now="div_tamu" href="javascript:void(0)" onclick="showdiv($(this), 'div_login')"><i class="fa fa-chevron-left"></i> Kembali ke Sebelumnya</a>
                    </div>
                </div>
            </form>
            
            <form action="{{ url('/login/set_address') }}" method="POST" name="form_regis_addrs" id="form_regis_addrs" enctype="multipart/form-data">
                <div class="container" id="div_acc" style="display: none;">
                    <div class="row account__form" style="border: none;">
                        <div class="col-sm-12">
                            <h3 class="small-head">Tambah Alamat</h3>
                        </div>
                        <div class="input__box col-lg-6 col-sm-12">
                            <label>Nama Sebagai<span>*</span></label>
                            <input type="text" name="title" id="title">
                        </div>
                        <div class="input__box col-lg-6 col-sm-12">
                            <label>Nama Penerima<span>*</span></label>
                            <input type="text" name="customer_name" id="customer_name">
                        </div>
                        <div class="input__box col-lg-6 col-sm-12">
                            <label>Telepon Penerima<span>*</span></label>
                            <input type="text" name="phone" id="phone">
                        </div>
                        <div class="input__box col-lg-6 col-sm-12">
                            <label>Alamat Penerima<span>*</span></label>
                            <textarea type="text" name="address" id="address"></textarea>
                        </div>
                        <div class="input__box col-lg-6 col-sm-12">
                            <label>Email Penerima<span>*</span></label>
                            <input type="text" name="email" id="email">
                        </div>
                        <div class="input__box col-lg-6 col-sm-12">
                            <label>Kode POS<span>*</span></label>
                            <input type="text" name="zip_code" id="zip_code">
                        </div>
                        <div class="input__box col-lg-6 col-sm-12">
                            <label>Provinsi<span>*</span></label>
                            <select class="select2" onchange="get_regency($(this))" name="province_id" id="province_id">
                                
                            </select>
                        </div>
                        <div class="input__box col-lg-6 col-sm-12">
                            <label>Kabupaten / Kota<span>*</span></label>
                            <select class="select2" name="regency_id" id="regency_id">
            
                            </select>
                        </div>
                        <div class="col-sm-12 text-center-cust">
                            <button type="button" style="width: 100%;" class="btn-cust" div_now="div_tamu" href="javascript:void(0)" onclick="post_data($(this), 'div_succ', form_regis_addrs)" style="width: 100%;">Simpan</button>
                        </div>
                    </div>
                </div>
            </form>
		</section>
      </div>
    </div>

  </div>
</div>
<!-- Start BEst Seller Area -->

@endsection

