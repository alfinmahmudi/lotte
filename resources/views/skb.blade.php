@extends('layout.master')

@section('title', 'Lotte Grosir | Kenali Kami')

@section('upsource', view('layout.upsource', ['source' => $source['upsource']]))

@section('mobilemenu', view('layout.mobilemenu', ['heads' => ['search', 'card', 'store', 'cart', 'address', 'logout']]))

@if (session('is_login'))
    @section('headbar', view('layout.headbar', ['heads' => ['address', 'logout']]))
@else
    @section('headbar', view('layout.headbar', ['heads' => ['login']]))
@endif

@section('footer', view('layout.footer'))

@section('downsource', view('layout.downsource', ['plugins' => $source['down_plugins'], 'scripts' => $source['down_scripts']]))

@section('content')

<div class="page-about about_area bg--white section-padding--lg">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="section__title--3 text-center pb--30">
                    <h2>SYARAT DAN KETENTUAN APLIKASI LOTTE GROSIR</h2>
                </div>
            </div>
        </div>
        <div class="row align-items-center">
            <div class="col-lg-12 col-sm-12 col-12">
                <div class="content">
                    <table>
                        <tr>
                            <td style="vertical-align: top;"><p><strong>1. </strong></p></td>
                            <td>
                                <p>Syarat-syarat dan ketentuan-ketentuan ini merupakan syarat-syarat dan ketentuan-ketentuan perjanjian kami yang akan berlaku untuk semua pembelian Anda atas produk-produk pada aplikasi Lotte Grosir atau www.lottegrosir.co.id dan Anda harus membacanya dengan teliti. Alamat web ini merupakan milik PT. Lotte Shopping Indonesia, beralamat di Jalan Lingkar Luar Selatan No. 6 Ciracas, Jakarta Timur, Indonesia.</p>
                                <p>PT. Lotte Shopping Indonesia untuk selanjutnya disebut sebagai “Kami”. Kami dapat merubah syarat-syarat ini dari waktu ke waktu dan karenanya Anda harus mengeceknya sebelum Anda melakukan pembelian baru pada setiap kegiatan berkaitan dengan jual beli dari aplikasi Lotte Grosir atau www.lottegrosir.co.id.</p>
                            </td>
                        </tr>
                        <tr>
                            <td style="vertical-align: top;"><p><strong>2. </strong></p></td>
                            <td>
                                <strong>Kebijakan Pemesanan</strong>
                                <table>
                                    <tr>
                                        <td style="vertical-align: top;">2.1</td>
                                        <td>
                                            Setiap Produk yang ditawarkan untuk Anda melalui situs Kami, Kami menyediakan informasi produk berupa jenis produk, keterangan produk dan harga produk yang Kami sediakan untuk Anda yang bertujuan untuk membantu Anda melakukan pemilihan dan pemesanan produk.
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="vertical-align: top;">2.2</td>
                                        <td>
                                            Kami akan mengkonfirmasikan rincian pesanan Anda melalui email/telpon/sarana komunikasi lain yang tersedia, sehingga memungkinkan Kami untuk memproses pesanan yang dilakukan oleh Anda melalui Situs Kami. Untuk memastikan bahwa pesanan yang Anda lakukan telah sesuai dengan pilihan Anda dan sesuai dengan kondisi stok Kami. Sehingga jika sudah melalui proses konfirmasi pesanan, maka pesanan tidak dapat dibatalkan kembali.
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="vertical-align: top;">2.3</td>
                                        <td>
                                            Anda setuju dan bersedia untuk memberikan kepada Kami rincian pesanan Anda yang sudah lengkap dan akurat ketika diminta oleh Kami.
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="vertical-align: top;">2.4</td>
                                        <td>
                                            Apabila atas suatu sebab tertentu, ada suatu Produk yang Anda pesan tidak tersedia maka Kami tidak berkewajiban untuk mengganti pesanan dengan Produk lain yang sejenis, tetapi Kami akan berusaha untuk menginformasikan Anda untuk memberikan konfirmasi kepada Anda.
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="vertical-align: top;">2.5</td>
                                        <td>
                                            Kami berhak untuk membatasi kuantitas produk atau jasa yang Kami tawarkan. Semua deskripsi produk atau harga produk dapat berubah sewaktu-waktu tanpa pemberitahuan. Kami berhak untuk menghentikan setiap produk setiap saat.
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td style="vertical-align: top;"><p><strong>3. </strong></p></td>
                            <td>
                                <strong>Kebijakan Penerimaan & Penolakan Pemesanan</strong>
                                <table>
                                    <tr>
                                        <td style="vertical-align: top;">3.1</td>
                                        <td>
                                            Kami berhak untuk menerima atau menolak pesanan Anda untuk alasan apapun, termasuk jika produk yang diminta tidak tersedia atau jika ada kesalahan dalam harga atau informasi produk yang dipasang di Situs Kami atau dalam pesanan Anda atau adanya ketidak sesuaian biaya kirim.
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="vertical-align: top;">3.2</td>
                                        <td>
                                            Jika Kami menolak pesanan yang Anda pesan melalui Situs Kami, maka Kami akan menginformasikan kepada Anda tentang penolakan tersebut pada waktu yang wajar setelah anda melakukan pembayaran, namun karena terdapat hal tertentu Kami tidak dapat memprosesnya, maka pesanan Anda harus Kami tolak. 
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="vertical-align: top;">3.3</td>
                                        <td>
                                            Kami berhak menolak pesanan Anda bila tidak sesuai dengan program promo atau bila pesanan diluar jangkuan area Toko Kami atau ada ketidaksesuaian lainya. 
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td style="vertical-align: top;"><p><strong>4. </strong></p></td>
                            <td>
                                <strong>Kebijakan Pembatalan Pesanan</strong>
                                <table>
                                    <tr>
                                        <td style="vertical-align: top;">4.1</td>
                                        <td>
                                            Anda dapat membatalkan pesanan setiap saat sebelum order dalam status konfirmasi pembayaran.
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="vertical-align: top;">4.2</td>
                                        <td>
                                            Ketika order sudah dalam status konfirmasi pembayaran, pembatalan akan mengacu pada Ketentuan Lotte Grosir.
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="vertical-align: top;">4.3</td>
                                        <td>
                                            Lotte Grosir dapat membatalkan persiapan pesanan apabila produk yang di order telah kosong/tidak tersedia, salah informasi harga dan bila ada cacat produk dengan memberikan informasi terlebih dahulu kepada pelanggan.
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="vertical-align: top;">4.4</td>
                                        <td>
                                            Lotte Grosir berhak membatalkan pesanan bila pihak pembeli tidak melakukan pengambilan pesanan sesuai konfirmasi ordernya.
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="vertical-align: top;">4.5</td>
                                        <td>
                                            Pelanggan berhak mendapatkan pengembalian uang jika pembatalan dilakukan oleh Lotte Grosir dalam periode minimal 14 (empat belas) hari kerja.
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td style="vertical-align: top;"><p><strong>5. </strong></p></td>
                            <td>
                                <strong>Kebijakan Pengambilan Pesanan</strong>
                                <table>
                                    <tr>
                                        <td style="vertical-align: top;">5.1</td>
                                        <td>
                                            Batas maksimum Lotte Grosir Online Order mempersiapkan pesanan yaitu 7 (tujuh) hari kerja dari tanggal konfirmasi pembayaran.
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="vertical-align: top;">5.2</td>
                                        <td>
                                            Lokasi pengambilan pesanan disesuaikan dengan lokasi yang Anda pilih ketika melakukan pesanan online order. Sebelum 
                                            melakukan konfirmasi pembelian, sistem secara default akan memberikan pilihan tanggal dan waktu pengambilan barang atas 
                                            pesanan online order anda. Ketentuan waktu pengambilan sbb:"Produk yang Anda pesan dapat diambil 3 jam setelah konfirmasi pembayaran dilakukan, dengan syarat status pesanan berubah menjadi Siap Diambil dan pesanan diambil pada jam operasional toko".
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="vertical-align: top;">5.3</td>
                                        <td>
                                            Prosedur Pengambilan Barang
                                            <table>
                                                <tr>
                                                    <td>1)</td>
                                                    <td>Pelanggan menunjukkan sms / notifikasi / email konfirmasi pembayaran.</td>
                                                </tr>
                                                <tr>
                                                    <td>2)</td>
                                                    <td>Tim  Online Order melakukan cross check di sistem mengenai bukti transfer dan konfirmasi pembayaran yang dibawa 
                                                        atau ditunjukkan pelanggan.
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>3)</td>
                                                    <td>
                                                        Apabila status sudah OK (sudah dilakukan pembayaran) maka Tim CIS membuat update pada backend mengenai pengambilan barang
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td style="vertical-align: top;"><p><strong>6.</strong></p></td>
                            <td>
                                <strong>Kebijakan Pengiriman Pesanan</strong>
                                <table>
                                    <tr>
                                        <td style="vertical-align: top;">6.1</td>
                                        <td>
                                            Batas maksimum Lotte Grosir Online Order mempersiapkan pesanan yaitu 14 hari kerja dari tanggal konfirmasi pembayaran.
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="vertical-align: top;">6.2</td>
                                        <td>
                                            Lokasi pengiriman pesanan disesuaikan dengan lokasi yang Anda pilih ketika melakukan pesanan online order. Estimasi 
                                            barang akan diterima tergantung delivery dan type pengiriman yang dipilih pelanggan.
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="vertical-align: top;">6.3</td>
                                        <td>
                                            Prosedur Pengiriman Barang
                                            <table>
                                                <tr>
                                                    <td>1)</td>
                                                    <td>Pelanggan mendapatkan email konfirmasi pembayaran dengan rincian barang yang dipesan beserta estimasi waktu pengiriman.</td>
                                                </tr>
                                                <tr>
                                                    <td>2)</td>
                                                    <td>Tahapan selanjutnya adalah pengiriman barang dari toko.
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>3)</td>
                                                    <td>
                                                        The Lorry ditunjuk sebagai pihak ketiga untuk mengumpulkan dan mengengemas produk yang telah dipesan pelanggan melakukan 
                                                        serah terima ke pihak delivery mengirimkan barang pesanan sesuai alamat pada email konfirmasi pembayaran dan lokasi yang dipilih sebelumnya
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>4)</td>
                                                    <td>
                                                        Kami akan melakukan serah terima barang yang akan dikirim ke Anda (pelanggan/customer) kepada Pihak ketiga (kurir/delivery) untuk dilakukan proses pengiriman dan bila sudah dilakukan serah terima maka barang tersebut akan menjadi Tanggung Jawab pihak ketiga (kurir/delivery).
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>5)</td>
                                                    <td>
                                                        Pelanggan menerima barang pesanan dan invoice.
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td style="vertical-align: top;"><p><strong>7.</strong></p></td>
                            <td>
                                <strong>Penukaran & Pengembalian</strong>
                                <table>
                                    <tr>
                                        <td style="vertical-align: top;">7.1</td>
                                        <td>
                                            Penukaran / Pengembalian Produk
                                            <table>
                                                <tr>
                                                    <td>1)</td>
                                                    <td>Pembeli tidak dapat mengajukan permintaan penukaran atau pengembalian produk setelah menerima barang dan menandatangani Surat Tanda Terima Pengiriman dari Pihak Delivery.</td>
                                                </tr>
                                                <tr>
                                                    <td>2)</td>
                                                    <td>Anda (Customer) wajib memeriksa barang saat melakukan penerimaan dari Pihak Ke Tiga (Delivery) dan memastikan semua barang sesuai dan dalam kondisi baik.</td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td style="vertical-align: top;"><p><strong>8. </strong></p></td>
                            <td>
                                <strong>Kebijakan Atas Kekayaan Intelektual</strong>
                                <table>
                                    <tr>
                                        <td style="vertical-align: top;">8.1</td>
                                        <td>
                                            Semua isi, konten, materi, data, deskripsi, gambar maupun informasi yang ditampilkan pada Situs maupun yang ditempatkan atau terdapat 
                                            dalam Situs termasuk tetapi tidak terbatas pada teks, grafik, logo, tombol ikon, gambar, klip audio, digital download, judul kampanye dan/atau 
                                            kompilasi data milik www.lottegrosir.co.id. Anda tidak diizinkan untuk mempublikasikan, memanipulasi, mendistribusikan, menggandakan, 
                                            menyebarluaskan dan/atau mereproduksi dengan cara maupun dalam bentuk atau format apapun, satu atau lebih isi, konten, materi, data, deskripsi, 
                                            gambar maupun informasi yang ditampilkan pada Situs maupun salinannya untuk keperluan apapun termasuk tetapi tidak terbatas untuk keperluan bisnis 
                                            Anda maupun dalam rangka kerjasama dengan pihak manapun.
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="vertical-align: top;">8.2</td>
                                        <td>
                                            Setiap grafis, logo, judul halaman, tombol ikon, teks, maupun nama layanan yang termasuk dalam atau tersedia dalam Situs adalah merek dagang 
                                            milik kami atau pemegang lisensi. Merek dagang www.lottegrosir.co.id tidak dapat dengan cara apapun dan dalam kondisi apapun digunakan dalam kaitannya 
                                            dengan barang, jasa, produk dan/atau hal lainnya yang tidak tersedia pada Situs kami, termasuk dengan cara apapun yang atau mungkin dapat menyebabkan 
                                            kebingungan di kalangan publik maupun Pelanggan atau dengan cara yang meremehkan atau mendiskreditkan www.lottegrosir.co.id, Situs dan/atau. Semua 
                                            merek dagang atau [hak atas kekayaan intelektual lainnya] yang tidak dimiliki oleh kami yang muncul di Situs adalah milik masing-masing pemiliknya.
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="vertical-align: top;">8.3</td>
                                        <td>
                                            Anda tidak boleh menggunakan bagian manapun dari Situs kami maupun sebagian atau seluruh isi, konten, materi, data, deskripsi, gambar maupun 
                                            informasi yang ditampilkan pada Situs maupun yang ditempatkan atau terdapat dalam Situs kami untuk tujuan apapun termasuk untuk tujuan komersial 
                                            tanpa memperoleh lisensi langsung dan resmi serta secara tertulis terlebih dahulu dari kami atau pemegang lisensi. Dalam hal kami mengetahui adanya 
                                            pelanggaran hak atas kekayaan intelektual, kami dapat mengambil tindakan hukum sesuai dengan ketentuan hukum dan peraturan perundang-undangan yang berlaku.
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="vertical-align: top;">8.4</td>
                                        <td>
                                            Jika Anda mencetak, menyalin atau mengunduh bagian manapun dari Situs kami namun melanggar satu atau lebih Syarat dan Ketentuan ini, hak Anda untuk 
                                            menggunakan Situs akan segera dihentikan tanpa pemberitahuan terlebih dahulu dan tanpa diperlukan ijin dari Anda dan Anda wajib untuk, sesuai keputusan 
                                            atau kebijakan kami, mengembalikan atau memusnahkan setiap salinan isi, konten, materi, data, deskripsi, gambar maupun informasi yang ditampilkan pada 
                                            Situs maupun yang ditempatkan atau terdapat dalam Situs kami yang telah Anda buat atau Anda akses atau Anda miliki. Anda dilarang dengan cara apapun 
                                            memodifikasi, menerjemahkan, menguraikan, membongkar atau membuat karya turunan berdasarkan perangkat lunak atau dokumentasi yang menyertainya disediakan 
                                            oleh kami maupun oleh pemegang lisensi yang bersangkutan.
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection