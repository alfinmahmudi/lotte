@extends('layout.master')

@section('title', 'Lotte Grosir | Selamat Datang di Lotte Grosir ' . session('storedata.name'))

@section('upsource', view('layout.upsource', ['source' => $source['upsource']]))

@section('nav', view('layout.nav'))

@section('mobilemenu', view('layout.mobilemenu', ['heads' => ['search', 'card', 'store', 'cart', 'address', 'logout']]))

@section('headbar', view('layout.headbar', ['heads' => ['card', 'store', 'cart', 'address', 'logout']]))

@section('footer', view('layout.footer'))

@section('downsource', view('layout.downsource', ['plugins' => $source['down_plugins'], 'scripts' => $source['down_scripts']]))

@section('header', view('layout.header', ['name' => session('storedata.name')]))

@section('content')
<meta name="csrf-token" content="{{ csrf_token() }}" />
<section class="page_error section-padding--lg bg--white">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="error__inner text-center">
                    <div class="error__content">
                        <h2><?= $msg; ?></h2>
                        <a href="javascript:void(0)" onclick="goBack()"><p>Back To Previous Page</p></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection