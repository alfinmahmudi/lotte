@extends('layout.master')

@section('title', 'Lotte Grosir | Kenali Kami')

@section('upsource', view('layout.upsource', ['source' => $source['upsource']]))

@section('mobilemenu', view('layout.mobilemenu', ['heads' => ['search', 'card', 'store', 'cart', 'address', 'logout']]))

@if (session('is_login'))
    @section('headbar', view('layout.headbar', ['heads' => ['address', 'logout']]))
@else
    @section('headbar', view('layout.headbar', ['heads' => ['login']]))
@endif

@section('footer', view('layout.footer'))

@section('downsource', view('layout.downsource', ['plugins' => $source['down_plugins'], 'scripts' => $source['down_scripts']]))

@section('content')

<div class="page-about about_area bg--white section-padding--lg">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="section__title--3 text-center pb--30">
                    <h2>KEBIJAKAN PRIVASI</h2>
                </div>
            </div>
        </div>
        <div class="row align-items-center">
            <div class="col-lg-12 col-sm-12 col-12">
                <div class="content">
                    <table>
                        <tr>
                            <td style="vertical-align: top;"><p><strong>1.</strong></p></td>
                            <td>
                                <p>
                                    <strong>Informasi Pengguna</strong> <br>
                                    Aplikasi Lotte Grosir dan www.lottegrosir.co.id mengumpulkan informasi seperti Kartu Anggota Lotte Grosir, Nama Lengkap, Nomor HP, Nomor Telepon, Alamat,  dan Email  dengan tujuan untuk memproses dan memperlancar proses penggunaan. Aplikasi Lotte Grosir dan situs www.lottegrosir.co.id . Tindakan tersebut telah memperoleh persetujuan pada saat Register dan ketika Login pertama kali dilakukan.
                                </p>
                            </td>
                        </tr>
                        <tr>
                            <td style="vertical-align: top;"><p><strong>2.</strong></p></td>
                            <td>
                                <p>
                                    <strong>Tujuan penggunaan</strong> <br>
                                    Adapun tujuan penggunaan informasi tersebut digunakan untuk manajeman keanggotaan, layanan pelanggan dan juga pengiriman pesanan.  
                                </p>
                            </td>
                        </tr>
                        <tr>
                            <td style="vertical-align: top;"><p><strong>3.</strong></p></td>
                            <td>
                                <p>
                                    <strong>Periode penggunaan</strong> <br>
                                    Periode penggunaan dinyatakan aktif sampai keanggotaan berakhir.
                                </p>
                            </td>
                        </tr>   
                        <tr>
                            <td style="vertical-align: top;"><p><strong>4.</strong></p></td>
                            <td>
                                <p>
                                    <strong>Kontak informasi</strong> <br>
                                    Hubungi Pusat Pelayanan Pelanggan (CIS) atau Direct Line Lotte Grosir terdekat. 
                                </p>
                            </td>
                        </tr>
                    </table>
                </div>
                {{-- <div class="content">
                    <h3>Buy Book</h3>
                    <h2>Different Knowledge</h2>
                    <p class="mt--20 mb--20">Claritas est etiam processus dynamicus, qui sequitur mutationem consuetudium lectorum. Mirum est notare quam littera gothica, quam nunc putamus parum claram, anteposuerit litterarum formas humanitatis per seacula quarta decima et quinta decima. Eodem modo typi, qui nunc nobis videntur parum clari, fiant sollemnes in futurum.</p>
                    <strong>London Address</strong>
                    <p>34 Parer Place via Musk Avenue Kelvin Grove, QLD, 4059</p>
                </div> --}}
            </div>
        </div>
    </div>
</div>
@endsection