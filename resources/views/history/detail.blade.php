@extends('layout.master')

@section('title', 'Lotte Grosir | Selamat Datang di Lotte Grosir ' . session('storedata.name'))

@section('upsource', view('layout.upsource', ['source' => $source['upsource']]))

@section('nav', view('layout.nav'))

@section('mobilemenu', view('layout.mobilemenu', ['heads' => ['search', 'card', 'store', 'cart', 'address', 'logout']]))

@section('headbar', view('layout.headbar', ['heads' => ['card', 'store', 'cart', 'address', 'logout']]))

@section('footer', view('layout.footer'))

@section('downsource', view('layout.downsource', ['plugins' => $source['down_plugins'], 'scripts' => $source['down_scripts']]))

@section('header', view('layout.header', ['name' => session('storedata.name')]))

@section('modal', view('modal_store', ['htmlStore' => $htmlStore]))

@section('content')
<meta name="csrf-token" content="{{ csrf_token() }}" />
<section class="wn__bestseller__area bg--white pb--30">
    <div class="cart-main-area section-padding--lg bg--white">
		<div class="container">
			<div class="row">
                <div class="col-md-12 mx-0">
                    <form method="POST" name="form_cart_all" id="msform" enctype="multipart/form-data">
                        <?= $datahis['status']; ?>

                        <div class="row">
                            <div class="col-lg-8 col-8">
                                <div class="col-lg-6 col-6">
                                    <table>
                                        <tr>
                                            <td>
                                                <b> Transaksi ID </b>
                                            </td>
                                            <td>
                                                :
                                            </td>
                                            <td>
                                                <b><?= $datahis['transaction_id']; ?></b>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Metode Pengiriman
                                            </td>
                                            <td>
                                                :
                                            </td>
                                            <td>
                                                <?= $datahis['shipping']; ?>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Metode Pembayaran
                                            </td>
                                            <td>
                                                :
                                            </td>
                                            <td>
                                                <?= $datahis['payment']; ?>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Nama Toko
                                            </td>
                                            <td>
                                                :
                                            </td>
                                            <td>
                                                <?= $datahis['store_name']; ?>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Nomor Resi
                                            </td>
                                            <td>
                                                :
                                            </td>
                                            <td>
                                                <?= $datahis['awb']; ?>
                                            </td>
                                        </tr>
                                        <?php if ($datahis['payment_va_no'] != '') { ?>
                                            <tr>
                                                <td>
                                                    VA Number
                                                </td>
                                                <td>
                                                    :
                                                </td>
                                                <td>
                                                    <?= $datahis['payment_va_no']; ?>
                                                </td>
                                            </tr>
                                        <?php } ?>
                                    </table>
                                </div>
                                <?= $datahis['htmlhis']; ?>
                            </div>
                            <div class="col-sm-4 col-md-4 col-lg-4">
                                <div class="cartbox-total d-flex justify-content-between">
                                    <ul class="cart__total__list" style="text-align: left;">
                                        <li><strong>Ringkasan Belanja</strong></li>
                                        <li>Sub Total</li>
                                        <li>Biaya Layanan</li>
                                        <li id="li_txt_deliv">Total Ongkos Kirim</li>
                                        <li>Biaya Admin Bank</li>
                                        <li>Total</li>
                                    </ul>
                                    <ul class="cart__total__list">
                                        <li>&nbsp;</li>
                                        <li><strong>Rp. </strong></li>
                                        <li><strong>Rp. </strong></li>
                                        <li id="li_txt_deliv"><strong>Rp. </strong></li>
                                        <li><strong>Rp. </strong></li>
                                        <li><strong>Rp. </strong></li>
                                    </ul>
                                    <ul class="cart__total__tk" style="text-align: right;">
                                        <li>&nbsp;</li>
                                        <li><?= number_format($datahis['sub_total_price']); ?></li>
                                        <li><?= number_format($datahis['service_fee']); ?></li>
                                        <li id="total_amount_delivery"><?= number_format($datahis['shipping_price']); ?></li>
                                        <li><?= number_format($datahis['bank_service_fee']); ?></li>
                                        <li id="total_amount_cart_txt"><?= number_format($datahis['total_price']); ?></li>
                                    </ul>
                                </div>
                                <?= $datahis['btnpayment']; ?>
                            </div>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
<div id="modal-payment" class="modal fade" role="dialog" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-lg">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-body" id="modal-body-payment" style="height: 800px">
                
            </div>
        </div>
    </div>
</div>
@endsection