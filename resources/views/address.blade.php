@extends('layout.master')

@section('title', 'Lotte Grosir | Pengaturan Alamat')

@section('upsource', view('layout.upsource', ['source' => $source['upsource']]))

@section('mobilemenu', view('layout.mobilemenu', ['heads' => ['search', 'card', 'store', 'cart', 'address', 'logout']]))

@section('headbar', view('layout.headbar', ['heads' => ['store', 'logout']]))

@section('footer', view('layout.footer'))

@section('downsource', view('layout.downsource', ['plugins' => $source['down_plugins'], 'scripts' => $source['down_scripts']]))

@section('modal', view('modal_store', ['htmlStore' => $htmlStore]))

@section('content')

<!-- Start Blog Area -->
<div class="page-blog bg--white section-padding--lg blog-sidebar right-sidebar">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-12">
                <div class="blog-page">
                    <div class="page__header">
                        <h2>Pengaturan Alamat  <a style="color:#e80916;" href="javascript:void(0)" data-id="0" onclick="show_addrs($(this), 0)" data-url="<?= url('/login/get_address_detail'); ?>"><small>Tambah Baru</small></a></h2>
                    </div>
                    <!-- Start Single Post -->
                    <?= $htmladdrs; ?>
                    <!-- End Single Post -->
                </div>
            </div>
        </div>
    </div>
</div>

<!-- End Blog Area -->
<div id="modal-addrs" class="modal fade" role="dialog" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog modal-lg">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-body">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <section class="bg--white">
            <div class="maincontent bg--white">
				<div class="container" id="cont_addrs">
                    
				</div>
			</div>
		</section>
	  </div>
	</div>
	<!-- Modal content-->
	
  </div>
</div>

@endsection