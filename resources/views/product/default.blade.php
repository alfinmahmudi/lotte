@extends('layout.master')

@section('title', 'Lotte Grosir | Selamat Datang di Lotte Grosir ' . session('storedata.name'))

@section('upsource', view('layout.upsource', ['source' => $source['upsource']]))

@section('nav', view('layout.nav'))

@section('mobilemenu', view('layout.mobilemenu', ['heads' => ['card', 'store', 'cart', 'address', 'logout'], 'search' => '']))

@section('headbar', view('layout.headbar', ['heads' => ['search', 'card', 'store', 'cart', 'address', 'logout'], 'search' => $search]))

@section('footer', view('layout.footer'))

@section('downsource', view('layout.downsource', ['plugins' => $source['down_plugins'], 'scripts' => $source['down_scripts']]))

@section('header', view('layout.header', ['name' => session('storedata.name')]))

@section('modal', view('modal_store', ['htmlStore' => $htmlStore]))

@section('modal_cart', view('parts.modal_cart'))

{{-- @if (count($arrpromo) > 0)
    @section('promo', view('layout.promo', ['arrpromo' => $arrpromo]))
@endif --}}

@section('content')
	<input type="hidden" name="store_id" id="store_id" value="<?= session('storedata.id') ?>">
	<input type="hidden" name="page" id="page" value="1">
    <div class="page-shop-sidebar left--sidebar bg--white section-padding--lg">
        	<div class="container">
        		<div class="row">
        			<div class="col-lg-3 col-12 order-2 order-lg-1 md-mt-40 sm-mt-40">
        				<div class="shop__sidebar">
        					<aside class="wedget__categories poroduct--cat">
        						<h3 class="wedget__title">Product Categories</h3>
        						<ul id="cat_prod">
        							
								</ul>
        					</aside>
        				</div>
        			</div>
        			<div class="col-lg-9 col-12 order-1 order-lg-2">
						<!-- <div class="row">
        					<div class="col-lg-12 p-3">
								<input class="form-control form-control-sm ml-3 w-75" type="text" autocomplete="off" id="comp_search" placeholder="Search..." aria-label="Search">
								<input type="hidden" name="search" id="search">
        					</div>
        				</div> -->
        				<div class="tab__container">
	        				<div class="shop-grid tab-pane fade show active" id="nav-grid" role="tabpanel">
	        					<div class="row" id="main_prod">
	        						<?= $htmlProd; ?>
								</div>
								<div id="main_paging">
									<?= $htmlpaging; ?>
								</div>
	        				</div>
        				</div>
        			</div>
        		</div>
        	</div>
	</div>
@endsection