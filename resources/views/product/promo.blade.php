@extends('layout.master')

@section('title', 'Lotte Grosir | Selamat Datang di Lotte Grosir ' . session('storedata.name'))

@section('upsource', view('layout.upsource', ['source' => $source['upsource']]))

@section('nav', view('layout.nav'))

@section('mobilemenu', view('layout.mobilemenu', ['heads' => ['card', 'store', 'cart', 'address', 'logout'], 'search' => '']))

@section('headbar', view('layout.headbar', ['heads' => ['search', 'card', 'store', 'cart', 'address', 'logout'], 'search' => $search]))

@section('footer', view('layout.footer'))

@section('downsource', view('layout.downsource', ['plugins' => $source['down_plugins'], 'scripts' => $source['down_scripts']]))

@section('header', view('layout.header', ['name' => session('storedata.name')]))

@section('modal', view('modal_store', ['htmlStore' => $htmlStore]))

@section('modal_cart', view('parts.modal_cart'))

{{-- @if (count($arrpromo) > 0)
    @section('promo', view('layout.promo', ['arrpromo' => $arrpromo]))
@endif --}}

@section('content')

<section class="wn__team__area pt--40 pb--75 bg--white">
    <input type="hidden" id="store_id" value="<?= session('storedata.id') ?>">
    <div class="container">
        <div class="row">
            <?= $htmlPromo; ?>
        </div>
    </div>
</section>

@endsection