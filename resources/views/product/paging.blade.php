@if ($paginator->hasPages())
    <ul class="wn__pagination">
        {{-- Previous Page Link --}}
        @if (!$paginator->onFirstPage())
            <li><a href="#"><i class="zmdi zmdi-chevron-left"></i></a></li>    
        @endif

        {{-- Pagination Elements --}}
        @foreach ($elements as $element)
            {{-- "Three Dots" Separator --}}
            @if (is_string($element))
                <li class="disabled"><span>{{ $element }}</span></li>
            @endif

            {{-- Array Of Links --}}
            @if (is_array($element))
                @foreach ($element as $page => $url)
                    @if ($page == $paginator->currentPage())
                        <li class="active"><a href="javascript:void(0)" data_url="{{ $url }}" onclick="nextpage($(this))">{{ $page }}</a></li>
                    @else
                        <li><a href="javascript:void(0)" data_url="{{ $url }}" onclick="nextpage($(this))">{{ $page }}</a></li>
                    @endif
                @endforeach
            @endif
        @endforeach

        {{-- Next Page Link --}}
        @if ($paginator->hasMorePages())
            <li><a href="javascript:void(0)" data_url="{{ $url }}" onclick="nextpage($(this))"><i class="zmdi zmdi-chevron-right"></i></a></li>
        @endif
    </ul>
@endif