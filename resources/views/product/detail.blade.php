@extends('layout.master')

@section('title', 'Lotte Grosir | Selamat Datang di Lotte Grosir ' . session('storedata.name'))

@section('upsource', view('layout.upsource', ['source' => $source['upsource']]))

@section('nav', view('layout.nav'))

@section('mobilemenu', view('layout.mobilemenu', ['heads' => ['search', 'card', 'store', 'cart', 'address', 'logout']]))

@section('headbar', view('layout.headbar', ['heads' => ['card', 'store', 'cart', 'address', 'logout']]))

@section('footer', view('layout.footer'))

@section('downsource', view('layout.downsource', ['plugins' => $source['down_plugins'], 'scripts' => $source['down_scripts']]))

@section('header', view('layout.header', ['name' => session('storedata.name')]))

@section('modal', view('modal_store', ['htmlStore' => $htmlStore]))

@section('content')
<meta name="csrf-token" content="{{ csrf_token() }}" />
<div class="maincontent bg--white pt--80 pb--55">
	<div class="container">
		<div class="row">
			<div class="col-lg-12 col-12">
				<form id="form_quantity" name="form_quantity" action="{{ url('/cart/add_to_cart') }}" method="POST" enctype="multipart/form-data">
					<input type="hidden" id="quantity_prod" name="DATA[product_id]" value="<?= $data_prod['product_id'] ?>">
					<input type="hidden" id="quantity_store" name="DATA[store_id]" value="<?= $data_prod['store_id'] ?>">
					<input type="hidden" id="store_id" value="<?= $data_prod['store_id'] ?>">
					<input type="hidden" id="price" name="price">
					<input type="hidden" id="hidden_grand_total" name="grand_total">
					<input type="hidden" id="img" name="img">
					<input type="hidden" id="name" name="name">
					<div class="wn__single__product">
						<div class="row">
							<div class="col-lg-6 col-12">
								<div class="wn__fotorama__wrapper">
									<div class="fotorama wn__fotorama__action" data-nav="thumbs">
										<?= $img_prod; ?>
									</div>
								</div>
							</div>
							<div class="col-lg-6 col-12">
								<div class="product__info__main">
									<h1><?= $data_prod['product_name'] ?></h1>
									<div class="price-box">
										<span>Rp. <?= number_format($data_prod['price']); ?></span>
									</div>
									<div class="product__overview">
										<b>Deskripsi : </b>
										<p> <?= $data_prod['product_description'] ?> </p>
									</div>
									<div class="price-box">Qty</div>
									<div class="box-tocart d-flex">
										<a href="javascript:void(0)" onclick="minus_qty()"><span><i class="fa fa-minus fa-lg"></i></span></a>
										<input id="qty" class="input-text qty" style="margin: 0 10px 15px 0;" name="DATA[total]" min="<?= $data_prod['min_order'] ?>" value="<?= $data_prod['min_order'] ?>" title="Qty" type="number">
										<a href="javascript:void(0)" onclick="plus_qty()"><span><i class="fa fa-plus fa-lg"></i></span></a>
										<div class="addtocart__actions">
											<button class="tocart" type="button" onclick="add_to_cart(form_quantity)" title="Add to Cart">Add to Cart</button>
										</div>
									</div>
								</div>
								
								<div class="product__info__detailed" <?= $display; ?>>
									<div class="pro_details_nav nav justify-content-start" role="tablist">
										<a class="nav-item nav-link active" data-toggle="tab" href="#nav-details" role="tab">Info BBLM</a>
									</div>
									<div class="tab__container">
										<!-- Start Single Tab Content -->
										<div class="pro__tab_label tab-pane fade show active" id="nav-details" role="tabpanel">
											<div class="description__attribute">
												<?= $htmlbblm; ?>
											</div>
										</div>
										<!-- End Single Tab Content -->
									</div>
								</div>

							</div>


						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
@endsection