<!-- Footer Area -->
<footer id="wn__footer" class="footer__area bg__cat--8 brown--color">
    <div class="footer-static-top">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="footer__widget footer__menu">
                        <div class="ft__logo">
                            <a href="index.html">
                                <img src="{{ url('img/logolotte.png') }}" alt="logo">
                            </a>
                            <p>Bergabunglah dengan kami : </p>
                        </div>
                        <div class="footer__content">
                            <ul class="social__net social__net--2 d-flex justify-content-center">
                                <li><a href="https://www.instagram.com/lottegrosir"><i class="fa fa-instagram"></i></a></li>
                                <li><a href="https://www.facebook.com/lottegrosir/ "><i class="fa fa-facebook"></i></a></li>
                                <li><a href="https://www.youtube.com/channel/UCcIT0MxxLA4aywJvRMk9j1Q/featured "><i class="fa fa-youtube-play"></i></a></li>
                                <li><a href="https://bit.ly/lsiandro"><i class="fa fa-android"></i></a></li>
                            </ul>
                            <ul class="mainmenu d-flex justify-content-center">
                                {{-- <li><a href="{{ url('/about') }}">Kenali Kami</a></li> --}}
                                <li><a href="{{ url('/skb') }}">Syarat & Ketentuan</a></li>
                                <li><a href="{{ url('/privasi') }}">Kebijakan Privasi</a></li>
                                <li><a href="{{ url('/faq') }}">FAQ</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="copyright__wrapper">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-12">
                    <div class="copyright">
                        <div class="copy__right__inner text-left">
                            <p>Copyright <i class="fa fa-copyright"></i> PT. LOTTE Shopping Indonesia.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
<!-- //Footer Area -->