<nav class="mainmenu__nav">
    <ul class="meninmenu d-flex justify-content-start">
        <li class="drop with--one--item"><a href="<?= url('product/bblm/' . session('storedata.id')); ?>">BBLM</a></li>
        <li class="drop with--one--item"><a href="<?= url('product/promo/' . session('storedata.id')); ?>">Promo</a></li>
        <li class="drop"><a href="#">Seluruh Kategori</a>
            <div class="megamenu dropdown">
                <ul class="item item01" id="category">
                </ul>
            </div>
        </li>
        <li class="drop"><a href="#">Daftar Belanja</a>
            <div class="megamenu dropdown">
                <ul class="item item01">
                    <li><a href="<?= url('history/waiting/' . session('storedata.id')); ?>">Menunggu Pembayaran</a></li>
                    <li><a href="<?= url('history/process/' . session('storedata.id')); ?>">Pesanan Diproses</a></li>
                    <li><a href="<?= url('history/delivered/' . session('storedata.id')); ?>">Pesanan Selesai</a></li>
                    <li><a href="<?= url('history/failed/' . session('storedata.id')); ?>">Pesanan Dibatalkan</a></li>
                </ul>
            </div>
        </li>
    </ul>
</nav>