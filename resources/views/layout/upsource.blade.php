<!-- Google font (font-family: 'Roboto', sans-serif; Poppins ; Satisfy) -->
<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800" rel="stylesheet"> 
<link href="https://fonts.googleapis.com/css?family=Poppins:300,300i,400,400i,500,600,600i,700,700i,800" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700,900" rel="stylesheet"> 

<!-- Stylesheets -->
<link rel="stylesheet" href="{{ url('templates/css/bootstrap.min.css') }}">
<link rel="stylesheet" href="{{ url('templates/css/plugins.css') }}">
<link rel="stylesheet" href="{{ url('templates/style.css') }}">

<!-- Cusom css -->
<link rel="stylesheet" href="{{ url('templates/css/custom.css') }}">

<!-- Modernizer js -->
<script src="{{ url('templates/js/vendor/modernizr-3.5.0.min.js') }}"></script>

{{-- fontawesome --}}
<link rel="stylesheet" href="{{ url('templates/plugins/fontawesome/css/font-awesome.min.css') }}">

<script>
var site_url = '{{ url('') }}';
</script>

{{-- ui_extended_modals.html --}}
@foreach ($source as $src)

<link rel="stylesheet" href="{{ $src }}">

@endforeach