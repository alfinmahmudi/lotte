<ul class="header__sidebar__right d-flex justify-content-end align-items-center">
    @foreach ($heads as $hd)
        @if ($hd == 'card')
            <li class="headbar-right nav_fa">
                @if (session('datauser.card_grade_id') == 1)
                    <a href="<?= url('profile'); ?>"><i class="fa fa-address-card fa-lg" style="color: gold"></i><br><small><?= str_replace(" ", "", session('datauser.card_grade')); ?></small></a>
                @endif
                @if (session('datauser.card_grade_id') == 2 || session('datauser.card_grade_id') == 3)
                    <a href="<?= url('profile'); ?>"><i class="fa fa-address-card fa-lg" style="color: blue"></i><br><small><?= str_replace(" ", "", session('datauser.card_grade')); ?></small></a>
                @endif
                @if (session('datauser.card_grade_id') == 4)
                    <a href="<?= url('profile'); ?>"><i class="fa fa-address-card fa-lg" style="color: silver"></i><br><small><?= str_replace(" ", "", session('datauser.card_grade')); ?></small></a>
                @endif
            </li>
        @endif
        @if ($hd == 'store')
            <li class="headbar-right nav_fa">
                <a href="javascript:void(0)" data-toggle="modal" data-target="#modal-store"><i class="fa fa-building-o fa-lg"></i><br>Store</a>
            </li>
        @endif
        @if ($hd == 'login')
            <li class="headbar-right nav_fa">
                <a href="<?= url('/'); ?>"><i class="fa fa-sign-in fa-lg"></i><br>Login</a>
            </li>
        @endif
        @if ($hd == 'catalog')
            <li class="headbar-right nav_fa">
                <a href=""><i class="fa fa-newspaper-o fa-lg"></i><br>Catalog</a>
            </li>
        @endif
        @if ($hd == 'cart')
            <li class="headbar-right nav_fa">
                @if (session('sum_cart') > 0)
                    <a href="javascript:void(0)" onclick="show_cart('web')"><i class="fa fa-shopping-cart fa-lg cartmuch"><span class="smallword"><?= session('sum_cart') ?></span></i><br>Cart</a>
                    <input id="sum_cart" type="hidden" value="<?= session('sum_cart') ?>">
                @else
                    <a href="javascript:void(0)" onclick="show_cart('web')"><i class="fa fa-shopping-cart fa-lg cartmuch"></i><br>Cart</a>
                    <input id="sum_cart" type="hidden" value="0">
                @endif
                <!-- Start Shopping Cart -->
                <div class="block-minicart" id="cart_box" style="display: none;">
                    <div class="minicart-content-wrapper">
                        <div class="micart__close" onclick="show_cart('web')">
                            <span>close</span>
                        </div>
                        <div class="items-total d-flex justify-content-between">
                            <span class="cartmuch2"></span>
                            <span>Cart Subtotal</span>
                        </div>
                        <div class="total_amount text-right">
                            <span class="carttotal"></span>
                        </div>
                        <div class="mini_action checkout">
                            <a class="checkout__btn">Go to Checkout</a>
                        </div>
                        <div class="single__items" id="minidiv" style="display: none; overflow: scroll; height: 300px;">
                            <div class="miniproduct product_head">
                                
                            </div>
                        </div>
                        <div class="mini_action cart">
                            <a class="cart__btn">View and edit cart</a>
                        </div>
                    </div>
                </div>
                <!-- End Shopping Cart -->
            </li>
        @endif
        @if ($hd == 'address')
            <li class="headbar-right nav_fa">
                @if (session('sum_addrs') > 0)
                    <a href="javascript:void(0)" onclick="show_address()"><i class="fa fa-address-book fa-lg"><span class="smallword"><?= session('sum_addrs') ?></span></i><br>Alamat</a>
                @else
                    <a href="javascript:void(0)" onclick="show_address()"><i class="fa fa-address-book fa-lg"></i><br>Alamat</a>
                @endif
                <!-- Start Shopping Cart -->
                <div class="block-minicart" id="address_box">
                    <div class="minicart-content-wrapper">
                        <div class="micart__close" onclick="show_address()">
                            <span>close</span>
                        </div>
                        <div class="single__items">
                            <div class="miniproduct address_head" style="overflow: scroll; height: 300px;">
                                
                            </div>
                        </div>
                        <div class="mini_action cart">
                            <a class="cart__btn" href="<?= url('login/get_full_address'); ?>">Pengaturan Alamat</a>
                        </div>
                    </div>
                </div>
                <!-- End Shopping Cart -->
            </li>
        @endif
        @if ($hd == 'logout')
            <li class="headbar-right nav_fa">
                <a href="<?= url('/logout'); ?>"><i class="fa fa-sign-out fa-lg"></i><br>Logout</a>
            </li>
        @endif
        @if ($hd == 'search')
            <li class="headbar-right nav_fa">
                <!-- <a href="<?= url('/product/' . session('storedata.id')); ?>"><i class="fa fa-search fa-lg"></i><br>Search</a> -->
                <a href="javascript:void(0)" onclick="show_search()"><i class="fa fa-search fa-lg"></i><br>Search</a>
                <!-- Start Shopping Cart -->
                <div class="block-minicart" id="search_box">
                    <div class="minicart-content-wrapper">
                        <div class="micart__close" onclick="show_search()">
                            <span>close</span>
                        </div>
                        <div class="wn__sidebar">
                            <!-- <aside class="widget search_widget">
                                <form action="javascript:void(0)" onsubmit="search_submit()">
                                    <div class="form-input">
                                        <input class="form-control form-control-sm ml-3 w-75" type="text" autocomplete="off" id="comp_search" placeholder="Search..." aria-label="Search">
                                        <input type="hidden" name="search" id="search">
                                        <button onclick="search_submit()"><i class="fa fa-search"></i></button>
                                    </div>
                                </form>
                            </aside> -->
                            <input class="form-control form-control-sm ml-3 w-75" type="text" autocomplete="off" id="comp_search" placeholder="Search..." onkeydown="search_enter(this)" aria-label="Search">
                            <input type="hidden" name="search" id="search">
                        </div>

                    </div>
                </div>
                <!-- End Shopping Cart -->
            </li>
        @endif
    @endforeach
</ul>
@csrf

