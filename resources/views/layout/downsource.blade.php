<!-- JS Files -->
<script src="{{ url('templates/js/vendor/jquery-3.2.1.min.js') }}"></script>
<script src="{{ url('templates/js/popper.min.js') }}"></script>
<script src="{{ url('templates/js/bootstrap.min.js') }}"></script>
<script src="{{ url('templates/js/plugins.js') }}"></script>
<script src="{{ url('templates/js/active.js') }}"></script>
<script src="{{ url('js/numeral.min.js') }}"></script>


@foreach ($plugins as $src)

<script src="{{ $src }}"></script>

@endforeach

@foreach ($scripts as $src)

<script src="{{ $src.'?'.date('ymdhis') }}"></script>

@endforeach
<script src="{{ url('js/geocoder.min.js') }}"></script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDPmDqJoysQK_h3pjevhJLs6Mt8WYYhoNI"></script>
<script src="{{ url('js/allpage.js?'.date('ymdhis')) }}"></script>