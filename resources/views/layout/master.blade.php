<!doctype html>
<html class="no-js" lang="zxx">
<head>
	<meta charset="utf-8">
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<title>@yield('title')</title>
	<meta name="description" content="">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<!-- Favicons -->
	<link rel="shortcut icon" href="{{ url('img/titlelogo.png') }}">
	<link rel="apple-touch-icon" href="{{ url('img/titlelogo.png') }}">

	@yield('upsource')
</head>
<body>
	<style>
		.swal-wide{
			width:850px !important;
		}
	</style>
	<!--[if lte IE 9]>
		<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="https://browsehappy.com/">upgrade your browser</a> to improve your experience and security.</p>
	<![endif]-->

	<!-- Main wrapper -->
	<div class="wrapper" id="wrapper">
		<!-- Header -->
		<header id="wn__header" class="header__area header__absolute sticky__header">
			<div class="container-fluid">
				<div class="row">
					<div class="col-md-6 col-sm-6 col-6 col-lg-2">
						<div class="logo">
							<?php if (session('storedata')) { ?>
								<a href="<?= url('home/' . session('storedata.id') ); ?>">
							<?php }else if (session('is_login') == 1){ ?>
								<a href="<?= url('store/home'); ?>">
							<?php }else{ ?>
								<a href="<?= url(''); ?>">
							<?php } ?>
								<img src="{{ url('img/logolotte.png') }}" alt="logo images">
							</a>
						</div>
					</div>
					<div class="col-lg-8 d-none d-lg-block">
						@yield('nav')
					</div>
					<div class="col-md-6 d-none d-lg-block col-lg-2">
						@yield('headbar')
					</div>
				</div>
				@yield('mobilemenu')
			</div>		
		</header>
		<!-- //Header -->
		<!-- Start Search Popup -->
		<div class="brown--color box-search-content search_active block-bg close__top">
			<form id="search_mini_form" class="minisearch" action="#">
				<div class="field__search">
					<input type="text" placeholder="Search entire store here...">
					<div class="action">
						<a href="#"><i class="zmdi zmdi-search"></i></a>
					</div>
				</div>
			</form>
			<div class="close__wrap">
				<span>close</span>
			</div>
		</div>
		<!-- End Search Popup -->
		<input type="hidden" name="sum_cart" id="sum_cart" value="0">
		@yield('header')
		@yield('promo')
        @yield('content')

		@yield('modal')

		@yield('modal_cart')
		@yield('footer')
	</div>
	<!-- //Main wrapper -->

	@yield('downsource')
	
</body>
</html>