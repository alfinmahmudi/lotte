<div class="slider-area brown__nav slider--15 slide__activation slide__arrow01 owl-carousel owl-theme">
    @foreach ($arrpromo as $promo)
        <div class="slide animation__style10 bg-image-cust fullscreen align__center--left" style="background-color: transparent;">
            <?php if (array_key_exists('image_url', $promo)) {
                if ($promo['image_origin']) { ?>
                <img src="<?= $promo['image_origin']; ?>">
            <?php }else { ?>
                <img src="<?= $promo['image']; ?>">
            <?php }  } else {  ?>
                <img src="<?= $promo['image']; ?>">
            <?php } ?>
        </div>
    @endforeach
</div>