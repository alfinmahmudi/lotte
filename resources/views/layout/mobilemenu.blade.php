<!-- Start Mobile Menu -->
<div class="row d-none">
    <div class="col-lg-12 d-none">
        <nav class="mobilemenu__nav">
            <ul class="meninmenu">
                <li><a href="index.html">Hot Promo</a></li>
                <li><a href="index.html">Promo</a></li>
                <li><a href="index.html">Lotte Brand</a></li>
                <li><a href="#">Seluruh Kategori</a>
                    <ul id="category_mobile">
                        
                    </ul>
                </li>
                @foreach ($heads as $hd)
                    @if ($hd == 'card')
                        <li class="headbar-right nav_fa">
                            @if (session('datauser.card_grade_id') == 1)
                                <a href="javascript:void(0)"><i class="fa fa-address-card fa-lg" style="color: gold"></i><br><small><?= str_replace(" ", "", session('datauser.card_grade')); ?></small></a>
                            @endif
                            @if (session('datauser.card_grade_id') == 2 || session('datauser.card_grade_id') == 3)
                                <a href="javascript:void(0)"><i class="fa fa-address-card fa-lg" style="color: blue"></i><br><small><?= str_replace(" ", "", session('datauser.card_grade')); ?></small></a>
                            @endif
                            @if (session('datauser.card_grade_id') == 4)
                                <a href="javascript:void(0)"><i class="fa fa-address-card fa-lg" style="color: silver"></i><br><small><?= str_replace(" ", "", session('datauser.card_grade')); ?></small></a>
                            @endif
                        </li>
                    @endif
                    @if ($hd == 'store')
                        <li class="headbar-right nav_fa">
                            <a href="javascript:void(0)" data-toggle="modal" data-target="#modal-store"><i class="fa fa-building-o fa-lg"></i><br>Store</a>
                        </li>
                    @endif
                    @if ($hd == 'login')
                        <li class="headbar-right nav_fa">
                            <a href="<?= url('/'); ?>"><i class="fa fa-sign-in fa-lg"></i><br>Login</a>
                        </li>
                    @endif
                    @if ($hd == 'catalog')
                        <li class="headbar-right nav_fa">
                            <a href=""><i class="fa fa-newspaper-o fa-lg"></i><br>Catalog</a>
                        </li>
                    @endif
                    @if ($hd == 'cart')
                        <li class="headbar-right nav_fa">
                            @if (session('sum_cart') > 0)
                                <a href="javascript:void(0)" onclick="show_cart_mobile()"><i class="fa fa-shopping-cart fa-lg cartmuch"><span class="smallword"><?= session('sum_cart') ?></span></i><br>Cart</a>
                                <input id="sum_cart" type="hidden" value="<?= session('sum_cart') ?>">
                            @else
                                <a href="javascript:void(0)" onclick="show_cart_mobile()"><i class="fa fa-shopping-cart fa-lg cartmuch"></i><br>Cart</a>
                                <input id="sum_cart" type="hidden" value="0">
                            @endif
                        </li>
                    @endif
                    @if ($hd == 'address')
                        <li class="headbar-right nav_fa">
                            @if (session('sum_addrs') > 0)
                                <a href="javascript:void(0)" onclick="show_address_mobile()"><i class="fa fa-address-book fa-lg"><span class="smallword"><?= session('sum_addrs') ?></span></i><br>Alamat</a>
                            @else
                                <a href="javascript:void(0)" onclick="show_address_mobile()"><i class="fa fa-address-book fa-lg"></i><br>Alamat</a>
                            @endif
                        </li>
                    @endif
                    @if ($hd == 'logout')
                        <li class="headbar-right nav_fa">
                            <a href="<?= url('/logout'); ?>"><i class="fa fa-sign-out fa-lg"></i><br>Logout</a>
                        </li>
                    @endif
                    @if ($hd == 'search')
                        <li class="headbar-right nav_fa">
                            <a href="javascript:void(0)" onclick="show_search()"><i class="fa fa-search fa-lg"></i><br>Search</a>
                            <!-- Start Shopping Cart -->
                            <div class="block-minicart" id="search_box">
                                <div class="minicart-content-wrapper">
                                    <div class="micart__close" onclick="show_search()">
                                        <span>close</span>
                                    </div>
                                    <div class="wn__sidebar">
                                        <aside class="widget search_widget">
                                            <form action="javascript:void(0)" onsubmit="search_submit()">
                                                <div class="form-input">
                                                    <input type="text" name="search" id="search" placeholder="Search...">
                                                    <button onclick="search_submit()"><i class="fa fa-search"></i></button>
                                                </div>
                                            </form>
                                        </aside>
                                    </div>

                                </div>
                            </div>
                            <!-- End Shopping Cart -->
                        </li>
                    @endif
                @endforeach
            </ul>
        </nav>
    </div>
</div>
<!-- End Mobile Menu -->
<div class="mobile-menu d-block d-lg-none">
</div>
<!-- Mobile Menu -->	

<!-- Start Shopping Cart -->
<div class="block-minicart" id="cart_box_mobile" style="display: none;">
    <div class="minicart-content-wrapper">
        <div class="micart__close" onclick="show_cart_mobile()">
            <span>close</span>
        </div>
        <div class="items-total d-flex justify-content-between">
            <span class="cartmuch2_mobile"></span>
            <span>Cart Subtotal</span>
        </div>
        <div class="total_amount text-right">
            <span class="carttotal_mobile"></span>
        </div>
        <div class="mini_action checkout">
            <a class="checkout__btn">Go to Checkout</a>
        </div>
        <div class="single__items" id="minidiv_mobile" style="display: none; overflow: scroll; height: 100px;">
            <div class="miniproduct product_head_mobile">
                
            </div>
        </div>
        <div class="mini_action cart">
            <a class="cart__btn">View and edit cart</a>
        </div>
    </div>
</div>
<!-- End Shopping Cart -->

<!-- Start Shopping Cart -->
<div class="block-minicart" id="address_box_mobile">
    <div class="minicart-content-wrapper">
        <div class="micart__close" onclick="show_address_mobile()">
            <span>close</span>
        </div>
        <div class="single__items">
            <div class="miniproduct address_head_mobile" style="overflow: scroll; height: 100px;">
                
            </div>
        </div>
        <div class="mini_action cart">
            <a class="cart__btn" href="<?= url('login/get_full_address'); ?>">Pengaturan Alamat</a>
        </div>
    </div>
</div>
<!-- End Shopping Cart -->