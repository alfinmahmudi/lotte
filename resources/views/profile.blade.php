@extends('layout.master')

@section('title', 'Lotte Grosir | Selamat Datang di Lotte Grosir ' . session('storedata.name'))

@section('upsource', view('layout.upsource', ['source' => $source['upsource']]))

@section('nav', view('layout.nav'))

@section('mobilemenu', view('layout.mobilemenu', ['heads' => ['search', 'card', 'store', 'cart', 'address', 'logout']]))

@section('headbar', view('layout.headbar', ['heads' => ['card', 'store', 'cart', 'address', 'logout']]))

@section('footer', view('layout.footer'))

@section('downsource', view('layout.downsource', ['plugins' => $source['down_plugins'], 'scripts' => $source['down_scripts']]))

@section('header', view('layout.header', ['name' => session('storedata.name')]))

@section('modal', view('modal_store', ['htmlStore' => $htmlStore]))

@section('content')

<div class="container">
    <div class="row">
        <div class="col-lg-12 col-12">
            <div class="contact-form-wrap">
                <form id="contact-form" action="#" method="post">
                    <div class="customer_details">
                    <h2 class="contact__title">Profile <i class="fa fa-address-card fa-lg" style="color: <?= $profile['color']; ?>"></i> </h2>
                        <div class="customar__field">
                            <div class="margin_between">
                                <div class="input_box space_between">
                                    <label>Username</label>
                                    <input type="text" value="<?= $profile['username'] ?>" readonly>
                                </div>
                                <div class="input_box space_between">
                                    <label>Nomor Kartu</label>
                                    <input type="text" value="<?= $profile['card_no'] ?>" readonly>
                                </div>
                            </div>
                            
                            <div class="margin_between">
                                <div class="input_box space_between">
                                    <label>Nama</label>
                                    <input type="text" value="<?= $profile['name'] ?>" readonly>
                                </div>
                                <div class="input_box space_between">
                                    <label>Email</label>
                                    <input type="text" value="<?= $profile['email'] ?>" readonly>
                                </div>
                            </div>

                            <div class="margin_between">
                                <div class="input_box space_between">
                                    <label>Nomor Telepon</label>
                                    <input type="text" value="<?= $profile['phone'] ?>" readonly>
                                </div>
                                <div class="input_box space_between">
                                    <label>Grade Kartu</label>
                                    <input type="text" value="<?= $profile['card_grade'] ?>" readonly>
                                </div>
                            </div>

                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

@endsection