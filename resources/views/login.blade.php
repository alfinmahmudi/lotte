@extends('layout.master')

@section('title', 'Lotte Grosir | Pilih Toko')

@section('upsource', view('layout.upsource', ['source' => $source['upsource']]))

@section('headbar', view('layout.headbar', ['heads' => []]))

@section('footer', view('layout.footer'))

@section('downsource', view('layout.downsource', ['scripts' => $source['down_scripts'], 'plugins' => $source['down_plugins']]))

@section('content')
<link rel="stylesheet" href="https://unpkg.com/leaflet@1.0.3/dist/leaflet.css" />
<script src="https://unpkg.com/leaflet@1.0.3/dist/leaflet.js"></script>
<script src="https://tiles.unwiredmaps.com/js/leaflet-unwired.js"></script>

{{-- <meta name="csrf-token" content="{{ csrf_token() }}" /> --}}

<form action="{{ url('/login/login_member') }}" method="POST" name="form_login_member" id="form_login_member" enctype="multipart/form-data">
    @csrf
    <div class="container pt--80 pb--30" id="div_login">
        <div class="row account__form" style="margin: 0 auto;width: 50%;">
            <div class="col-sm-12 pb--10">
                <h3 class="small-head">Masuk</h3>
            </div>
            <div class="input__box col-sm-12">
                <label>Nomor Telepon<span>*</span></label>
                <input type="text" name="phone" onkeyup="zero($(this))" id="phone">
            </div>
            <div class="input__box col-sm-12">
                <label>Nomor Kartu Member<span>*</span></label>
                <input type="text" id="card_no" name="card_no">
            </div>
            <!-- <div class="box-tocart d-flex">
                <input class="checkbox-cust" id="chk_prod" name="PROD[]" value="' . $dataaddrs['id'] . '" type="checkbox">
            </div> -->
            <div class="form__btn">
                <label class="label-for-checkbox">
                    <input id="confirm" class="input-checkbox" onclick="confirm_login($(this))" name="confirm" type="checkbox">
                    <span>Saya "SETUJU" kepada <a href="https://lottegrosir.co.id/skb" target="_blank"><b>Ketentuan Layanan dan Kebijakan Privasi Lotte Grosir</b></a></span>
                </label>
            </div>
            <div class="col-sm-12 text-center-cust">
                <button type="button" id="next_btn" disabled style="width: 100%;" class="btn-cust" div_now="div_login" href="javascript:void(0)" onclick="post_data($(this), 'div_otp', form_login_member)" style="width: 100%;">Selanjutnya</button>
            </div>
            <!-- <div class="col-sm-12 text-center-cust">
                <button type="button" id="send">Push</button>
            </div> -->
            <a class="forget_pass" div_now="div_login" href="javascript:void(0)" onclick="showdiv($(this), 'div_tamu')">Masuk Sebagai Tamu <i class="fa fa-chevron-right"></i></a>
        </div>
    </div>
</form>

<form action="{{ url('/login/regis_guest') }}" method="POST" name="form_regis_guest" id="form_regis_guest" enctype="multipart/form-data">
    @csrf
    <div class="container pt--80 pb--30" id="div_tamu" style="display: none;">
        <div class="row account__form">
            <div class="col-sm-12">
                <h3 class="small-head">Masuk Sebagai Tamu</h3>
            </div>
            <div class="input__box col-sm-12">
                <label>Nama<span>*</span></label>
                <input type="text" id="name" name="name">
            </div>
            <div class="input__box col-sm-12">
                <label>Email<span>*</span></label>
                <input type="text" id="email" name="email">
            </div>
            <div class="input__box col-sm-12">
                <label>Nomor Telepon<span>*</span></label>
                <input type="text" id="phone" onkeyup="zero($(this))" name="phone">
            </div>
            <div class="input__box col-sm-12">
                <label>Gender<span>*</span></label>
                <select name="gender" id="gender">
                    <option value="1">Male</option>
                    <option value="2">Female</option>
                </select>
            </div>
            <div class="form__btn">
                <label class="label-for-checkbox">
                    <input id="confirm_guest" class="input-checkbox" onclick="confirm_login_guest($(this))" name="confirm" type="checkbox">
                    <span>Saya "SETUJU" kepada <a href="https://lottegrosir.co.id/skb" target="_blank"><b>Ketentuan Layanan dan Kebijakan Privasi Lotte Grosir</b></a></span>
                </label>
            </div>
            <div class="col-sm-12 text-center-cust">
                <button type="button" style="width: 100%;" disabled id="next_btn_guest" class="btn-cust" div_now="div_tamu" href="javascript:void(0)" onclick="post_data($(this), 'div_succ', form_regis_guest)" style="width: 100%;">Selanjutnya</button>
            </div>
            <a class="forget_pass" div_now="div_tamu" href="javascript:void(0)" onclick="showdiv($(this), 'div_login')"><i class="fa fa-chevron-left"></i> Kembali ke Sebelumnya</a>
        </div>
    </div>
</form>

<form action="{{ url('/login/set_address') }}" method="POST" name="form_regis_addrs" id="form_regis_addrs" enctype="multipart/form-data">
    @csrf
    <div class="container pt--80 pb--30" id="div_acc" style="display: none;">
        <div class="row account__form">
            <div class="col-sm-12">
                <h3 class="small-head">Tambah Alamat</h3>
            </div>
            <div class="input__box col-lg-6 col-sm-12">
                <label>Nama Sebagai<span>*</span></label>
                <input type="text" name="title" id="title">
            </div>
            <div class="input__box col-lg-6 col-sm-12">
                <label>Nama Penerima<span>*</span></label>
                <input type="text" name="customer_name" id="customer_name">
            </div>
            <div class="input__box col-lg-6 col-sm-12">
                <label>Telepon Penerima<span>*</span></label>
                <input type="text" onkeyup="zero($(this))" name="phone" id="phone">
            </div>
            <div class="input__box col-lg-6 col-sm-12">
                <label>Alamat Penerima<span>*</span></label>
                <textarea type="text" name="address" onchange="focusmaps($(this))" id="address"></textarea>
            </div>
            <div class="input__box col-lg-6 col-sm-12">
                <label>Email Penerima<span>*</span></label>
                <input type="text" name="email" id="email">
            </div>
            <div class="input__box col-lg-6 col-sm-12">
                <label>Kode POS<span>*</span></label>
                <input type="text" name="zip_code" id="zip_code">
            </div>
            <div class="input__box col-lg-6 col-sm-12">
                <label>Provinsi<span>*</span></label>
                <select class="select2" onchange="get_regency($(this))" name="province_id" id="province_id">
                    
                </select>
            </div>
            <div class="input__box col-lg-6 col-sm-12">
                <label>Kabupaten / Kota<span>*</span></label>
                <select class="select2" name="regency_id" id="regency_id">

                </select>
            </div>
            <div class="input__box col-lg-12 col-sm-12">
                <div id="floating-panel">
                    <div class="wn__sidebar">
                        <aside class="widget search_widget">
                            <div class="form-input">
                                <input type="text" id="addrs" placeholder="Search...">
                                <button type="button" id="btn-lat"><i class="fa fa-search"></i></button>
                            </div>
                        </aside>
                    </div> 
                </div>
                <div id="map" style="height: 250px;"></div>
                <input type="hidden" name="latitude" id="lat">
                <input type="hidden" name="longitude" id="lon">
                <input type="hidden" name="flag" id="lon" value="0">
            </div>
            </div>
            <div class="col-sm-12 text-center-cust">
                <button type="button" style="width: 100%;" class="btn-cust" div_now="div_tamu" href="javascript:void(0)" onclick="post_data($(this), 'div_succ', form_regis_addrs)" style="width: 100%;">Simpan</button>
            </div>
        </div>
    </div>
</form>

<div class="container pt--80 pb--30" id="div_otp" style="display: none;">
    <form action="{{ url('/login/login_otp') }}" method="POST" name="form_otp" id="form_otp" enctype="multipart/form-data">
        @csrf
        <div class="row account__form">
            <div class="col-sm-12">
                <p class="small-head">Verifikasi Akun</p>
                <p class="small-p">Kami telah mengirimkan kode verifikasi 6 digit lewat SMS ke nomor telepon Anda.</p>
            </div>
            <div class="confirmation_code split_input small_top_margin" data-multi-input-code="true">
                <div class="confirmation_code_group">
                    <div class="split_input_item input_wrapper"><input type="text" id="code1" name="OTP[code1]" onkeyup="movefocus(this.form, this, 'code2', event, '');" class="inline_input" maxlength="1"></div>
                    <div class="split_input_item input_wrapper"><input type="text" id="code2" name="OTP[code2]" onkeyup="movefocus(this.form, this, 'code3', event, 'code1');" class="inline_input" maxlength="1"></div>
                    <div class="split_input_item input_wrapper"><input type="text" id="code3" name="OTP[code3]" onkeyup="movefocus(this.form, this, 'code4', event, 'code2');" class="inline_input" maxlength="1"></div>
                </div>

                <div class="confirmation_code_span_cell">—</div>

                <div class="confirmation_code_group">
                    <div class="split_input_item input_wrapper"><input type="text" id="code4" name="OTP[code4]" onkeyup="movefocus(this.form, this, 'code5', event, 'code3');" class="inline_input" maxlength="1"></div>
                    <div class="split_input_item input_wrapper"><input type="text" id="code5" name="OTP[code5]" onkeyup="movefocus(this.form, this, 'code6', event, 'code4');" class="inline_input" maxlength="1"></div>
                    <div class="split_input_item input_wrapper"><input type="text" id="code6" name="OTP[code6]" onkeyup="movefocus(this.form, this, '', event, 'code5');" class="inline_input" maxlength="1"></div>
                </div>
            </div>
            <div class="col-sm-12 text-center-cust" style="height: 50px;" id="for_time"></div>
            <div class="col-sm-12 text-center-cust" style="height: 50px;">
                <button type="button" class="btn-cust" id="resend" disabled div_now="div_login" href="javascript:void(0)" onclick="post_data($(this), 'div_otp', form_login_member)">Kirim Kembali</button>
            </div>
            <div class="col-sm-12 text-center-cust">
                <button type="button" style="width: 100%;" class="btn-cust" div_now="div_otp" href="javascript:void(0)" onclick="post_data($(this), 'div_acc', form_otp)">Verifikasi</button>
            </div>
        </div>
    </form>
</div>

<div class="container pt--80 pb--30" id="div_succ" style="display: none;">
    <form action="{{ url('/login/get_province') }}" method="POST" name="form_login_guest" id="form_login_guest" enctype="multipart/form-data">
        @csrf
        <div class="row account__form">
            <div class="col-sm-12">
                <p class="small-head">Registrasi Berhasil</p>
                <p class="small-p">Terima kasih telah mendaftarkan diri anda sebagai TAMU. Berikut ini adalah ID Tamu anda :</p>
            </div>
            <div class="col-sm-12 text-center-cust pb--10 pt--10">
                <h3 class="small-head" id="succ_card_no"></h3>
                <input type="hidden" name="card_no" id="card_no_guest">
                <input type="hidden" name="phone" id="phone_guest">
            </div>    
            <div class="col-sm-12">
                <p class="small-p" style="color: red;">*Nomor ID Tamu ini bukan ID member Lotte Grosir</p>
            </div>
            <div class="col-sm-12 text-center-cust">
                <button type="button" style="width: 100%;" class="btn-cust" div_now="div_succ" href="javascript:void(0)" onclick="directhome()">Lanjutkan</button>
            </div>
        </div>
    </form>
</div>
@endsection