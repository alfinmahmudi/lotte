@extends('layout.master')

@section('title', 'Lotte Grosir | Selamat Datang di Lotte Grosir ' . session('storedata.name'))

@section('upsource', view('layout.upsource', ['source' => $source['upsource']]))

@section('nav', view('layout.nav'))

@section('mobilemenu', view('layout.mobilemenu', ['heads' => ['search', 'card', 'store', 'cart', 'address', 'logout']]))

@section('headbar', view('layout.headbar', ['heads' => ['card', 'store', 'cart', 'address', 'logout']]))

@section('footer', view('layout.footer'))

@section('downsource', view('layout.downsource', ['plugins' => $source['down_plugins'], 'scripts' => $source['down_scripts']]))

@section('header', view('layout.header', ['name' => session('storedata.name')]))

@section('modal', view('modal_store', ['htmlStore' => $htmlStore]))

@section('content')
<meta name="csrf-token" content="{{ csrf_token() }}" />
<div class="cart-main-area section-padding--lg bg--white">
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-sm-8 ol-lg-8">
                <div class="post__itam">
                    <div class="content" style="padding: 20px 20px 15px;">
                        <span class="day" style="color: #e80916;">Pilih Semua</span>
                        <div class="post-meta">
                            <ul>
                                <li><a href="javascript:void(0)" onclick="delete_qty_all($(this))"><i class="fa fa-trash fa-lg"></i></a></li>
                                <li>
                                    <div class="box-tocart d-flex">
                                        <input class="checkbox-cust" id="check_all" checked type="checkbox">
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <form action="{{ url('/cart/set_final') }}" method="POST" name="form_cart_all" id="form_cart_all" enctype="multipart/form-data">
                    <input type="hidden" id="store_id" name="store_id" value="<?= session('storedata.id'); ?>">               
                    <?= $datacart['htmlcart']; ?>
                </form>
            </div>
            <div class="col-sm-4 col-md-4 col-lg-4">
                <div class="cartbox-total d-flex justify-content-between">
                    <ul class="cart__total__list">
                        <li><strong>Ringkasan Belanja</strong></li>
                        <li>Total Harga</li>
                    </ul>
                    <ul class="cart__total__list">
                        <li>&nbsp;</li>
                        <li>Rp. </li>
                    </ul>
                    <ul class="cart__total__tk" style="text-align: right;">
                        <li>&nbsp;</li>
                        <li id="total_amount_cart"><?= $datacart['total_amount_cart']; ?></li>
                    </ul>
                </div>
                <div class="col-sm-12 text-center-cust">
                    <button type="button" style="width: 100%;" class="btn-cust" href="javascript:void(0)" onclick="set_post_cart(form_cart_all)" style="width: 100%;">Proses Pesanan</button>
                </div>
            </div>
        </div>
    </div>  
</div>

<div id="modal-checkout" class="modal fade" role="dialog" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <section class="bg--white">
                    <form action="{{ url('/cart/update_notes') }}" method="POST" name="form_edit_notes" id="form_edit_notes" enctype="multipart/form-data">
                        <div class="container" id="div_login">
                            <div class="row account__form" style="border: none;">
                                <div class="col-sm-12 pb--10">
                                    <h3 class="small-head">Edit Notes</h3>
                                    <input type="hidden" name="product_modal" id="product_modal">
                                    <input type="hidden" name="store_modal" id="store_modal">
                                    <input type="hidden" name="qty_modal" id="qty_modal">
                                </div>
                                <div class="input__box col-sm-12">
                                    <textarea type="text" name="note_modal" id="note_modal"></textarea>
                                </div>
                                <div class="col-sm-12 text-center-cust">
                                    <button type="button" style="width: 100%;" class="btn-cust" div_now="div_login" href="javascript:void(0)" onclick="save_note(form_edit_notes)" style="width: 100%;">Simpan</button>
                                </div>
                            </div>
                        </div>
                    </form>  
                </section>              
            </div>
        </div>
    </div>
</div>
@endsection