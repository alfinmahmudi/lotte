@extends('layout.master')

@section('title', 'Lotte Grosir | Selamat Datang di Lotte Grosir ' . session('storedata.name'))

@section('upsource', view('layout.upsource', ['source' => $source['upsource']]))

@section('nav', view('layout.nav'))

@section('mobilemenu', view('layout.mobilemenu', ['heads' => ['search', 'card', 'store', 'cart', 'address', 'logout']]))

@section('headbar', view('layout.headbar', ['heads' => ['card', 'store', 'cart', 'address', 'logout']]))

@section('footer', view('layout.footer'))

@section('downsource', view('layout.downsource', ['plugins' => $source['down_plugins'], 'scripts' => $source['down_scripts']]))

@section('header', view('layout.header', ['name' => session('storedata.name')]))

@section('modal', view('modal_store', ['htmlStore' => $htmlStore]))

@section('content')

<form action="{{ url('/cart/preview') }}" method="POST" name="form_deliv" id="form_deliv" enctype="multipart/form-data">
    {{ csrf_field() }}
    <div class="cart-main-area">
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-sm-8 ol-lg-8">
                    <input type="hidden" name="store_id" value="<?= session('storedata.id'); ?>">
                    <input type="hidden" name="store_zip_code" value="<?= session('storedata.zip_code'); ?>">
                    <input type="hidden" id="addres_id_store" value="<?= session('dataaddress.id'); ?>">
                    <input type="hidden" name="customer_address_id" id="customer_address_id" value="<?= session('dataaddress.id'); ?>">
                    <div class="post_wrapper" id="for_addrs" style="display: none;">
                        <h3 class="reply_title">Alamat Pengiriman <small><a style="color:#e80916;" href="javascript:void(0)" onclick="change_addrs()">Ubah Alamat</a></small></h3>
                        <div id="content_addrs">
                            
                        </div>
                    </div>
                    <div class="row" style="border-bottom: 1px solid black; display: none;" id="for_store">
                        <div class="col-lg-6 col-sm-12">
                            <div class="post_wrapper">
                                <h3 class="reply_title">Alamat Toko </h3>
                                <div id="content_store">
                                    <div class="post_header">
                                        <div>
                                            <ul>
                                                <li>LOTTE <?= session('storedata.name'); ?></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="post_content">
                                        <p><?= session('storedata.contacts.0.value'); ?>
                                            <br> <?= session('storedata.address'); ?> </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6 col-sm-12">
                            <div class="post_wrapper">
                                <h3 class="reply_title">Informasi Pengambilan Produk </h3>
                                <div id="content_store">
                                    <div class="post_header">
                                        <div>
                                            <ul>
                                                <li>
                                                    Produk yang Anda pesan dapat diambil setelah konfirmasi pembayaran dan menerima notifikasi status melalui aplikasi. <br> 
                                                    Pesanan yang masuk diatas jam operasional Toko akan diproses pada hari berikutnya.
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <?= $datacart['htmlcart']; ?>
                    <input type="hidden" name="total_amount_cart" id="total_amount_cart" value="<?= $datacart['total_amount_cart']; ?>">
                    <input type="hidden" name="total_delivery" id="total_delivery">
                    <input type="hidden" name="real_delivery" id="real_delivery">
                    <input type="hidden" name="total_diskon" id="total_diskon">
                </div>
                <div class="col-sm-4 col-md-4 col-lg-4">
                    <div class="cartbox-total d-flex justify-content-between">
                        <ul class="cart__total__list">
                            <li><strong>Ringkasan Belanja</strong></li>
                            <li>Sub Total</li>
                            <li id="li_txt_deliv" style="display: none;">Ongkos Kirim</li>
                            <li id="li_txt_diskon" style="display: none;">Diskon Ongkos Kirim</li>
                            <li>Total</li>
                        </ul>
                        <ul class="cart__total__list">
                            <li>&nbsp;</li>
                            <li>Rp. </li>
                            <li id="li_amount_deliv" style="display: none;">Rp. </li>
                            <li id="li_amount_diskon" style="display: none;">Rp. </li>
                            <li>Rp. </li>
                        </ul>
                        <ul class="cart__total__tk" style="text-align: right;">
                            <li>&nbsp;</li>
                            <li><?= number_format($datacart['total_amount_cart']); ?></li>
                            <li id="total_amount_delivery" style="display: none;"></li>
                            <li id="total_amount_diskon" style="display: none;"></li>
                            <li id="total_amount_cart_txt"><?= number_format($datacart['total_amount_cart']); ?></li>
                        </ul>
                    </div>
                    <div class="row account__form" style="border:0px; margin: 0px; padding: 0px;">
                        <div class="input__box col-lg-12 col-sm-12">
                            <label>Metode Pengiriman<span>*</span></label>
                            <select class="select2" onchange="get_shipp_method($(this))" name="shipp_method" id="shipp_method">
                                <?= $htmlship; ?>
                            </select>
                            <input type="hidden" name="shipp_method_txt" id="shipp_method_txt">
                        </div>
                        <div class="input__box col-lg-12 col-sm-12" id="for_expidition" style="display: none;">
                            <label>Pilih Kurir<span>*</span></label>
                            <select class="select2" data-action="<?= url('/cart/get_durasi') ?>" onchange="get_durasi($(this))" name="expidition" id="expidition">
                                
                            </select>
                            <input type="hidden" name="expidition_txt" id="expidition_txt">
                        </div>
                        <div class="col-lg-12 col-sm-12" id="for_durasi" style="display: none;">
                            <label>Durasi Pengiriman<span>*</span></label>
                            <div class="post__itam">
                                <div class="content" style="padding: 20px 20px 15px;" id="durasi_div">
                                    
                                </div>
                            </div>
                            <input type="hidden" name="durasi_txt" id="durasi_txt">
                        </div>
                    </div>
                    <div class="col-sm-12 text-center-cust">
                        <button type="submit" style="width: 100%;" id="submit_checkout" class="btn-cust" style="width: 100%;">Proses Pembayaran</button>
                    </div>
                </div>
            </div>
        </div>  
    </div>
</form>

<div id="modal-change-addrs" class="modal fade" role="dialog" data-backdrop="static" data-keyboard="false">
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <section class="bg--white">
                    <form action="{{ url('/cart/set_chenge_addrs') }}" method="POST" name="form_change_addrs" id="form_change_addrs" enctype="multipart/form-data">
                        <div class="post_wrapper">
                            <h3 class="reply_title">Pilih Alamat 
                            <small><a style="color:#e80916;" href="javascript:void(0)" data-id="0" onclick="show_addrs($(this), 1)" data-url="<?= url('/login/get_address_detail'); ?>">Tambah Alamat</a></small></h3>
                        </div>
                        <div class="post__itam">
                            <div class="content" style="padding: 20px 20px 15px;" id="addrs_modal">
                                
                            </div>
                        </div>
                        <div class="col-sm-12 text-center-cust">
                            <button type="button" style="width: 100%;" class="btn-cust" href="javascript:void(0)" onclick="set_change_addrs(form_change_addrs)">Pilih Alamat</button>
                        </div>
                    </form>  
                </section>              
            </div>
        </div>
    </div>
</div>

<div id="modal-payment" class="modal fade" role="dialog" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-lg">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-body" id="modal-body-payment" style="height: 800px">
                
            </div>
        </div>
    </div>
</div>

<div id="modal-addrs" class="modal fade" role="dialog" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog modal-lg">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-body">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <section class="bg--white">
            <div class="maincontent bg--white">
				<div class="container" id="cont_addrs">
                    
				</div>
			</div>
		</section>
	  </div>
	</div>
	<!-- Modal content-->
	
  </div>
</div>
@endsection