@extends('layout.master')

@section('title', 'Lotte Grosir | Selamat Datang di Lotte Grosir ' . session('storedata.name'))

@section('upsource', view('layout.upsource', ['source' => $source['upsource']]))

@section('nav', view('layout.nav'))

@section('mobilemenu', view('layout.mobilemenu', ['heads' => ['search', 'card', 'store', 'cart', 'address', 'logout']]))

@section('headbar', view('layout.headbar', ['heads' => ['card', 'store', 'cart', 'address', 'logout']]))

@section('footer', view('layout.footer'))

@section('downsource', view('layout.downsource', ['plugins' => $source['down_plugins'], 'scripts' => $source['down_scripts']]))

@section('header', view('layout.header', ['name' => session('storedata.name')]))

@section('modal', view('modal_store', ['htmlStore' => $htmlStore]))

@section('content')
<meta name="csrf-token" content="{{ csrf_token() }}" />
<div class="cart-main-area">
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-sm-8 ol-lg-8">
                <div class="col-lg-12 col-12" style="padding: 20px 20px 15px;">
                    <table>
                        <tr>
                            <td>Metode Pengiriman</td>
                            <td>:</td>
                            <td><?= $datacart['shipp_method_txt']; ?></td>
                        </tr>
                        <tr>
                            <td>Nama Toko</td>
                            <td>:</td>
                            <td>LOTTE GROSIR <?= session('storedata.name'); ?></td>
                        </tr>
                        <?= $datacart['additional_info']; ?>
                    </table>
                </div>
                <?= $datacart['htmlcart']; ?>
            </div>
            
            <div class="col-sm-4 col-md-4 col-lg-4">
                <div class="cartbox-total d-flex justify-content-between">
                    <ul class="cart__total__list">
                        <li><strong>Ringkasan Belanja</strong></li>
                        <li>Sub Total</li>
                        <li>Biaya Layanan</li>
                        <li>Diskon Biaya Layanan</li>
                        <?= $datacart['li_up_deliv']; ?>
                        <?= $datacart['li_up_deliv_disc']; ?>
                        <li>Biaya Admin Bank</li>
                        <li>Diskon Biaya Admin Bank</li>
                        <li>Total</li>
                    </ul>
                    <ul class="cart__total__list">
                        <li>&nbsp;</li>
                        <li><strong>Rp. </strong></li>
                        <li><strong>Rp. </strong></li>
                        <li><strong>Rp. </strong></li>
                        <?= $datacart['li_mid_deliv']; ?>
                        <?= $datacart['li_mid_deliv_disc']; ?>
                        <li><strong>Rp. </strong></li>
                        <li><strong>Rp. </strong></li>
                        <li><strong>Rp. </strong></li>
                    </ul>
                    <ul class="cart__total__tk" style="text-align: right;">
                        <li>&nbsp;</li>
                        <li><?= number_format($datacart['sub_total_price']); ?></li>
                        <li id="biaya_layanan">0</li>
                        <li id="disc_biaya_layanan">0</li>
                        <?= $datacart['li_down_deliv']; ?>
                        <?= $datacart['li_down_deliv_disc']; ?>
                        <li id="admin_bank">0</li>
                        <li id="disc_admin_bank">0</li>
                        <li id="total_amount_cart_txt"><?= number_format($datacart['total_price']); ?></li>
                    </ul>
                </div>
                <div id="promo-used" class="checkout_accordion mt--30 pb--30" role="tablist" style="display: none;">
                    <div class="payment">
                        <div class="che__header" role="tab" id="headingOne">
                            <a class="checkout__title" data-toggle="collapse" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                <span>Promo</span>
                            </a>
                        </div>
                        <div id="collapseOne" class="collapse show" role="tabpanel" aria-labelledby="headingOne" data-parent="#accordion">
                            <div class="payment-body" id="promo-txt">Selamat anda mendapatkan potongan sebesar 0</div>
                        </div>
                    </div>
                </div>
                <form action="{{ url('/cart/checkout_final') }}" method="POST" name="form_deliv" id="form_deliv" enctype="multipart/form-data">
                    <input type="hidden" id="store_id" name="store_id" value="<?= session('storedata.id'); ?>">
                    <input type="hidden" name="store_zip_code" value="<?= session('storedata.zip_code'); ?>">
                    <input type="hidden" name="shipp_method" value="<?= $datacart['shipp_method']; ?>">
                    <input type="hidden" name="expidition" id="expidition" value="<?= $datacart['expidition']; ?>">
                    <input type="hidden" name="durasi" id="durasi" value="<?= $datacart['durasi']; ?>">
                    <input type="hidden" name="customer_address_id" id="customer_address_id" value="<?= $datacart['customer_address_id']; ?>">
                    <input type="hidden" name="transaction_id" id="transaction_id">
                    
                    <div class="input__box col-lg-12 col-sm-12">
                        <label>Pembayaran<span>*</span></label>
                        <select class="select2" name="payment_method" data-url="{{ url('/cart/preview_final') }}" onchange="final_preview($(this), form_deliv)" id="payment_method">
                            <?= $htmlpayment; ?>
                        </select>
                        <input type="hidden" name="payment_method_text" id="payment_method_text">
                    </div>
                    
                    <div class="input__box col-lg-12 col-sm-12">
                        <label>&nbsp;</label>
                        <button type="button" style="width: 100%;" class="btn-cust" href="javascript:void(0)" onclick="open_promo()" style="width: 100%;">Input Kode Promo</button>
                    </div>

                    <input type="hidden" name="total_amount_cart" id="total_amount_cart" value="<?= $datacart['total_price']; ?>">
                    <input type="hidden" name="total_delivery" id="total_delivery" value="<?= $datacart['deliv_price']; ?>">
                    <input type="hidden" name="real_delivery" id="real_delivery" value="<?= $datacart['real_delivery']; ?>">
                    <input type="hidden" name="total_diskon" id="total_diskon" value="<?= $datacart['total_diskon']; ?>">
                </form>
                <div class="col-sm-12 text-center-cust">
                    <button type="button" style="width: 100%;" class="btn-cust" href="javascript:void(0)" onclick="set_checkout(form_deliv)" style="width: 100%;">Proses Pembayaran</button>
                </div>
            </div>
        </div>
    </div>  
</div>

<div id="modal-payment" class="modal fade" role="dialog" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-lg">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-body" id="modal-body-payment" style="height: 800px">
                
            </div>
        </div>
    </div>
</div>

<div id="modal-promo" class="modal fade" role="dialog" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-lg">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <section class="bg--white">
                    <div class="maincontent bg--white">
                        <div class="container" id="modal-promo-body">
                            <div class="post_wrapper">
                                <h3 class="reply_title">Kode Promo 
                            </div>
                            <div class="input-group mb-3">
                                <input type="text" class="form-control" id="code_txt" placeholder="Input Kode Promo" aria-describedby="basic-addon2">
                                <div class="input-group-append">
                                    <button class="btn-cust" onclick="search_code_voucher();" type="button">Gunakan</button>
                                </div>
                            </div>
                            <div id="voucher-code">
                                
                            </div>
                            <div id="voucher-list">
                                
                            </div>
                            <input type="hidden" id="arrcode" >
                        </div>
                        <div class="container" id="modal-promo-detail" style="display: none;">
                            <input type="hidden" id="vou_id">
                            <input type="hidden" id="vou_code">
                            <div class="post_wrapper">
                                <h3 class="reply_title">Detail Kupon
                            </div>
                            <div class="col-lg-12" id="voucher-detail">
                                
                            </div>
                            <div class="col-sm-12">
                                <button type="button" style="width: 100%;" class="btn-cust" data-url="{{ url('/cart/use_voucher') }}" onclick="use_voucher($(this), form_deliv);" href="javascript:void(0)">Gunakan Kupon</button>
                            </div>
                            <div class="col-sm-12 text-center-cust">
                                <button type="button" style="width: 100%;" class="btn-cust" id="back_button_voucher" code="" href="javascript:void(0)" onclick="back_to_list();">Kembali</button>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>
</div>
@endsection