<script src="{{ asset('js/maps.js') }}"></script>
<form action="{{ url('/login/set_address') }}" autocomplete="off" method="POST" name="form_regis_addrs" id="form_regis_addrs" enctype="multipart/form-data">
    @csrf
    <div class="container" id="div_acc">
        <div class="row account__form">
            <div class="col-sm-12">
                <h3 class="small-head">Tambah Alamat</h3>
            </div>
            <div class="input__box col-lg-6 col-sm-12">
                <label>Nama Sebagai<span>*</span></label>
                <input type="text" name="title" id="title">
            </div>
            <div class="input__box col-lg-6 col-sm-12">
                <label>Nama Penerima<span>*</span></label>
                <input type="text" name="customer_name" id="customer_name">
            </div>
            <div class="input__box col-lg-6 col-sm-12">
                <label>Telepon Penerima<span>*</span></label>
                <input type="text" name="phone" onkeyup="zero($(this))" id="phone">
            </div>
            <div class="input__box col-lg-6 col-sm-12">
                <label>Alamat Penerima<span>*</span></label>
                <textarea type="text" name="address" id="address" onkeyup="focusmaps($(this))"></textarea>
            </div>
            <div class="input__box col-lg-6 col-sm-12">
                <label>Email Penerima<span>*</span></label>
                <input type="text" name="email" id="email">
            </div>
            <div class="input__box col-lg-6 col-sm-12">
                <label>Kode POS<span>*</span></label>
                <input type="text" name="zip_code" id="zip_code">
            </div>
            <div class="input__box col-lg-6 col-sm-12">
                <label>Provinsi<span>*</span></label>
                <select class="select2" onchange="get_regency($(this))" name="province_id" id="province_id">
                    <?= $prov; ?>
                </select>
            </div>
            <div class="input__box col-lg-6 col-sm-12">
                <label>Kabupaten / Kota<span>*</span></label>
                <select class="select2" name="regency_id" id="regency_id">

                </select>
            </div>
            <div class="input__box col-lg-6 col-sm-12">
                <label>Default Address<span>*</span></label>
                <select class="select2" name="is_default" id="is_default">
                    <option value="0">No</option>
                    <option value="1">Yes</option>
                </select>
            </div>
            <div class="input__box col-lg-12 col-sm-12">
                <div id="floating-panel">
                    <!-- <div class="wn__sidebar">
                        <aside class="widget search_widget">
                            <div class="form-input autocomplete">
                                <input type="text" id="addrs" placeholder="Search...">
                                <button type="button" id="btn-lat"><i class="fa fa-search"></i></button>
                            </div>
                        </aside>
                    </div>  -->
                    <input class="form-control form-control-sm ml-3 w-75" type="text" autocomplete="off" id="map_search" placeholder="Search..." aria-label="Search">
                    <input type="hidden" name="search" id="search">
                </div>
                <div id="map" style="height: 250px;"></div>
                <input type="hidden" name="latitude" id="lat">
                <input type="hidden" name="longitude" id="lon">
                <input type="hidden" name="flag" id="lon" value="<?= $flag ?>">
            </div>
            <div class="input__box col-lg-12 col-sm-12">
                <input type="text" id="lonlat" readonly style="text-align: center;">
            </div>
            <div class="col-sm-12 text-center-cust">
                <button type="button" style="width: 100%;" class="btn-cust" div_now="div_tamu" href="javascript:void(0)" onclick="post_data_public($(this), form_regis_addrs)" style="width: 100%;">Simpan</button>
            </div>
        </div>
    </div>
</form>