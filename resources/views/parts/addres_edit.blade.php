
<script src="{{ asset('js/maps.js') }}"></script>
<form action="{{ url('/login/update_address') }}" method="POST" name="form_regis_addrs" id="form_regis_addrs" enctype="multipart/form-data">
    @csrf
    <input type="hidden" id="edited_addrs" value="1">
    <div class="container" id="div_acc">
        <div class="row account__form">
            <div class="col-sm-12">
                <h3 class="small-head">Edit Alamat</h3>
                <input type="hidden" name="address_id" value="<?= $data['id'] ?>">
            </div>
            <div class="input__box col-lg-6 col-sm-12">
                <label>Nama Sebagai<span>*</span></label>
                <input type="text" value="<?= $data['title'] ?>" name="ADDRS[title]" id="title">
            </div>
            <div class="input__box col-lg-6 col-sm-12">
                <label>Nama Penerima<span>*</span></label>
                <input type="text" value="<?= $data['customer_name'] ?>" name="ADDRS[customer_name]" id="customer_name">
            </div>
            <div class="input__box col-lg-6 col-sm-12">
                <label>Telepon Penerima<span>*</span></label>
                <input type="text" value="<?= $data['phone'] ?>" name="ADDRS[phone]" id="phone">
            </div>
            <div class="input__box col-lg-6 col-sm-12">
                <label>Alamat Penerima<span>*</span></label>
                <textarea type="text" name="ADDRS[address]" id="address"><?= $data['address'] ?></textarea>
            </div>
            <div class="input__box col-lg-6 col-sm-12">
                <label>Email Penerima<span>*</span></label>
                <input type="text" value="<?= $data['email'] ?>" name="ADDRS[email]" id="email">
            </div>
            <div class="input__box col-lg-6 col-sm-12">
                <label>Kode POS<span>*</span></label>
                <input type="text" value="<?= $data['zip_code'] ?>" name="ADDRS[zip_code]" id="zip_code">
            </div>
            <div class="input__box col-lg-6 col-sm-12">
                <label>Provinsi<span>*</span></label>
                <select class="select2" onchange="get_regency($(this))" name="ADDRS[province_id]" id="province_id">
                    <?= $prov ?>
                </select>
            </div>
            <div class="input__box col-lg-6 col-sm-12">
                <label>Kabupaten / Kota<span>*</span></label>
                <select class="select2" name="ADDRS[regency_id]" onchange="focusmaps($(this))" id="regency_id">
                    <?= $reg ?>
                </select>
            </div>
            <div class="input__box col-lg-6 col-sm-12">
                <label>Default Address<span>*</span></label>
                <select class="select2" name="ADDRS[is_default]" id="is_default">
                    <?php if ($data['is_default'] == 1) {
                        $selected = 'selected';
                    }else {
                        $selected = '';
                    } ?>
                    <option value="0">No</option>
                    <option value="1" <?= $selected; ?>>Yes</option>
                </select>
            </div>
            <div class="input__box col-lg-12 col-sm-12">
                <div id="floating-panel">
                    <!-- <div class="wn__sidebar">
                        <aside class="widget search_widget">
                            <div class="form-input">
                                <input type="text" id="addrs" placeholder="Search...">
                                <button type="button" id="btn-lat"><i class="fa fa-search"></i></button>
                            </div>
                        </aside>
                    </div>  -->
                    <input class="form-control form-control-sm ml-3 w-75" type="text" autocomplete="off" id="map_search" placeholder="Search..." aria-label="Search">
                    <input type="hidden" name="search" id="search">
                </div>
                <div id="map" style="height: 180px;"></div>
                <input type="hidden" name="ADDRS[latitude]" id="lat" value="<?= $data['latitude'] ?>">
                <input type="hidden" name="ADDRS[longitude]" id="lon" value="<?= $data['longitude'] ?>">
                <input type="hidden" name="flag" id="lon" value="<?= $flag ?>">
            </div>
            <div class="input__box col-lg-12 col-sm-12">
                <input type="text" id="lonlat" value="<?= $data['latitude'] . ', ' . $data['longitude'] ?>" readonly style="text-align: center;">
            </div>
            <div class="col-sm-12 text-center-cust">
                <button type="button" style="width: 100%;" class="btn-cust" div_now="div_tamu" href="javascript:void(0)" onclick="post_data_public($(this), form_regis_addrs)" style="width: 100%;">Simpan</button>
            </div>
        </div>
    </div>
</form>