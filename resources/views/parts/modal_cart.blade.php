<meta name="csrf-token" content="{{ csrf_token() }}" />
<div id="modal-quantity" class="modal fade" role="dialog" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog modal-lg">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-body">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <section class="bg--white">
            <div class="maincontent bg--white pt--80 pb--55">
				<div class="container">
					<form id="form_quantity" name="form_quantity" action="{{ url('/cart/add_to_cart') }}" method="POST" enctype="multipart/form-data">
						<input type="hidden" id="quantity_prod" name="DATA[product_id]" value="">
						<input type="hidden" id="quantity_store" name="DATA[store_id]">
						<input type="hidden" id="price" name="price">
						<input type="hidden" id="hidden_grand_total" name="grand_total">
						<input type="hidden" id="img" name="img">
						<input type="hidden" id="name" name="name">
						<div class="row">
							<div class="col-lg-12 col-12">
								<div class="wn__single__product">
									<div class="row">
										<div class="col-lg-6 col-12">
											<div class="wn__fotorama__wrapper">
												<div class="fotorama wn__fotorama__action" id="quantity_img" data-nav="thumbs">
													
												</div>
											</div>
										</div>
										<div class="col-lg-6 col-12">
											<div class="product__info__main">
												<h1 id="quantity_name"></h1>
												<div class="price-box" id="quantity_price">
													
												</div>
												<div class="box-tocart d-flex">
													<span>Qty</span>
													<input id="qty" name="DATA[total]" class="input-text qty" onkeyup="set_total($(this))" min="1" value="1" title="Qty" type="number">
													<input type="hidden" id="quantity_max">
												</div>
											</div>
										</div>
									</div>
									<div class="row" id="for_bblm">

									</div>
									<div class="row account__form" style="border: none;">
										<div class="input__box col-sm-12">
											<label>Notes</label>
											<textarea name="DATA[note]" id=""></textarea>
										</div>
										<div class="input__box form__btn col-sm-12" style="height: 0%; text-align: right;">
											<!-- <span>Total Harga : <span id="grand_total"></span></span> &emsp;  -->
											<button type="button" onclick="add_to_cart(form_quantity)">Add To Cart</button>
										</div>
									</div>
								</div>
							</div>
						</div>
					</form>
				</div>
			</div>
		</section>
	  </div>
	</div>
	<!-- Modal content-->
	
  </div>
</div>