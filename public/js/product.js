$.ajaxSetup({

    headers: {

        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')

    }

});

$(document).ready(function () {
    var options = {

        url: function(phrase) {
          return site_url + '/search_results/' + $("#comp_search").val();
        },
      
        getValue: function(element) {
          return element.product_name;
        },
      
        ajaxSettings: {
          dataType: "json",
          method: "GET",
          data: {
            dataType: "json"
          }
        },
      
        preparePostData: function(data) {
          data.phrase = $("#comp_search").val();
          return data;
        },

        list: {
            match: {
                enabled: true
            },
            onSelectItemEvent: function() {
                var selectedItemValue = $("#comp_search").getSelectedItemData().product_name; 
                $("#search").val(selectedItemValue).trigger("change");
                // search_submit(selectedItemValue);
                // console.log(selectedItemValue);
            },
            onHideListEvent: function() {
                if ($("#search").val()) {
                    search_submit();
                }else{
                    console.log("nope");
                }
            }
        },
        
        requestDelay: 400
    };
      
    
    $("#comp_search").easyAutocomplete(options);
});

$(window).scroll(function () {
    // console.log('screen  ' + $(document).height());
    // console.log('scroll  ' + Math.round($(window).scrollTop() + $(window).height()));
    var scroll = Math.round($(window).scrollTop() + $(window).height()) - 50;
    var screen = $(document).height() - 50;
    if (scroll == screen) {
        // alert("bottom!");
        var store_id = $('#store_id').val();
        var page = $('#page').val();
        $('#page').val(parseInt(page) + 1);
        $.ajax({
            type: "GET",
            url: site_url + '/get_cust_product/' + store_id + '/' + page,
            dataType: 'json',
            beforeSend: function () {
                $('#loading_page').show();
            },
            error: function () {
                swal({
                    type: 'error',
                    title: 'Ooops! Something Wrong in Here',
                    text: 'Silahkan Refresh Halaman Dan Coba Kembali',
                });
            },
            success: function (data) {
                // console.log(data);
                // return false;
                if (data) {
                    if (data == 'Product Not Found') {
                        $('#main_prod').html(data);
                    }else{
                        $('#main_prod').append(data);
                    }
                } else {
                    $('#down_prod').html('You In The End Of Page');
                }
                $('#loading_page').hide();
            }
        });

    }
});
