$(document).ready(function () {
    if ($('#is_login').val() == false) {
        $('#modal-login').modal('show');
    }

    map = new google.maps.Map(document.getElementById("map"), 
    {
        zoom: 15,
        center: { lat: -6.208428, lng: 106.7824432 }
    });

    var geocoder = new google.maps.Geocoder();
    document.getElementById("btn-lat").addEventListener("click", function() {
        geocodeAddress(geocoder, map);
    });

    map.addListener('click', function(mapsMouseEvent) {
        setMapOnAll(null);

        addMarker(mapsMouseEvent.latLng);
        var geo = mapsMouseEvent.latLng.toString();
        geo = geo.substring(1, geo.length - 1);
        // console.log(geo);
        var arrgeo = geo.split(",");
        $('#lat').val(parseFloat(arrgeo[0]));
        $('#lon').val(parseFloat(arrgeo[1]));
    });
});

// Initialize and add the map
// function myMap() {
//     // The location of Uluru
//     var uluru = {lat: -25.344, lng: 131.036};
//     // The map, centered at Uluru
//     var map = new google.maps.Map(
//         document.getElementById('map'), {zoom: 4, center: uluru});
//     // The marker, positioned at Uluru
//     var marker = new google.maps.Marker({position: uluru, map: map});
// }

function unique(array){
    return array.filter(function(el, index, arr) {
        return index === arr.indexOf(el);
    });
}

// function getlokasi(lokasi) {
//     var key = 'AIzaSyBZH__uXOLAUb0prGZN_qmeUPFM6Oopy_w';
//     $.get( "https://maps.googleapis.com/maps/api/geocode/json", {address: lokasi.val(), key: key}, function( data ) {
//         console.log(data);
//     });
    
// }

// $.ajaxSetup({

//     headers: {

//         'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')

//     }

// });

function startTimer(duration, display) {
    var timer = duration,
        minutes, seconds;
    setInterval(function () {
        minutes = parseInt(timer / 60, 10);
        seconds = parseInt(timer % 60, 10);

        minutes = minutes < 10 ? "0" + minutes : minutes;
        seconds = seconds < 10 ? "0" + seconds : seconds;

        display.textContent = minutes + ":" + seconds;

        if (--timer < 0) {
            // location.reload();
        }
    }, 1000);
}

function showdiv(par, target) {
    // console.log(form_name);
    // return false;
    var divnow = par.attr('div_now');
    $('#' + divnow).find('input:text:visible').val('');

    if (target == 'div_acc') {
        $('.modal-dialog').addClass('modal-lg');
    } else {
        $('.modal-dialog').removeClass('modal-lg');
    }
    $('#' + divnow).hide();
    $('#' + target).show();

}

function directhome() {
    swal({
        type: 'success',
        title: 'Registrasi Berhasil',
        text: 'Silahakan Login Ke Halaman Selanjutnya'
    });

    setTimeout(function () {
        location.href = site_url;
    }, 2000);
    return false;
}

function post_data(par, target, form_name) {
    // console.log(form_name);
    // return false;

    var form_table = new FormData(form_name);
    var url = $(form_name).attr('action');
    
    $.ajax({
        type: "POST",
        url: url,
        data: form_table,
        dataType: 'json',
        processData: false,
        contentType: false,
        beforeSend: function () {
            swal({
                imageUrl: site_url + '/img/pacman.gif',
                imageHeight: 1500,
                title: 'Please Wait',
                showCancelButton: true,
                showConfirmButton: false,
                text: 'We Process Your Request',
            },
                function (isConfirm) {
                    if (isConfirm) {
                        xhr.abort();
                    }
                });
        },
        error: function () {
            swal({
                type: 'error',
                title: 'Cek Nomor Kartu Gagal',
                text: 'Terjadi Kesalahan Pada Sitem',
            });
        },
        success: function (data) {
            // console.log(data);
            // return false;
            if (data.res) {
                var divnow = par.attr('div_now');
                if ($(form_name).attr('id') != 'form_login_member') {
                    $('#' + divnow).find('input:text:visible').val('');
                }

                if (data.url && target == 'div_succ') {
                    swal({
                        type: 'warning',
                        title: data.title,
                        text: data.msg
                    });

                    setTimeout(function () {
                        location.href = data.url;
                    }, 2000);
                    return false;
                }

                if (target == 'div_acc') {
                    $('.modal-dialog').addClass('modal-lg');
                } else {
                    $('.modal-dialog').removeClass('modal-lg');
                }

                if ($(form_name).attr('id') == 'form_regis_guest') {
                    swal.close();
                    // console.log(data.data.card_no);
                    $('#succ_card_no').html(data.data.card_no);
                    $('#card_no_guest').val(data.data.card_no);
                    $('#phone_guest').val(data.data.phone);
                }

                if ($(form_name).attr('id') == 'form_login_member') {
                    // console.log(data);
                    // return false;
                    swal.close();

                    var htmltime = 'Akan Mengirim Code Kembali Dalam Waktu <span id="time"> 05:00 </span> menit!';
                    $('#for_time').html(htmltime);
                    var fiveMinutes = 60 * 5;
                    var resendtime = parseInt(fiveMinutes) * 1000;
                    // console.log(resendtime);
                    display = document.querySelector('#time');
                    startTimer(fiveMinutes, display);
                    setTimeout(function () {
                        // console.log("masuk");
                        $('#resend').removeAttr('disabled');
                    }, resendtime);
                }

                if ($(form_name).attr('id') == 'form_otp') {
                    // console.log(data);
                    // return false;
                    if (data.addrs) {
                        $('.close').click();
                        swal({
                            type: 'success',
                            title: data.msg,
                            text: 'Please Wait',
                        });

                        setTimeout(function () {
                            location.href = data.url;
                        }, 2000);
                        return false;
                    }else{
                        swal.close();
                        $('#province_id').html(data.htmlprov);
                    }
                }

                if ($(form_name).attr('id') == 'form_login_guest') {
                    swal.close();
                    $('#province_id').html(data.htmlprov);
                }

                if ($(form_name).attr('id') == 'form_regis_addrs') {
                    // console.log(data.htmlprov);
                    swal({
                        type: 'success',
                        title: data.msg,
                        text: 'Please Wait',
                    });
                    $('#modal-login').modal('toogle');
                    setTimeout(function () {
                        location.href = data.url;
                    }, 2000);
                    return false;
                }

                $('#' + divnow).hide();
                $('#' + target).show();   
            }else{
                if (data.title) {
                    swal({
                        type: 'error',
                        title: data.title,
                        text: data.msg,
                    });
                }else{
                    swal({
                        type: 'error',
                        title: data.msg
                    });
                }
            }
        }
    });
}

function movefocus(nmfrm, nmthis, nextfocus, event, prevfocus) {
    var tempKey = event.keyCode.toString();
    if (tempKey == 8) {
        if ((nmthis.value.length == 0) && (prevfocus != '')) {
            nmfrm[prevfocus].focus();
        }
    } else {
        if ((nmthis.maxLength == nmthis.value.length) && (nextfocus != '')) {
            nmfrm[nextfocus].select();
            nmfrm[nextfocus].focus();
        }
    }
}

function showmodal(params) {
    var url = params.attr('data-url');
    // add this for post method
    // headers: {
    //     'X-CSRF-TOKEN': token
    // },
    // var token = $('[name="_token"]').val();
    // console.log(token);

    $.ajax({
        type: "GET",
        url: url,
        dataType: 'json',
        processData: false,
        contentType: false,
        beforeSend: function () {
            swal({
                    imageUrl: site_url + '/img/pacman.gif',
                    imageHeight: 1500,
                    title: 'Please Wait',
                    showCancelButton: true,
                    showConfirmButton: false,
                    text: 'We Process Your Request',
                },
                function (isConfirm) {
                    if (isConfirm) {
                        xhr.abort();
                    }
                });
        },
        error: function () {
            swal({
                type: 'error',
                title: 'Ooops! Something Wrong in Here',
                text: 'Silahkan Refresh Halaman Dan Coba Kembali',
            });
        },
        success: function (data) {
            // console.log(data);
            // return false;
            $('#modal-default').html(data.temp);
            $('#modal-default').modal('show');
            swal.close();
        }
    });
}