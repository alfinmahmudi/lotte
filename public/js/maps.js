$(document).ready(function () {
    var options = {

        url: function(phrase) {
          return site_url + '/search_maps/' + $("#map_search").val();
        },
      
        getValue: function(element) {
          return element.desc;
        },
      
        ajaxSettings: {
          dataType: "json",
          method: "GET",
          data: {
            dataType: "json"
          }
        },
      
        preparePostData: function(data) {
          data.phrase = $("#map_search").val();
          return data;
        },

        list: {
            match: {
                enabled: true
            },
            onSelectItemEvent: function() {
                var selectedItemValue = $("#map_search").getSelectedItemData().map_id; 
                $("#search").val(selectedItemValue).trigger("change");
                // console.log(selectedItemValue);
            },
            onHideListEvent: function() {
                if ($("#search").val()) {
                    complete($("#map_search").val());
                }
            }
        },
      
        requestDelay: 400
    };
      
    $("#map_search").easyAutocomplete(options);
});