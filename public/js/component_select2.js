var JsonGeneralSelect2 = function () {
	var Select2Clasic = function (custom_placeholder) {
		//ref: [ROOT]\assets\pages\scripts\components-select2.js
		$.fn.select2.defaults.set("theme", "bootstrap");

		var placeholder = (custom_placeholder) ? custom_placeholder : "Select..";

		$(".select2, .select2-multiple").select2({
			placeholder: placeholder,
			width: null
		});

		$(".select2-allow-clear").select2({
			allowClear: true,
			placeholder: placeholder,
			width: null
		});
	};

	var Select2Remote = function (set_class_name, set_url) {
		$.fn.select2.defaults.set("theme", "bootstrap");

		var placeholder = "Select..";

		$("." + set_class_name).select2({
			width: "off",
			placeholder: placeholder,
			allowClear: true,
			ajax: {
				url: set_url,
				dataType: 'json',
				delay: 250,
				data: function (params) {
					return {
						term: params.term, // search term
						page: params.page
					};
				},
				processResults: function (data, page) {
					return {
						results: $.map(data, function (items) {
							return {
								text: items.rs_text,
								id: items.rs_id
							}
						})
					};
				},
				cache: true
			},
			escapeMarkup: function (markup) {
				return markup;
			},
			minimumInputLength: 2,
			minimumResultsForSearch: 10,
			templateSelection: function (container) {
				$(container.element).attr("data-source", container.source);
				return container.text;
			}
		});
	};

	return {
		init: function () {
			Select2Clasic();

			Select2Remote('select2remote_city', site_url + 'json_general/select2_city/');
			Select2Remote('select2remote_district', site_url + 'json_general/select2_district/');
			Select2Remote('select2remote_postal', site_url + 'json_general/select2_postal/');
			Select2Remote('select2remote_parties', site_url + 'json_general/select2_parties/');
			Select2Remote('select2remote_tender', site_url  + 'json_general/select2_tender/');
			Select2Remote('select2remote_loa', site_url  + 'json_general/select2_loa/');
			Select2Remote('select2remote_supplier', site_url  + 'json_general/select2_supplier/');
			Select2Remote('select2remote_item_code', site_url  + 'json_general/select2_item_code/');

		}
	};

}();

jQuery(document).ready(function () {
	JsonGeneralSelect2.init();
});



