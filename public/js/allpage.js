$.ajaxSetup({

    headers: {

        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')

    }

});


$( document ).ready(function() {
    getcat();
});

function getcat() {
    if($('#category').is(':visible')){
        $.get( site_url + "/category/" + $('#store_id').val(), function( data ) {
            // console.log(data);
            $('#category').html(data.cat);
            $('#cat_prod').html(data.cat_prod);
            $('#category_mobile').html(data.cat);
        }, 'json');
    }
}

function receiveMessage(event) {

    // event.source is window.opener
    // event.data is "hello there!"

    // Assuming you've verified the origin of the received message (which
    // you must do in any case), a convenient idiom for replying to a
    // message is to call postMessage on event.source and provide
    // event.origin as the targetOrigin.
    // event.source.postMessage("hi there yourself!  the secret response " +
    //     "is: rheeeeet!",
    //     event.origin);

    // console.log(event.data);
}


function confirm_login(params) {
    if (params.is(':checked')) {
        $('#next_btn').removeAttr('disabled');
    }else{
        $('#next_btn').attr('disabled', 'yes');
    }
}

function confirm_login_guest(params) {
    if (params.is(':checked')) {
        $('#next_btn_guest').removeAttr('disabled');
    }else{
        $('#next_btn_guest').attr('disabled', 'yes');
    }
}

var map = null;
var markers = [];
var countries = [];

function focusmaps(params) {
    // console.log(params.find(":selected").text());
    var geocoder = new google.maps.Geocoder();
    geocoder.geocode({address: params.val()}, function(results, status) {
        if (status === "OK") {
            for (let i = 0; i < results.length; i++) {
                // console.log(results[i].formatted_address);
                tempres = results[i].formatted_address;
                countries.push(tempres);
            }
            // console.log(results);
            map.setCenter(results[0].geometry.location);
        } else {
            // swal({
            //     type: 'error',
            //     title: 'Maps Pointer Not Found'
            // });
            console.log("Maps Pointer Not Found");
        }
    });
    countries = countries.filter( onlyUnique ); // returns ['a', 1, 2, '1']
    // console.log(countries);
}

function focusmaps_select(params) {
    // console.log(params.find(":selected").text());
    var geocoder = new google.maps.Geocoder();
    geocoder.geocode({address: params.find(":selected").text()}, function(results, status) {
        if (status === "OK") {
            for (let i = 0; i < results.length; i++) {
                console.log(results[i].formatted_address);
                tempres = results[i].formatted_address;
                countries.push(tempres);
            }
            console.log(results);
            map.setCenter(results[0].geometry.location);
        } else {
            // swal({
            //     type: 'error',
            //     title: 'Maps Pointer Not Found'
            // });
            console.log("Maps Pointer Not Found");
        }
    });
    countries = countries.filter( onlyUnique ); // returns ['a', 1, 2, '1']
    // console.log(countries);
    var options = {
        data: countries,
        list: {
            maxNumberOfElements: 1,
            match: {
                enabled: true
            }
        }
    };
    
    $("#addrs").easyAutocomplete(options);
}

function onlyUnique(value, index, self) { 
    return self.indexOf(value) === index;
}

function complete(params) {
    var tempres = '';
    var geocoder = new google.maps.Geocoder();
    geocoder.geocode({address: params}, function(results, status) {
        if (status === "OK") {
            // console.log(results);
            for (let i = 0; i < results.length; i++) {
                // console.log(results[i].formatted_address);
                tempres = results[i].formatted_address;
                countries.push(tempres);
            }
            map.setCenter(results[0].geometry.location);
        } else {
            // swal({
            //     type: 'error',
            //     title: 'Maps Pointer Not Found'
            // });
            console.log("Maps Pointer Not Found");
        }
    });
}

function geocodeAddress(geocoder) {
    var address = document.getElementById("addrs").value;
    // console.log(address);
    // return false;
    geocoder.geocode({address: address}, function(results, status) {
        if (status === "OK") {
            map.setCenter(results[0].geometry.location);
            // console.log(results[0].geometry.location);
            addMarker(results[0].geometry.location);
            var geo = results[0].geometry.location.toString();
            geo = geo.substring(1, geo.length - 1);
            $('#lonlat').val(geo);
            // console.log(geo);
            var arrgeo = geo.split(",");
            $('#lat').val(parseFloat(arrgeo[0]));
            $('#lon').val(parseFloat(arrgeo[1]));
        } else {
            swal({
                type: 'error',
                title: 'Maps Pointer Not Found'
            });
        }
    });
}
function zero(params) {
    // console.log(params.val());
    var vals = params.val();
    var arrsplit = vals.split("");
    // console.log(arrsplit);

    if (vals.length == 1 && vals == 0) {
        params.val("62");
    } else if(arrsplit[0] == 0){
        var temp = '';
        for (let i = 0; i < arrsplit.length; i++) {
            if (i == 0 && arrsplit[0] == 0) {
                temp += "62";   
            }else{
                temp += arrsplit[i];
            }
        }
        // console.log(temp);
        params.val(temp);
    }
    
}

// window.addEventListener("message", receiveMessage, false);

// //message sender
// document.getElementById('send').addEventListener("click", function (e) {
//     var message = "Hai KAKA CAKEP!";
//     window.postMessage("hi there yourself!  the secret response " +
//         "is: rheeeeet!",
//         event.origin);
// }, false);

function minus_qty() {
    var qty = $('#qty').val();
    if (qty > 0) {
        $('#qty').val(parseInt(qty) - 1);
    }
}

function plus_qty() {
    var qty = $('#qty').val();

    $('#qty').val(parseInt(qty) + 1);
}

function quantity_modal(par, store, prod) {
    $('#quantity_store').val(store);
    $('#quantity_prod').val(prod);
    
    var price = '<span> Rp. ' + par.attr('prod_price') + ' </span>';

    var img_tag = '<a href="#"><img src="' + par.attr('prod_img') + '" alt=""></a>';
    // console.log(par.attr('prod_img'));
    $('#quantity_img').html(img_tag);

    $('#img').val(par.attr('prod_img'));
    $('#quantity_price').html(price);
    $('#grand_total').html(price);
    $('#quantity_name').html(par.attr('prod_name'));
    $('#name').val(par.attr('prod_name'));
    $('#quantity_max').val(par.attr('prod_quant'));

    $('#price').val(par.attr('prod_price'));
    $('#hidden_grand_total').val(par.attr('total'));
    var htmlbblm = $('#bblm_' + prod).html();
    // console.log(htmlbblm);
    $('#for_bblm').html(htmlbblm);

    $('#modal-quantity').modal('show');
    // console.log(img);
    // return false;
}

function set_total(par) {
    var quant = par.val();
    var price = $('#quantity_price').text();
    price = price.substring(5);
    price = price.replace(",", "");
    var total = price * quant;
    total = numeral(total).format('0,0')
    if (quant == '' || quant == null || quant == 0) {
        $('#grand_total').html('Rp. ' + 0);
        $('#hidden_grand_total').val(0);
    }else{
        $('#grand_total').html('Rp. '+ total);
        $('#hidden_grand_total').val(total);
    }
    // console.log(total);
    // console.log(final);
    // return false;

}

function post_data_public(par, form_name) {
    // console.log(form_name);
    // return false;

    var form_table = new FormData(form_name);
    var url = $(form_name).attr('action');
    
    $.ajax({
        type: "POST",
        url: url,
        data: form_table,
        dataType: 'json',
        processData: false,
        contentType: false,
        beforeSend: function () {
            swal({
                imageUrl: site_url + '/img/pacman.gif',
                imageHeight: 1500,
                title: 'Please Wait',
                showCancelButton: true,
                showConfirmButton: false,
                text: 'We Process Your Request',
            },
                function (isConfirm) {
                    if (isConfirm) {
                        xhr.abort();
                    }
                });
        },
        error: function () {
            swal({
                type: 'error',
                title: 'Cek Nomor Kartu Gagal',
                text: 'Terjadi Kesalahan Pada Sitem',
            });
        },
        success: function (data) {
            // console.log(data);
            // return false;
            if (data.res) {
                swal({
                    type: 'success',
                    title: data.msg,
                    text: 'Please Wait',
                });
                $('.modal').modal('hide');
                if (data.url != '') {
                    setTimeout(function () {
                        location.href = data.url;
                    }, 2000);   
                }
                return false;
            }else{
                swal({
                    type: 'error',
                    title: data.msg,
                });
            }
        }
    });
}

function add_to_cart(form_name) {
    // console.log(data);
    // return false;

    var temp = '';
    var form_table = new FormData(form_name);
    var url = $(form_name).attr('action');

    $.ajax({
        type: "POST",
        url: url,
        data: form_table,
        dataType: 'json',
        processData: false,
        contentType: false,
        beforeSend: function () {
            swal({
                    imageUrl: site_url + '/img/pacman.gif',
                    imageHeight: 1500,
                    title: 'Please Wait',
                    showCancelButton: true,
                    showConfirmButton: false,
                    text: 'We Process Your Request',
                },
                function (isConfirm) {
                    if (isConfirm) {
                        xhr.abort();
                    }
                });
        },
        error: function () {
            swal({
                type: 'error',
                title: 'Ooops! Something Wrong in Here',
                text: 'Silahkan Refresh Halaman Dan Coba Kembali',
            });
        },
        success: function (ret) {
            // console.log(ret);
            // return false;

            if (!ret.res) {
                swal({
                    type: 'error',
                    title: data.msg,
                    text: 'Silahkan Refresh Halaman Dan Coba Kembali',
                });
                return false;
            }
            
            var data = $(form_table).serializeArray();
            // console.log(data);
            // return false;

            $('.product_head').append(ret.datacart.htmlprod);
            $('#minidiv').show();
            $('#modal-quantity').modal('toggle');
            var sum_cart = $('#sum_cart').val();
            sum_cart = parseInt(sum_cart) + 1;

            var html_note = '<span class="smallword-cart">' + sum_cart + '</span>';
            $('.cartmuch').html(html_note);
            $('.cartmuch2').html(sum_cart + ' Items');
            $('.carttotal').html(ret.datacart.total_amount_cart);
            $('#sum_cart').val(ret.datacart.total_item);
            $('#qty').val(1);
            $('#cart_box').show();

            swal({
                title: 'Thank You',
                text: 'We Added To Your Bag!',
                type: 'success',
                showConfirmButton: true
            });

        }
    });
    
}

// function show_cart() {
//     // console.log("okekeoekoeko");
//     if ($('#cart_box').hasClass('is-visible')) {
//         $('#cart_box').removeClass('is-visible');
//     } else {
//         $('#cart_box').show();
//         $('#cart_box').addClass('is-visible');
//     }
// }

function show_cart() {
    // console.log("okekeoekoeko");
    if ($('#cart_box').hasClass('is-visible')) {
        $('#cart_box').removeClass('is-visible');
    } else {
        $.ajax({
            type: "GET",
            url: site_url + '/cart/get_cart',
            dataType: 'json',
            beforeSend: function () {
                swal({
                        imageUrl: site_url + '/img/pacman.gif',
                        imageHeight: 1500,
                        title: 'Please Wait',
                        showCancelButton: true,
                        showConfirmButton: false,
                        text: 'We Process Your Request',
                    },
                    function (isConfirm) {
                        if (isConfirm) {
                            xhr.abort();
                        }
                    });
            },
            error: function () {
                swal({
                    type: 'error',
                    title: 'Ooops! Something Wrong in Here',
                    text: 'Silahkan Refresh Halaman Dan Coba Kembali',
                });
            },
            success: function (data) {
                // console.log(data);
                // return false;
                if (data.res) {
                    $('.product_head').html(data.datacart.htmlprod);

                    var html_note = '<span class="smallword-cart">' + data.datacart.total_item + '</span>';
                    $('.cartmuch').html(html_note);
                    $('.cartmuch2').html(data.datacart.total_item + ' Items');
                    $('.carttotal').html(data.datacart.total_amount_cart_num);
                    $('#sum_cart').val(data.datacart.total_item);
                    $('#qty').val(1);
                    $('#cart_box').show();
                    $('.checkout__btn').attr('href', site_url + '/cart/checkout/' + data.datacart.store_id);
                    $('.cart__btn').attr('href', site_url + '/cart/index/' + data.datacart.store_id);
                    $('#minidiv').show();
                    $('#cart_box').show();
                    $('#cart_box').addClass('is-visible');
                    swal.close();
                }else{
                    // swal({
                    //     type: 'error',
                    //     title: data.msg,
                    //     text: 'Silahkan Refresh Halaman Dan Coba Kembali',
                    // });
                    $('#cart_box').show();
                    $('#cart_box').addClass('is-visible');
                    swal.close();
                }
            }
        });
    }
}

function show_cart_mobile() {
    // console.log("okekeoekoeko");
    if ($('#cart_box_mobile').hasClass('is-visible')) {
        $('#cart_box_mobile').removeClass('is-visible');
    } else {
        $.ajax({
            type: "GET",
            url: site_url + '/cart/get_cart',
            dataType: 'json',
            beforeSend: function () {
                swal({
                        imageUrl: site_url + '/img/pacman.gif',
                        imageHeight: 1500,
                        title: 'Please Wait',
                        showCancelButton: true,
                        showConfirmButton: false,
                        text: 'We Process Your Request',
                    },
                    function (isConfirm) {
                        if (isConfirm) {
                            xhr.abort();
                        }
                    });
            },
            error: function () {
                swal({
                    type: 'error',
                    title: 'Ooops! Something Wrong in Here',
                    text: 'Silahkan Refresh Halaman Dan Coba Kembali',
                });
            },
            success: function (data) {
                // console.log(data);
                // return false;
                if (data.res) {
                    $('.product_head_mobile').html(data.datacart.htmlprod);

                    var html_note = '<span class="smallword-cart">' + data.datacart.total_item + '</span>';
                    $('.cartmuch_mobile').html(html_note);
                    $('.cartmuch2_mobile').html(data.datacart.total_item + ' Items');
                    $('.carttotal_mobile').html(data.datacart.total_amount_cart_num);
                    $('#sum_cart_mobile').val(data.datacart.total_item);
                    $('#qty').val(1);
                    $('#cart_box_mobile').show();
                    $('.checkout__btn').attr('href', site_url + '/cart/checkout/' + data.datacart.store_id);
                    $('.cart__btn').attr('href', site_url + '/cart/index/' + data.datacart.store_id);
                    $('#minidiv_mobile').show();
                    $('#cart_box_mobile').show();
                    $('#cart_box_mobile').addClass('is-visible');
                    swal.close();
                }else{
                    // swal({
                    //     type: 'error',
                    //     title: data.msg,
                    //     text: 'Silahkan Refresh Halaman Dan Coba Kembali',
                    // });
                    $('#cart_box_mobile').show();
                    $('#cart_box_mobile').addClass('is-visible');
                    swal.close();
                }
            }
        });
    }
}

function show_address() {
    // console.log("okekeoekoeko");
    if (!$('#address_box').hasClass('is-visible')) {
        // console.log(params.val());
        // return false;
        $.ajax({
            type: "GET",
            url: site_url + '/login/get_address',
            dataType: 'json',
            beforeSend: function () {
                swal({
                        imageUrl: site_url + '/img/pacman.gif',
                        imageHeight: 1500,
                        title: 'Please Wait',
                        showCancelButton: true,
                        showConfirmButton: false,
                        text: 'We Process Your Request',
                    },
                    function (isConfirm) {
                        if (isConfirm) {
                            xhr.abort();
                        }
                    });
            },
            error: function () {
                swal({
                    type: 'error',
                    title: 'Ooops! Something Wrong in Here',
                    text: 'Silahkan Refresh Halaman Dan Coba Kembali',
                });
            },
            success: function (data) {
                // console.log(data.htmlregen);
                // return false;
                if (data.res) {
                    swal.close();
                    $('.address_head').html(data.htmlregen);
                    $('#address_box').addClass('is-visible');
                } else {
                    swal({
                        type: 'error',
                        title: data.msg,
                        text: 'Silahkan Refresh Halaman Dan Coba Kembali',
                    });
                    $('.address_head').html('Address Not Found');
                    $('#address_box').addClass('is-visible')
                }
            }
        });
    } else {
        $('#address_box').removeClass('is-visible');
    }
}

function show_address_mobile() {
    // console.log("okekeoekoeko");
    if (!$('#address_box_mobile').hasClass('is-visible')) {
        // console.log(params.val());
        // return false;
        $.ajax({
            type: "GET",
            url: site_url + '/login/get_address',
            dataType: 'json',
            beforeSend: function () {
                swal({
                        imageUrl: site_url + '/img/pacman.gif',
                        imageHeight: 1500,
                        title: 'Please Wait',
                        showCancelButton: true,
                        showConfirmButton: false,
                        text: 'We Process Your Request',
                    },
                    function (isConfirm) {
                        if (isConfirm) {
                            xhr.abort();
                        }
                    });
            },
            error: function () {
                swal({
                    type: 'error',
                    title: 'Ooops! Something Wrong in Here',
                    text: 'Silahkan Refresh Halaman Dan Coba Kembali',
                });
            },
            success: function (data) {
                // console.log(data.htmlregen);
                // return false;
                if (data.res) {
                    swal.close();
                    $('.address_head_mobile').html(data.htmlregen);
                    $('#address_box_mobile').addClass('is-visible');
                } else {
                    swal({
                        type: 'error',
                        title: data.msg,
                        text: 'Silahkan Refresh Halaman Dan Coba Kembali',
                    });
                    $('.address_head_mobile').html('Address Not Found');
                    $('#address_box_mobile').addClass('is-visible')
                }
            }
        });
    } else {
        $('#address_box_mobile').removeClass('is-visible');
    }
}

function show_search(params) {
    // console.log(params.val());
    // return false;
    if ($('#search_box').hasClass('is-visible')) {
        $('#search_box').removeClass('is-visible');
    } else {
        $('#search_box').show();
        $('#search_box').addClass('is-visible');
    }
}

function search_submit(){
    swal({
        imageUrl: site_url + '/img/pacman.gif',
        imageHeight: 1500,
        title: 'Please Wait',
        showCancelButton: true,
        showConfirmButton: false,
        text: 'We Process Your Request',
    },
    function (isConfirm) {
        if (isConfirm) {
            xhr.abort();
        }
    });
    
    var n = $('#search').val().search("/");
    var search = $('#search').val();
    if (n > 0) {
        search = $('#search').val().slice(0, n);   
    }
    window.location.replace(site_url + '/product/search/' + $('#store_id').val() + '/' + search);
}

function search_enter(ele) {
    if(event.key === 'Enter') {
        var search = $("#comp_search").val();
        $('#search').val(search);
        search_submit();        
    }
}

function setMapOnAll(map) {
    for (var i = 0; i < markers.length; i++) {
        markers[i].setMap(map);
    }
}

function addMarker(location) {
    var marker = new google.maps.Marker({
        position: location,
        map: map
    });
    markers.push(marker);
}

function show_addrs(params, flag) {
    // console.log(params.val());
    // return false;
    $('#modal-change-addrs').modal('hide')
    $.ajax({
        type: "GET",
        url: params.attr('data-url') + '/' + params.attr('data-id') + '/' + flag,
        beforeSend: function () {
            swal({
                    imageUrl: site_url + '/img/pacman.gif',
                    imageHeight: 1500,
                    title: 'Please Wait',
                    showCancelButton: true,
                    showConfirmButton: false,
                    text: 'We Process Your Request',
                },
                function (isConfirm) {
                    if (isConfirm) {
                        xhr.abort();
                    }
                });
        },
        error: function () {
            swal({
                type: 'error',
                title: 'Ooops! Something Wrong in Here',
                text: 'Silahkan Refresh Halaman Dan Coba Kembali',
            });
        },
        success: function (data) {
            // console.log(data);
            // return false;
            if (data) {
                swal.close();
                $('#cont_addrs').html(data);
                $('#modal-addrs').modal('show');
                // show_static_map($('#lat').val(), $('#lon').val());
                var lat = $('#lat').val();
                var lon = $('#lon').val();
                if (lat != '' && lon != '') {
                    map = new google.maps.Map(document.getElementById("map"), 
                    {
                        zoom: 15,
                        center: { lat: parseFloat(lat), lng: parseFloat(lon) }
                    });
                    // console.log(map.center);
                    // return false;
                    addMarker(map.center);
                }else{
                    map = new google.maps.Map(document.getElementById("map"), 
                    {
                        zoom: 15,
                        center: { lat: -6.208428, lng: 106.7824432 }
                    });
                }
                
                var geocoder = new google.maps.Geocoder();
                // document.getElementById("btn-lat").addEventListener("click", function() {
                //     geocodeAddress(geocoder, map);
                // });

                map.addListener('click', function(mapsMouseEvent) {
                    setMapOnAll(null);

                    addMarker(mapsMouseEvent.latLng);
                    var geo = mapsMouseEvent.latLng.toString();
                    geo = geo.substring(1, geo.length - 1);
                    $('#lonlat').val(geo);
                    // console.log(geo);
                    var arrgeo = geo.split(",");
                    $('#lat').val(parseFloat(arrgeo[0]));
                    $('#lon').val(parseFloat(arrgeo[1]));
                });

            } else {
                swal({
                    type: 'error',
                    title: data.msg,
                    text: 'Silahkan Refresh Halaman Dan Coba Kembali',
                });
            }
        }
    });
}

function load_payment_modal(params) {
    var url = params.attr('data-url');
    var popUp = '';
    // $('#modal-body-payment').html('<button type="button" class="close" onclick="reloadpage()" data-dismiss="modal">&times;</button><section class="bg--white"><iframe style="border: 0px; width:100%; height: 700px;" src="' + url + '"></iframe></div>');
    // $('#modal-payment').modal('show');

    popUp = window.open(url, '');
}

function goBack() {
    window.history.back();
}

function reloadpage() {
    swal({
            imageUrl: site_url + '/img/pacman.gif',
            imageHeight: 1500,
            title: 'Please Wait',
            showCancelButton: true,
            showConfirmButton: false,
            text: 'We Process Your Request',
        },
        function (isConfirm) {
            if (isConfirm) {
                xhr.abort();
            }
        });
    location.reload();
}

function back_to_home() {
    swal({
        type: 'success',
        title: 'Checkout Has Success',
        text: 'Take A Look History To Know Your Status',
    });
    location.href = site_url + '/store/home';
}

function delete_address(params) {
    // console.log(params.val());
    // return false;
    $.ajax({
        type: "GET",
        url: site_url + '/login/delete_address/' + params.attr('data-id'),
        beforeSend: function () {
            swal({
                    imageUrl: site_url + '/img/pacman.gif',
                    imageHeight: 1500,
                    title: 'Please Wait',
                    showCancelButton: true,
                    showConfirmButton: false,
                    text: 'We Process Your Request',
                },
                function (isConfirm) {
                    if (isConfirm) {
                        xhr.abort();
                    }
                });
        },
        error: function () {
            swal({
                type: 'error',
                title: 'Ooops! Something Wrong in Here',
                text: 'Silahkan Refresh Halaman Dan Coba Kembali',
            });
        },
        success: function (data) {
            // console.log(data);
            // return false;
            if (data) {
                $('.modal').modal('hide');
                swal({
                    title: 'Delete Address Success',
                    type: 'success',
                    showConfirmButton: true
                });
                setTimeout(function () {
                    location.reload();
                }, 2000);
            } else {
                swal({
                    type: 'error',
                    title: 'Delete Address Failed',
                    text: 'Silahkan Refresh Halaman Dan Coba Kembali',
                });
            }
        }
    });
}

function add_to_cart_again(form_name, params) {
    var form_table = new FormData(form_name);
    var url = params.attr('data-url');

    $.ajax({
        type: "POST",
        url: url,
        data: form_table,
        dataType: 'json',
        processData: false,
        contentType: false,
        beforeSend: function () {
            swal({
                imageUrl: site_url + '/img/pacman.gif',
                imageHeight: 1500,
                title: 'Please Wait',
                showCancelButton: true,
                showConfirmButton: false,
                text: 'We Process Your Request',
            },
                function (isConfirm) {
                    if (isConfirm) {
                        xhr.abort();
                    }
                });
        },
        error: function () {
            swal({
                type: 'error',
                title: 'Ooops! Something Wrong in Here',
                text: 'Silahkan Refresh Halaman Dan Coba Kembali',
            });
        },
        success: function (data) {
            // console.log(data);
            // return false;
            if (data.res) {
                $('.modal').modal('hide');
                swal({
                    title: data.msg,
                    type: 'success',
                    showConfirmButton: true
                });
                setTimeout(function () {
                    location.reload();
                }, 2000);
            } else {
                swal({
                    type: 'error',
                    title: data.msg,
                    text: 'Silahkan Refresh Halaman Dan Coba Kembali',
                });
            }
        }
    });
}

function makedefault(params) {
    // console.log(params.val());
    // return false;
    $.ajax({
        type: "GET",
        url: site_url + '/login/makedefault/' + params.attr('data-id'),
        dataType: 'json',
        beforeSend: function () {
            swal({
                    imageUrl: site_url + '/img/pacman.gif',
                    imageHeight: 1500,
                    title: 'Please Wait',
                    showCancelButton: true,
                    showConfirmButton: false,
                    text: 'We Process Your Request',
                },
                function (isConfirm) {
                    if (isConfirm) {
                        xhr.abort();
                    }
                });
        },
        error: function () {
            swal({
                type: 'error',
                title: 'Ooops! Something Wrong in Here',
                text: 'Silahkan Refresh Halaman Dan Coba Kembali',
            });
        },
        success: function (data) {
            // console.log(data);
            // return false;
            if (data) {
                $('.modal').modal('hide');
                swal({
                    title: data.msg,
                    type: 'success',
                    showConfirmButton: true
                });
                setTimeout(function () {
                    location.reload();
                }, 5000);
            } else {
                swal({
                    type: 'error',
                    title: data.msg,
                    text: 'Silahkan Refresh Halaman Dan Coba Kembali',
                });
            }
        }
    });
}

function setstatus(params) {
    // console.log("okekeoekoeko");
    $.ajax({
        type: "GET",
        url: params.attr('data-url'),
        dataType: 'json',
        beforeSend: function () {
            swal({
                    imageUrl: site_url + '/img/pacman.gif',
                    imageHeight: 1500,
                    title: 'Please Wait',
                    showCancelButton: true,
                    showConfirmButton: false,
                    text: 'We Process Your Request',
                },
                function (isConfirm) {
                    if (isConfirm) {
                        xhr.abort();
                    }
                });
        },
        error: function () {
            swal({
                type: 'error',
                title: 'Ooops! Something Wrong in Here',
                text: 'Silahkan Refresh Halaman Dan Coba Kembali',
            });
        },
        success: function (data) {
            // console.log(data);
            // return false;
            if (data.res) {
                swal({
                    title: data.msg,
                    type: 'success',
                    showConfirmButton: true
                });
                setTimeout(function () {
                    location.reload();
                }, 2000);
            } else {
                swal({
                    type: 'error',
                    title: data.msg,
                    text: 'Silahkan Refresh Halaman Dan Coba Kembali',
                });
            }
        }
    });
}

function get_regency(params) {
    // console.log(params.val());
    // return false;
    $.ajax({
        type: "GET",
        url: site_url + '/login/get_regency/' + params.val(),
        dataType: 'json',
        beforeSend: function () {
            swal({
                    imageUrl: site_url + '/img/pacman.gif',
                    imageHeight: 1500,
                    title: 'Please Wait',
                    showCancelButton: true,
                    showConfirmButton: false,
                    text: 'We Process Your Request',
                },
                function (isConfirm) {
                    if (isConfirm) {
                        xhr.abort();
                    }
                });
        },
        error: function () {
            swal({
                type: 'error',
                title: 'Ooops! Something Wrong in Here',
                text: 'Silahkan Refresh Halaman Dan Coba Kembali',
            });
        },
        success: function (data) {
            // console.log(data);
            // return false;
            if (data.res) {
                swal.close();
                $('#regency_id').html(data.htmlregen);
            } else {
                swal({
                    type: 'error',
                    title: data.msg,
                    text: 'Silahkan Refresh Halaman Dan Coba Kembali',
                });
            }
        }
    });
}