function showmodal(params) {
    var url = params.attr('data-url');
    // add this for post method
    // headers: {
    //     'X-CSRF-TOKEN': token
    // },
    // var token = $('[name="_token"]').val();
    // console.log(token);

    $.ajax({
        type: "GET",
        url: url,
        dataType: 'json',
        processData: false,
        contentType: false,
        beforeSend: function () {
            swal({
                    imageUrl: '../img/pacman.gif',
                    imageHeight: 1500,
                    title: 'Please Wait',
                    showCancelButton: true,
                    showConfirmButton: false,
                    text: 'We Process Your Request',
                },
                function (isConfirm) {
                    if (isConfirm) {
                        xhr.abort();
                    }
                });
        },
        error: function () {
            swal({
                type: 'error',
                title: 'Ooops! Something Wrong in Here',
                text: 'Silahkan Refresh Halaman Dan Coba Kembali',
            });
        },
        success: function (data) {
            // console.log(data);
            // return false;
            $('#modal-default').html(data.temp);
            $('#modal-default').modal('show');
            swal.close();
        }
    });
}