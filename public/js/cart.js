$.ajaxSetup({

    headers: {

        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')

    }

});

$( ".set" ).click(function() {
    console.log("masuk");
    back_to_home_cart();
});

// $(document).ready(function () {
//     window.addEventListener("message", function (event) {

//         // alert("received: " + event.data);
//         console.log(event);

//         // can message back using event.source.postMessage(...)
//     });
// });

$("form").submit(function (event) {
    var shipp_method = $('#shipp_method').val();

    if (shipp_method == 2) {
        var expidition = $('#expidition').val();
        var durasi = $('#durasi:checked').val();
        // console.log(durasi);
        // return false;
        if (expidition == 0 || expidition == '' || expidition == null) {
            swal({
                type: 'warning',
                title: 'Mohon Memilih Kurir Untuk Mengirim Belanjaan Anda',
                text: 'Anda Belum Memilih Kurir Untuk Mengirim Belanjaan Anda'
            });
            event.preventDefault();
        }

        if (durasi == 0 || durasi == '' || durasi == null) {
            swal({
                type: 'warning',
                title: 'Mohon Memilih Durasi Pengiriman',
                text: 'Anda Belum Memilih Durasi Pengiriman Untuk Mengirim Belanjaan Anda'
            });
            event.preventDefault();
        }

    } else if (shipp_method == 0 || shipp_method == '' || shipp_method == null) {
        swal({
            type: 'warning',
            title: 'Mohon Memilih Metode Pengiriman',
            text: 'Anda Belum Memilih Metode Pengiriman'
        });
        event.preventDefault();
    }else{
        return;
    }
    
});

$("#check_all").click(function () {
    $('input:checkbox').not(this).prop('checked', this.checked);
});

function get_text_payment(params) {
    var txt = params.find('option:selected').text();
    // console.log(txt);
    $('#payment_method_text').val(txt);
}

function preview_cart(form_name) {
    var form_table = new FormData(form_name);
    var url = $(form_name).attr('action');
    form_table.append('flag', 1);

    $.ajax({
        type: "POST",
        url: url,
        data: form_table,
        dataType: 'json',
        processData: false,
        contentType: false,
        beforeSend: function () {
            swal({
                imageUrl: site_url + '/img/pacman.gif',
                imageHeight: 1500,
                title: 'Please Wait',
                showCancelButton: true,
                showConfirmButton: false,
                text: 'We Process Your Request',
            },
                function (isConfirm) {
                    if (isConfirm) {
                        xhr.abort();
                    }
                });
        },
        error: function () {
            swal({
                type: 'error',
                title: 'Ooops! Something Wrong in Here',
                text: 'Silahkan Refresh Halaman Dan Coba Kembali',
            });
        },
        success: function (data) {
            console.log(data);
            // return false;
            if (data.length > 0) {
                for (let i = 0; i < data.length; i++) {
                    if (data[i].is_bblm && data[i].bblm_price != 0) {
                        $('#total_cart_' + data[i].product_id).val(data[i].bblm_total_price);
                        $('#price_txt_' + data[i].product_id).html('Rp. ' + numeral(data[i].bblm_price).format('0,0'));  
                    }else{
                        $('#total_cart_' + data[i].product_id).val(data[i].total_price);
                        $('#price_txt_' + data[i].product_id).html('Rp. ' + numeral(data[i].base_price).format('0,0'));  
                    }
                }
                count_all();
                swal.close();
            }
        }
    });
}

function plus_qty_cart(par, params, form_name) {
    var before = $('#' + params).val();
    var now = parseInt(before) + 1;
    $('#' + params).val(now);
    preview_cart(form_name);
}

function minus_qty_cart(par, params, form_name) {
    var before = $('#' + params).val();
    var now = parseInt(before) - 1;
    $('#' + params).val(now);
    preview_cart(form_name);
}

function edit_notes(params) {
    var store_id = $(params).attr('data-store');
    var product_id = $(params).attr('data-product');
    var notes = $('#notes_' + product_id).val();
    var qty = $('#qty_' + product_id).val();
    // console.log(store_id);
    // console.log(product_id);
    // console.log(notes);

    $('#store_modal').val(store_id);
    $('#product_modal').val(product_id);
    $('#qty_modal').val(qty);
    $('#note_modal').val(notes);

    $('#modal-checkout').modal('show');
}

function final_preview(params, form_name) {
    var form_table = new FormData(form_name);
    var url = params.attr('data-url');

    $.ajax({
        type: "POST",
        url: url,
        data: form_table,
        dataType: 'json',
        processData: false,
        contentType: false,
        beforeSend: function () {
            swal({
                    imageUrl: site_url + '/img/pacman.gif',
                    imageHeight: 1500,
                    title: 'Please Wait',
                    showCancelButton: true,
                    showConfirmButton: false,
                    text: 'We Process Your Request',
                },
                function (isConfirm) {
                    if (isConfirm) {
                        xhr.abort();
                    }
                });
        },
        error: function () {
            swal({
                type: 'error',
                title: 'Ooops! Something Wrong in Here',
                text: 'Silahkan Refresh Halaman Dan Coba Kembali',
            });
        },
        success: function (data) {
            // console.log(data);
            // return false;
            if (data.res) {
                $('#total_amount_delivery').html(numeral($('#real_delivery').val()).format('0,0'));
                $('#biaya_layanan').html(numeral(data.arr['service_fee']).format('0,0'));
                $('#admin_bank').html(numeral(data.arr['payment_service_fee']).format('0,0'));
                $('#disc_biaya_layanan').html('-' + numeral(data.arr['minusBiayaLayanan']).format('0,0'));
                $('#total_amount_cart_txt').html(numeral(data.arr['total_price']).format('0,0'));
                $('#disc_admin_bank').html('-' + numeral(data.arr['minusPaymentServiceFee']).format('0,0'));
                $('#total_amount_delivery_disc').html('-' + numeral($('#total_diskon').val()).format('0,0'));
                swal.close();

            } else {
                swal({
                    type: 'error',
                    title: data.msg,
                    text: 'Silahkan Refresh Halaman Dan Coba Kembali',
                });
            }
        }
    });
}

function set_post_cart(form_name) {
    var form_table = new FormData(form_name);
    var url = $(form_name).attr('action');
    form_table.append('flag', 0);

    $.ajax({
        type: "POST",
        url: url,
        data: form_table,
        dataType: 'json',
        processData: false,
        contentType: false,
        beforeSend: function () {
            swal({
                imageUrl: site_url + '/img/pacman.gif',
                imageHeight: 1500,
                title: 'Please Wait',
                showCancelButton: true,
                showConfirmButton: false,
                text: 'We Process Your Request',
            },
                function (isConfirm) {
                    if (isConfirm) {
                        xhr.abort();
                    }
                });
        },
        error: function () {
            swal({
                type: 'error',
                title: 'Ooops! Something Wrong in Here',
                text: 'Silahkan Refresh Halaman Dan Coba Kembali',
            });
        },
        success: function (data) {
            // console.log(data);
            // return false;
            if (data.res) {
                swal({
                    title: data.msg,
                    type: 'success',
                    showConfirmButton: true
                });
                setTimeout(function () {
                    location.href = data.url;
                }, 2000);
            } else {
                swal({
                    type: 'error',
                    title: data.msg,
                    text: 'Silahkan Refresh Halaman Dan Coba Kembali',
                });
            }
        }
    });
}

function set_checkout(form_name) {
    if ($('#payment_method').val() != 0 && $('#payment_method').val() != '' && $('#payment_method').val() != null) {

        var form_table = new FormData(form_name);
        var url = $(form_name).attr('action');

        $.ajax({
            type: "POST",
            url: url,
            data: form_table,
            dataType: 'json',
            processData: false,
            contentType: false,
            beforeSend: function () {
                swal({
                        imageUrl: site_url + '/img/pacman.gif',
                        imageHeight: 1500,
                        title: 'Please Wait',
                        showCancelButton: true,
                        showConfirmButton: false,
                        text: 'We Process Your Request',
                    },
                    function (isConfirm) {
                        if (isConfirm) {
                            xhr.abort();
                        }
                    });
            },
            error: function () {
                swal({
                    type: 'error',
                    title: 'Ooops! Something Wrong in Here',
                    text: 'Silahkan Refresh Halaman Dan Coba Kembali',
                });
            },
            success: function (data) {
                // console.log(data);
                // return false;
                if (data.res) {
                    // $('#modal-body-payment').html('<button type="button" onclick="back_to_home_cart()" class="close" data-dismiss="modal">&times;</button><section class="bg--white"><iframe style="border: 0px; width:100%; height: 700px;" src="' + data.url.data + '"></iframe></div>');
                    // $('#modal-payment').modal('show');
                    $('#transaction_id').val(data.transaction_id);
                    swal({
                        type: 'success',
                        title: 'Checkout Berhasil',
                        text: 'Silahkan Cek History Untuk Status Pembayaran',
                    }, function() {
                        window.open(data.url.data, '_blank');
                        location.href = site_url + '/history/detail/' + $('#store_id').val() + '/' + $('#transaction_id').val();
                    });
                    // back_to_home_cart();
                } else {
                    swal({
                        type: 'error',
                        title: data.msg,
                        text: 'Silahkan Refresh Halaman Dan Coba Kembali',
                    });
                }
            }
        });
    }else{
        swal({
            type: 'warning',
            title: 'Silahkan Pilih Metode Pembayaran Terlebih Dahulu'
        });
        return false;
    }
}

function open_promo() {
    $('#code_txt').val('');
    $('#voucher-code').html('');
    $('#modal-promo-body').show();
    $('#modal-promo-detail').hide();
    $.ajax({
        type: "GET",
        url: site_url + '/cart/list_voucher/' + $('#store_id').val(),
        dataType: 'json',
        beforeSend: function () {
            swal({
                    imageUrl: site_url + '/img/pacman.gif',
                    imageHeight: 1500,
                    title: 'Please Wait',
                    showCancelButton: true,
                    showConfirmButton: false,
                    text: 'We Process Your Request',
                },
                function (isConfirm) {
                    if (isConfirm) {
                        xhr.abort();
                    }
                });
        },
        error: function () {
            swal({
                type: 'error',
                title: 'Ooops! Something Wrong in Here',
                text: 'Silahkan Refresh Halaman Dan Coba Kembali',
            });
        },
        success: function (data) {
            // console.log(data);
            // return false;
            swal.close();
            $('#voucher-list').html(data.html);
            $('#voucher-detail').html(data.html_detail);
            var arrcode = data.code.join();
            // console.log(arrcode);
            $('#arrcode').val(arrcode);
        }
    });

    $('#modal-promo').modal('show');
}

function open_promo_detail(code, id) {
    // console.log('#detail_' + code);
    $('#back_button_voucher').attr("code", code);
    $('#modal-promo-detail').show();
    $('#detail_' + code).show();
    $('#modal-promo-body').hide();
    $('#vou_id').val(id);
    $('#vou_code').val(code);
}

function back_to_list() {
    // console.log('#detail_' + code);
    var code = $('#back_button_voucher').attr("code");
    $('#modal-promo-detail').hide();
    $('#detail_' + code).hide();
    $('#modal-promo-body').show();
}

function search_code_voucher() {
    var code_txt = $('#code_txt').val();
    var hasElementDiv = $("#voucher-code").has('div');
    var hasElementCont = $("#list_" + code_txt).has('div');
    console.log(hasElementDiv);
    // return false;
    
    //Clear in voucher code Div
    if(hasElementDiv.length > 0)
    {  
        $("#voucher-code").children().appendTo($('#voucher-list'));
    }
    
    if(hasElementCont.length == 0)
    {  
        $.ajax({
            type: "GET",
            url: site_url + '/cart/search_voucher/' + $('#store_id').val() + '/' + code_txt,
            dataType: 'json',
            beforeSend: function () {
                swal({
                        imageUrl: site_url + '/img/pacman.gif',
                        imageHeight: 1500,
                        title: 'Please Wait',
                        showCancelButton: true,
                        showConfirmButton: false,
                        text: 'We Process Your Request',
                    },
                    function (isConfirm) {
                        if (isConfirm) {
                            xhr.abort();
                        }
                    });
            },
            error: function () {
                swal({
                    type: 'error',
                    title: 'Ooops! Something Wrong in Here',
                    text: 'Silahkan Refresh Halaman Dan Coba Kembali',
                });
            },
            success: function (data) {
                // console.log(data);
                // return false;
                swal.close();
                $('#voucher-code').html(data.html);
                $('#voucher-detail').html(data.html_detail);
            }
        });   
    }else{
        $("#list_" + code_txt).appendTo($('#voucher-code'));
    }
}

function use_voucher(params, form_name) {

    var form_table = new FormData(form_name);
    var url = params.attr('data-url');
    form_table.append("id", $('#vou_id').val());
    form_table.append("code", $('#vou_code').val());

    $.ajax({
        type: "POST",
        url: url,
        data: form_table,
        dataType: 'json',
        processData: false,
        contentType: false,
        beforeSend: function () {
            swal({
                    imageUrl: site_url + '/img/pacman.gif',
                    imageHeight: 1500,
                    title: 'Please Wait',
                    showCancelButton: true,
                    showConfirmButton: false,
                    text: 'We Process Your Request',
                },
                function (isConfirm) {
                    if (isConfirm) {
                        xhr.abort();
                    }
                });
        },
        error: function () {
            swal({
                type: 'error',
                title: 'Ooops! Something Wrong in Here',
                text: 'Silahkan Refresh Halaman Dan Coba Kembali',
            });
        },
        success: function (data) {
            // console.log(data);
            // return false;
            if (data.res) {
                $('#total_amount_delivery').html(numeral($('#real_delivery').val()).format('0,0'));
                $('#biaya_layanan').html(numeral(data.arr['service_fee']).format('0,0'));
                $('#admin_bank').html(numeral(data.arr['payment_service_fee']).format('0,0'));
                $('#disc_biaya_layanan').html('-' + numeral(data.arr['minusBiayaLayanan']).format('0,0'));
                $('#total_amount_cart_txt').html(numeral(data.arr['total_price']).format('0,0'));
                $('#disc_admin_bank').html('-' + numeral(data.arr['minusPaymentServiceFee']).format('0,0'));
                $('#total_amount_delivery_disc').html('-' + numeral($('#total_diskon').val()).format('0,0'));
                $('#promo-used').show();
                $('#promo-txt').html(data.arr['txt']);
                swal({
                    type: 'success',
                    title: 'Selamat Kode Voucher Berhasil Gunakan'
                });
            } else {
                swal({
                    type: 'error',
                    title: data.msg
                });
            }
            $('#modal-promo').modal('hide');
        }
    });
}

function back_to_home_cart() {
    swal({
        type: 'success',
        title: 'Checkout Berhasil',
        text: 'Silahkan Cek History Untuk Status Pembayaran',
    });
    // console.log($('#store_id').val());
    // console.log($('#transaction_id').val());
    location.href = site_url + '/history/detail/' + $('#store_id').val() + '/' + $('#transaction_id').val();
}

function get_shipp_method(params) {
    if (params.val() == 2) {

        $.ajax({
            type: "GET",
            url: site_url + '/cart/get_delivery_attr',
            dataType: 'json',
            beforeSend: function () {
                swal({
                        imageUrl: site_url + '/img/pacman.gif',
                        imageHeight: 1500,
                        title: 'Please Wait',
                        showCancelButton: true,
                        showConfirmButton: false,
                        text: 'We Process Your Request',
                    },
                    function (isConfirm) {
                        if (isConfirm) {
                            xhr.abort();
                        }
                    });
            },
            error: function () {
                swal({
                    type: 'error',
                    title: 'Ooops! Something Wrong in Here',
                    text: 'Silahkan Refresh Halaman Dan Coba Kembali',
                });
            },
            success: function (data) {
                // console.log(data);
                // return false;
                if (data.res) {
                    swal.close();
                    $('#for_expidition').show();
                    $('#expidition').html(data.html.htmlexpi);
                    $('#for_addrs').show();
                    $('#content_addrs').html(data.html.htmladdrs);
                    $('#customer_address_id').val(data.html.address_id);

                    // close
                    $('#for_store').hide();
                    $('#for_pickup').hide();
                } else {

                    swal({
                        type: 'error',
                        title: data.msg,
                        showConfirmButton: true
                      }, function() {
                        if (data.url) {
                            location.href = data.url;
                        }
                      });
                }
            }
        });
    }else{
        $('#for_store').show();
        $('#for_pickup').show();
        $('#customer_address_id').val($('#addres_id_store').val());

        // close 
        $('#for_expidition').hide();
        $('#expidition').html('');
        $('#for_addrs').hide();
        $('#content_addrs').html('');
        $('#durasi_div').html('');
        $('#for_durasi').hide();
    }
    $('#shipp_method_txt').val(params.find(':selected').text());
}

function set_change_addrs(form_name) {
    var form_table = new FormData(form_name);
    var url = $(form_name).attr('action');

    $.ajax({
        type: "POST",
        url: url,
        data: form_table,
        dataType: 'json',
        processData: false,
        contentType: false,
        beforeSend: function () {
            swal({
                    imageUrl: site_url + '/img/pacman.gif',
                    imageHeight: 1500,
                    title: 'Please Wait',
                    showCancelButton: true,
                    showConfirmButton: false,
                    text: 'We Process Your Request',
                },
                function (isConfirm) {
                    if (isConfirm) {
                        xhr.abort();
                    }
                });
        },
        error: function () {
            swal({
                type: 'error',
                title: 'Ooops! Something Wrong in Here',
                text: 'Silahkan Refresh Halaman Dan Coba Kembali',
            });
        },
        success: function (data) {
            // console.log(data);
            // return false;
            if (data.res) {
                swal.close();
                $('#content_addrs').html(data.html.htmladdrs);
                $('#customer_address_id').val(data.html.address_id);
                $('#modal-change-addrs').modal('hide');
                reset_checkout();
            } else {
                swal({
                    type: 'error',
                    title: data.msg,
                    text: 'Silahkan Refresh Halaman Dan Coba Kembali',
                });
            }
        }
    });
}

function reset_checkout() {
    $('#li_amount_deliv').hide();
    $('#li_txt_deliv').hide();
    $('#li_txt_diskon').hide();

    $('#total_amount_delivery').html('0');
    $('#total_amount_delivery').hide();

    var diskon = $('#total_diskon').val();
    if (diskon != '') {
        $('#total_amount_diskon').html('0');
        $('#total_amount_diskon').hide();
        $('#li_amount_diskon').hide();
        $('#total_diskon').val('');   
    }

    var total_amount_before = $('#total_amount_cart').val() - $('#total_delivery').val();
    // console.log(total_amount_before);
    $('#total_amount_cart').val(total_amount_before);
    $('#total_amount_cart_txt').html(numeral(total_amount_before).format('0,0'));

    $('#total_delivery').val(0);
    $('#real_delivery').val(0);
    $('#durasi_div').html('');

    $('#durasi_txt').val('');

    if ($('#expidition').val() != '') {
        get_durasi($('#expidition')); 
    }
}

function get_durasi(params) {
    if (params.val() != 0 || params.val() != '') {
        var url = params.attr('data-action') + '/' + params.val();

        $.ajax({
            type: "GET",
            url: url,
            dataType: 'json',
            beforeSend: function () {
                swal({
                        imageUrl: site_url + '/img/pacman.gif',
                        imageHeight: 1500,
                        title: 'Please Wait',
                        showCancelButton: true,
                        showConfirmButton: false,
                        text: 'We Process Your Request',
                    },
                    function (isConfirm) {
                        if (isConfirm) {
                            xhr.abort();
                        }
                    });
            },
            error: function () {
                swal({
                    type: 'error',
                    title: 'Ooops! Something Wrong in Here',
                    text: 'Silahkan Refresh Halaman Dan Coba Kembali',
                });
            },
            success: function (data) {
                // console.log(data);
                // return false;
                if (data.res) {
                    swal.close();
                    $('#durasi_div').html(data.html.htmlexpi);
                    $('#for_durasi').show();

                    $('#expidition_txt').val(params.find(':selected').text());
                } else {
                    swal({
                        type: 'error',
                        title: data.msg,
                        text: 'Silahkan Refresh Halaman Dan Coba Kembali',
                    });
                }
            }
        });   
    }
}

function check_price(params, form_name) {
    var form_table = new FormData(form_name);
    var url = params.attr('data-action');
    if (params.is(":checked")) {
        $.ajax({
            type: "POST",
            url: url,
            data: form_table,
            dataType: 'json',
            processData: false,
            contentType: false,
            beforeSend: function () {
                swal({
                        imageUrl: site_url + '/img/pacman.gif',
                        imageHeight: 1500,
                        title: 'Please Wait',
                        showCancelButton: true,
                        showConfirmButton: false,
                        text: 'We Process Your Request',
                    },
                    function (isConfirm) {
                        if (isConfirm) {
                            params.removeAttr('checked');
                            xhr.abort();
                        }
                    });
            },
            error: function () {
                swal({
                    type: 'error',
                    title: 'Ooops! Something Wrong in Here',
                    text: 'Silahkan Refresh Halaman Dan Coba Kembali',
                });
            },
            success: function (data) {
                // console.log(data);
                // return false;
                if (data.res) {
                    swal.close();
                    $('#li_amount_deliv').show();
                    $('#li_txt_deliv').show();
    
                    var amount_deliv_real = data.html.amount_deliv - data.html.amount_disc;
                    var amount_deliv_txt = numeral(data.html.amount_deliv).format('0,0');
                    $('#total_amount_delivery').html(amount_deliv_txt);
                    $('#total_amount_delivery').show();
    
                    if (data.html.amount_disc != 0) {
                        var amount_diskon_txt = numeral(data.html.amount_disc).format('0,0');
                        $('#total_amount_diskon').html(' - ' + amount_diskon_txt);
                        $('#li_txt_diskon').show();
                        $('#total_amount_diskon').show();
                        $('#li_amount_diskon').show();
                        $('#total_diskon').val(data.html.amount_disc);   
                    }
    
                    var amount_cart_txt = numeral(data.html.amount_cart).format('0,0');
                    // console.log(amount_cart_txt);
                    $('#total_amount_cart_txt').html(amount_cart_txt);
    
                    $('#total_amount_cart').val(data.html.amount_cart);
                    $('#total_delivery').val(amount_deliv_real);
                    $('#real_delivery').val(data.html.amount_deliv);
                    
                    var durasi_val = params.val();
                    durasi_val = durasi_val.split("-");
                    // console.log($('#durasi_txt_' + durasi_val[0]).html());
                    $('#durasi_txt').val($('#durasi_txt_' + durasi_val[0]).html());
                } else {
                    params.prop('checked', false);
                    swal({
                        type: 'error',
                        title: data.title,
                        text: data.msg
                    });
                }
            }
        });   
    }else{
        reset_checkout();
    }
}

function change_addrs() {
    
    $.ajax({
        type: "GET",
        url: site_url + '/cart/change_addrs_deliv',
        dataType: 'json',
        beforeSend: function () {
            swal({
                    imageUrl: site_url + '/img/pacman.gif',
                    imageHeight: 1500,
                    title: 'Please Wait',
                    showCancelButton: true,
                    showConfirmButton: false,
                    text: 'We Process Your Request',
                },
                function (isConfirm) {
                    if (isConfirm) {
                        xhr.abort();
                    }
                });
        },
        error: function () {
            swal({
                type: 'error',
                title: 'Ooops! Something Wrong in Here',
                text: 'Silahkan Refresh Halaman Dan Coba Kembali',
            });
        },
        success: function (data) {
            // console.log(data);
            // $('#modal-addrs').modal('show');
            // return false;
            if (data.res) {
                swal.close();
                $('#addrs_modal').html(data.html.htmladdrs);
                $('#modal-change-addrs').modal('show');
            } else {
                swal({
                    type: 'error',
                    title: data.msg,
                    text: 'Silahkan Refresh Halaman Dan Coba Kembali',
                });
            }
        }
    });
}

function save_note(form_name) {
    var store_id = $('#store_modal').val();
    var product_id = $('#product_modal').val();
    var new_note = $('#note_modal').val();

    var temp = '';
    var form_table = new FormData(form_name);
    var url = $(form_name).attr('action');

    $.ajax({
        type: "POST",
        url: url,
        data: form_table,
        dataType: 'json',
        processData: false,
        contentType: false,
        beforeSend: function () {
            swal({
                    imageUrl: site_url + '/img/pacman.gif',
                    imageHeight: 1500,
                    title: 'Please Wait',
                    showCancelButton: true,
                    showConfirmButton: false,
                    text: 'We Process Your Request',
                },
                function (isConfirm) {
                    if (isConfirm) {
                        xhr.abort();
                    }
                });
        },
        error: function () {
            swal({
                type: 'error',
                title: 'Ooops! Something Wrong in Here',
                text: 'Silahkan Refresh Halaman Dan Coba Kembali',
            });
        },
        success: function (data) {
            // console.log(data);
            // return false;
            if (data.res) {
                $('#notes_' + product_id).val(new_note);
                $('#notes_span_' + product_id).html(new_note);
                $('#modal-checkout').modal('hide');
                
                swal({
                    title: data.msg,
                    type: 'success',
                    showConfirmButton: true
                });

            } else {
                swal({
                    type: 'error',
                    title: data.msg,
                    text: 'Silahkan Refresh Halaman Dan Coba Kembali',
                });
            }
        }
    });

}

function check_num(params, product_id, form_name) {
    var regCekNum = /^[0-9]+$/;
    var qty = params.val();
    var base_price = $('#base_price_' + product_id).val();
    if (!qty.match(regCekNum)) {
        swal({
            type: 'error',
            title: 'Ooops! Quantity Must Be A Number',
            text: 'Please Fill Quantity Correctly',
        });
        params.val(0);
        preview_cart(form_name);
        return false;
    }else{
        preview_cart(form_name);
    }
}

function count_all() {
    var total_amount = 0;
    $.each($(".total_cart"), function () {
        var temp_amount = $(this).val();
        // console.log(iTemp_val);
        total_amount = total_amount + parseInt(temp_amount);
    });

    total_amount = numeral(total_amount).format('0,0');
    $('#total_amount_cart').html(total_amount);
}

function delete_qty(par) {
    var store_id = par.attr('data-store');
    var product_id = par.attr('data-prod');
    var cart_number = par.attr('data-cart');

    $.ajax({
        type: "GET",
        url: site_url + '/cart/delete_cart/' + store_id + '/' + product_id,
        dataType: 'json',
        beforeSend: function () {
            swal({
                    imageUrl: site_url + '/img/pacman.gif',
                    imageHeight: 1500,
                    title: 'Please Wait',
                    showCancelButton: true,
                    showConfirmButton: false,
                    text: 'We Process Your Request',
                },
                function (isConfirm) {
                    if (isConfirm) {
                        xhr.abort();
                    }
                });
        },
        error: function () {
            swal({
                type: 'error',
                title: 'Ooops! Something Wrong in Here',
                text: 'Silahkan Refresh Halaman Dan Coba Kembali',
            });
        },
        success: function (data) {
            // console.log('cart_number_' + cart_number);
            // return false;
            if (data.res) {
                $('#cart_number_' + cart_number).remove();
                swal({
                    title: data.msg,
                    type: 'success',
                    showConfirmButton: true
                });
                setTimeout(function () {
                    location.reload();
                }, 2000);
            } else {
                swal({
                    type: 'error',
                    title: data.msg,
                    text: 'Silahkan Refresh Halaman Dan Coba Kembali',
                });
            }
        }
    });
}

function delete_qty_all(par) {

    $.ajax({
        type: "GET",
        url: site_url + '/cart/delete_cart_all',
        dataType: 'json',
        beforeSend: function () {
            swal({
                    imageUrl: site_url + '/img/pacman.gif',
                    imageHeight: 1500,
                    title: 'Please Wait',
                    showCancelButton: true,
                    showConfirmButton: false,
                    text: 'We Process Your Request',
                },
                function (isConfirm) {
                    if (isConfirm) {
                        xhr.abort();
                    }
                });
        },
        error: function () {
            swal({
                type: 'error',
                title: 'Ooops! Something Wrong in Here',
                text: 'Silahkan Refresh Halaman Dan Coba Kembali',
            });
        },
        success: function (data) {
            console.log(data);
            // return false;
            if (data.res) {
                swal({
                    title: data.msg,
                    type: 'success',
                    showConfirmButton: true
                });

                setTimeout(function () {
                    location.href = data.url;
                }, 2000);
            } else {
                swal({
                    type: 'error',
                    title: data.msg,
                    text: 'Silahkan Refresh Halaman Dan Coba Kembali',
                });
            }
        }
    });
}

function quantity_modal(par, store, prod) {
    $('#quantity_store').val(store);
    $('#quantity_prod').val(prod);
    
    var price = '<span> Rp. ' + par.attr('prod_price') + ' </span>';

    $('#quantity_img').attr('src', par.attr('prod_img'));
    $('#img').val(par.attr('prod_img'));
    $('#quantity_price').html(price);
    $('#grand_total').html(price);
    $('#quantity_name').html(par.attr('prod_name'));
    $('#name').val(par.attr('prod_name'));
    $('#quantity_max').val(par.attr('prod_quant'));

    $('#price').val(par.attr('prod_price'));
    $('#hidden_grand_total').val(par.attr('total'));

    $('#modal-quantity').modal('show');
    // console.log(img);
    // return false;
}

function set_total(par) {
    var quant = par.val();
    var price = $('#quantity_price').text();
    price = price.substring(5);
    price = price.replace(",", "");
    var total = price * quant;
    total = numeral(total).format('0,0')
    if (quant == '' || quant == null || quant == 0) {
        $('#grand_total').html('Rp. ' + 0);
        $('#hidden_grand_total').val(0);
    }else{
        $('#grand_total').html('Rp. '+ total);
        $('#hidden_grand_total').val(total);
    }
    // console.log(total);
    // console.log(final);
    // return false;

}