$(document).ready(function () {
    $(".form_datetime").datetimepicker({
        autoclose: true,
        isRTL: false,
        format: "dd MM yyyy - hh:ii",
        fontAwesome: true,
        pickerPosition: (false ? "bottom-right" : "bottom-left")
    });
});